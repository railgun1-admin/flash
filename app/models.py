from config import db
import datetime


class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key = True)
    user_name = db.Column(db.String(64), unique = True, nullable = False)
    password_hash = db.Column(db.String(64), nullable = False)
    role = db.Column(db.String(16), nullable = False)
    is_anonymous = False

    def __repr__(self):
        return '<User %r>' % (self.user_name)

    def is_authenticated(self):
        return True
    def is_active(self):
        return True
    def get_id(self):
        return self.id      
    def get_username(self):
        return self.user_name
    def get_urole(self):
        return self.role


class User_Store_Info(db.Model):
    __tablename__ = 'user_store_info'

    id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key = True)

    phone_number = db.Column(db.String(13), nullable = False)
    store_name = db.Column(db.String(30), nullable = False)
    e_mail = db.Column(db.String(30), nullable = False)
    director_name = db.Column(db.String(30))
    director_phone = db.Column(db.String(13))
    store_address = db.Column(db.String(30))

    prorogue = db.Column(db.Boolean, default = False)
    without_watermark = db.Column(db.Boolean, default = False)
    water_mark = db.Column(db.Boolean, default = False)
    access = db.Column(db.Boolean, default = False)


class User_Admin_Info(db.Model):
    __tablename__ = 'user_admin_info'

    id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key = True)

    admin_name = db.Column(db.String(30))
    delimeter = db.Column(db.String(2), default = ',')
    rights = db.Column(db.String(5))


class Download_Type(db.Model):
    __tablename__ = 'download_type'

    id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key = True)
    download_type = db.Column(db.SmallInteger)
    delimiter = db.Column(db.String(2))
    coding = db.Column(db.String(10))


class Download_Feature(db.Model):
    __tablename__ = 'download_feature'

    id = db.Column(db.Integer, primary_key = True)
    store_id = db.Column(db.Integer, db.ForeignKey('user_store_info.id'))
    feature_name = db.Column(db.String(20))
    feature_value = db.Column(db.String(40))


class User_Provider_Info(db.Model):
    __tablename__ = 'user_provider_info'

    id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key = True)
    provider_id = db.Column(db.Integer, db.ForeignKey('provider.id'))

    access_photo = db.Column(db.Boolean)
    provider_locale = db.Column(db.String(20))


class Service_Setting(db.Model):
    __tablename__ = 'service_setting'

    id = db.Column(db.Integer, primary_key = True)

    setting_type = db.Column(db.String(20), nullable = False)
    setting_value = db.Column(db.String(50), nullable = False)


class Displayed_Fields(db.Model):
    __tablename__ = 'displayed_fields'

    id = db.Column(db.Integer, primary_key = True)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    field_name = db.Column(db.String(20))
    flag = db.Column(db.Boolean)


class Upload_Field(db.Model):
    __tablename__ = 'upload_field'

    id = db.Column(db.Integer, primary_key = True)

    user_id = db.Column(db.Integer, db.ForeignKey('user_admin_info.id'))

    category = db.Column(db.Integer)
    column_name = db.Column(db.String(20))
    column_type = db.Column(db.String(20))


class Download_Field(db.Model):
    __tablename__ = 'download_config'

    id = db.Column(db.Integer, primary_key = True)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    category = db.Column(db.Integer)
    number_column = db.Column(db.Integer)
    name_column = db.Column(db.String(20))
    column_type = db.Column(db.String(20))


class Provider(db.Model):
    __tablename__ = 'provider'

    id = db.Column(db.Integer, primary_key = True)

    provider_name = db.Column(db.String(40), unique= True, nullable = False)
    address = db.Column(db.String(30))
    doll_to_uah = db.Column(db.Float)
    euro_to_uah = db.Column(db.Float)
    provider_phone = db.Column(db.String(13))
    provider_viber = db.Column(db.String(13))
    provider_mail = db.Column(db.String(30))
    market_rate = db.Column(db.Boolean)

    products = db.relationship('Product', lazy = 'dynamic')
    news = db.relationship('News', lazy = 'dynamic')


class Store_Provider(db.Model):
    __tablename__ = 'store_provider'

    id = db.Column(db.Integer, primary_key = True)

    provider_id = db.Column(db.Integer, db.ForeignKey('provider.id'))
    store_id = db.Column(db.Integer, db.ForeignKey('user_store_info.id'))


class Option(db.Model):
    __tablename__ = 'option'

    id = db.Column(db.Integer, primary_key = True)

    column_type = db.Column(db.String(20))
    column_option = db.Column(db.String(40))


class Product(db.Model):
    __tablename__ = 'product'

    id = db.Column(db.Integer, primary_key = True)

    category = db.Column(db.Integer)

    provider_id = db.Column(db.Integer, db.ForeignKey('provider.id'))

    news_product_id = db.Column(db.Integer, db.ForeignKey('new_product.id'))
    
    articul = db.Column(db.String(40), nullable = False)
    brand_name = db.Column(db.String(40))
    cost = db.Column(db.Float, nullable = False)
    cost_site = db.Column(db.Float, nullable = False)
    currency = db.Column(db.String(40), nullable = False)
    descreption = db.Column(db.String(300))
    thumbnail = db.Column(db.String(30), nullable = False)
    presence = db.Column(db.String(40), nullable = False)

    accept = db.Column(db.Boolean)

    date = db.Column(db.DateTime, default = datetime.datetime.utcnow())

    photos = db.relationship('Photo', lazy = 'dynamic')
    hist_change_cost = db.relationship('Old_Product_Cost', lazy = 'dynamic')
    hist_change_presence = db.relationship('Old_Product_Presence', lazy = 'dynamic')
    hist_change_features = db.relationship('Old_Product_Features', lazy = 'dynamic')
    hist_change_photos = db.relationship('New_Photo_Product', lazy = 'dynamic')
    hist_change_articul = db.relationship('New_Articul_Product', lazy = 'dynamic')


class Shoes(db.Model):
    __tablename__ = 'shoes'

    id = db.Column(db.Integer, db.ForeignKey('product.id'), primary_key = True)

    group = db.Column(db.String(40))
    type = db.Column(db.String(40))
    repeat = db.Column(db.String(40))
    sex = db.Column(db.String(40))
    min_order = db.Column(db.String(40))
    material_outside = db.Column(db.String(40))
    material_inside = db.Column(db.String(40))
    material_insole = db.Column(db.String(40))
    colour = db.Column(db.String(40))
    size = db.Column(db.String(40))
    season = db.Column(db.String(40))
    box = db.Column(db.String(40))
    manufacturer = db.Column(db.String(40))


class Photo(db.Model):
    __tablename__ = 'photo'

    id = db.Column(db.Integer, primary_key = True)

    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    url = db.Column(db.String(50))


class Preparatory_Shoes(db.Model):
    __tablename__ = 'preparatory_shoes'

    id = db.Column(db.Integer, primary_key = True)

    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    admin_id = db.Column(db.Integer, db.ForeignKey('user_admin_info.id'))
    provider_id = db.Column(db.Integer, db.ForeignKey('user_provider_info.id'))

    articul = db.Column(db.String(40))

    cost = db.Column(db.Float, nullable = False)
    cost_site = db.Column(db.Float, nullable = False)
    currency = db.Column(db.String(40), nullable = False)

    presence = db.Column(db.String(40), nullable = False)

    brand_name = db.Column(db.String(40))
    group = db.Column(db.String(40))
    type = db.Column(db.String(40))
    repeat = db.Column(db.String(40))
    sex = db.Column(db.String(40))
    min_order = db.Column(db.String(40))
    material_outside = db.Column(db.String(40))
    material_inside = db.Column(db.String(40))
    material_insole = db.Column(db.String(40))
    colour = db.Column(db.String(40))
    size = db.Column(db.String(40))
    season = db.Column(db.String(40))
    box = db.Column(db.String(40))
    manufacturer = db.Column(db.String(40))




class Admin_Upload(db.Model):
    __tablename__ = 'admin_upload'

    id = db.Column(db.Integer, primary_key = True)

    user_id = db.Column(db.Integer, db.ForeignKey('user_admin_info.id'))
    date = db.Column(db.DateTime, default = datetime.datetime.utcnow())


class News(db.Model):
    __tablename__ = 'news'

    id = db.Column(db.Integer, primary_key = True)

    provider_id = db.Column(db.Integer, db.ForeignKey('provider.id'))
    admin_id = db.Column(db.Integer, db.ForeignKey('user_admin_info.id'))

    category = db.Column(db.Integer)

    news_type = db.Column(db.SmallInteger)
    visible = db.Column(db.Boolean)

    date = db.Column(db.DateTime, default = datetime.datetime.utcnow())

    message = db.relationship('Admin_Message', uselist = False)

    products_change_cost = db.relationship('Old_Product_Cost', lazy = 'dynamic')
    products_change_presence = db.relationship('Old_Product_Presence', lazy = 'dynamic')
    products_change_features = db.relationship('Old_Product_Features', lazy = 'dynamic')
    products_change_photos = db.relationship('New_Photo_Product', lazy = 'dynamic')
    products_change_articul = db.relationship('New_Articul_Product', lazy = 'dynamic')


class New_Product(db.Model):
    __tablename__ = 'new_product'

    id = db.Column(db.Integer, db.ForeignKey('news.id'), primary_key = True)

    products = db.relationship('Product', lazy = 'dynamic')


class New_Photo_Product(db.Model):
    __tablename__ = 'new_photo_product'

    id = db.Column(db.Integer, primary_key = True)

    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    news_id = db.Column(db.Integer, db.ForeignKey('news.id'), primary_key = True)


class New_Articul_Product(db.Model):
    __tablename__ = 'new_articul_product'

    id = db.Column(db.Integer, primary_key = True)

    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    news_id = db.Column(db.Integer, db.ForeignKey('news.id'), primary_key = True)
    articul = db.Column(db.String(40))


class Old_Product_Cost(db.Model):
    __tablename__ = 'old_product_cost'

    id = db.Column(db.Integer, primary_key = True)

    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    news_id = db.Column(db.Integer, db.ForeignKey('news.id'))

    cost = db.Column(db.Float, nullable = False)
    cost_site = db.Column(db.Float, nullable = False)


class Old_Product_Presence(db.Model):
    __tablename__ = 'old_product_presence'

    id = db.Column(db.Integer, primary_key = True)

    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    news_id = db.Column(db.Integer, db.ForeignKey('news.id'))

    presence = db.Column(db.String(40), nullable = False)


class Old_Product_Features(db.Model):
    __tablename__ = 'old_product_features'

    id = db.Column(db.Integer, primary_key = True)

    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    news_id = db.Column(db.Integer, db.ForeignKey('news.id'))

    brand_name = db.Column(db.String(40))
    group = db.Column(db.String(40))
    type = db.Column(db.String(40))
    repeat = db.Column(db.String(40))
    sex = db.Column(db.String(40))
    min_order = db.Column(db.String(40))
    material_outside = db.Column(db.String(40))
    material_inside = db.Column(db.String(40))
    material_insole = db.Column(db.String(40))
    colour = db.Column(db.String(40))
    size = db.Column(db.String(40))
    season = db.Column(db.String(40))
    box = db.Column(db.String(40))
    manufacturer = db.Column(db.String(40))


class Admin_Message(db.Model):
    __tablename__ = 'admin_message'

    id = db.Column(db.Integer, primary_key = True)

    news_id = db.Column(db.Integer, db.ForeignKey('news.id'))

    tittle = db.Column(db.String(30))
    message = db.Column(db.String(2000))


class Feed_Back(db.Model):
    __tablename__ = 'feed_back'

    id = db.Column(db.Integer, primary_key = True)

    name = db.Column(db.String(40))
    phone = db.Column(db.String(13))
    email = db.Column(db.String(30))
    message = db.Column(db.String(1000))

    date = db.Column(db.DateTime, default = datetime.datetime.utcnow())


class Seen_News(db.Model):
    __tablename__ = 'seen_news'

    id = db.Column(db.Integer, primary_key = True)

    store_id = db.Column(db.Integer, db.ForeignKey('user_store_info.id'))
    news_id = db.Column(db.Integer, db.ForeignKey('news.id'))


class Store_Provider_Name(db.Model):
    __tablename__ = 'store_provider_name'

    id = db.Column(db.Integer, primary_key = True)

    store_id = db.Column(db.Integer, db.ForeignKey('user_store_info.id'))
    provider_id = db.Column(db.Integer, db.ForeignKey('provider.id'))
    provider_name = db.Column(db.String(40))


class Price(db.Model):
    __tablename__ = 'price'

    id = db.Column(db.Integer, primary_key = True)

    admin_id = db.Column(db.Integer, db.ForeignKey('user_admin_info.id'))
    photo = db.Column(db.Boolean)
    date_start = db.Column(db.DateTime)
    date_end = db.Column(db.DateTime)
    date = db.Column(db.DateTime, default = datetime.datetime.utcnow())

class Price_Filter(db.Model):
    __tablename__ = 'price_filter'

    id = db.Column(db.Integer, primary_key = True)

    price_id = db.Column(db.Integer, db.ForeignKey('price.id'))
    column_type = db.Column(db.String(20))
    column_value = db.Column(db.String(20))

class Price_Sort(db.Model):
    __tablename__ = 'price_sort'

    id = db.Column(db.Integer, primary_key = True)

    price_id = db.Column(db.Integer, db.ForeignKey('price.id'))
    column_type = db.Column(db.String(20))
    order_number = db.Column(db.SmallInteger)

class Price_Provider(db.Model):
    __tablename__ = 'price_provider'

    id = db.Column(db.Integer, primary_key = True)

    price_id = db.Column(db.Integer, db.ForeignKey('price.id'))
    provider_id = db.Column(db.Integer, db.ForeignKey('provider.id'))

