from app.models import *
from dateutil import tz
import datetime
import math


class Flash_Price():
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')

    def __init__(self, user_id):
        self.user_id = user_id


    def get_prices(self, page):
        query_prices = db.session.query(Price).order_by(Price.id.desc())

        quantity_prices = query_prices.count()

        prices_db = query_prices.paginate(page, 10, error_out=True)

        quantity_pages = int(math.ceil(quantity_prices/10))

        pages_li = [x for x in range(1, quantity_pages + 1) if math.fabs(x - page) < 3]

        prev = False if page == 1 else True

        next = False if page == quantity_pages or quantity_pages == 0 else True

        prices = []
        for price_db in prices_db.items:
            price_diction = {
                'id' : price_db.id,
                'count_providers' : db.session.query(Price_Provider).filter_by(price_id=price_db.id).count(),
                'count_filters' : db.session.query(Price_Filter).filter_by(price_id=price_db.id).count(),
                'photo' : 'Есть' if price_db.photo else 'Нет',
                'admin_name' : User_Admin_Info.query.get(price_db.admin_id).admin_name,
                'date' : price_db.date.replace(tzinfo=self.from_zone).astimezone(self.to_zone).strftime("%d-%m-%Y %H:%M")
            }

            prices.append(price_diction)
            
        return prices, pages_li, next, prev


    def get_price_detail(self, price_id):
        price_detail = {
            'price' : Price.query.get(price_id),
            'providers' : db.session.query(Provider).filter(Provider.id == Price_Provider.provider_id, Price_Provider.price_id == price_id).all(),
            'seasons' : Price_Filter.query.filter_by(column_type='season', price_id = price_id).all(),
            'groups' : Price_Filter.query.filter_by(column_type='group', price_id = price_id).all(),
            'presences' : Price_Filter.query.filter_by(column_type='presence', price_id = price_id).all(),
            'sortings' : Price_Sort.query.filter_by(price_id = price_id).order_by(Price_Sort.order_number).all()
        }

        return price_detail


    def get_price_info(self, price_id):
        price = Price.query.get(price_id)

        seasons_db = Price_Filter.query.filter_by(column_type='season', price_id = price.id).all()
        groups_db = Price_Filter.query.filter_by(column_type='group', price_id = price.id).all()
        presences_db = Price_Filter.query.filter_by(column_type='presence', price_id = price.id).all()
        providers_db = db.session.query(Provider).filter(Provider.id == Price_Provider.provider_id, Price_Provider.price_id == price.id).all()
        sortings_db = db.session.query(Price_Sort).filter(Price_Sort.price_id == price.id).all()

        seasons = []
        groups = []
        presences = []
        providers = []
        sortings = dict()

        for season_db in seasons_db:
            seasons.append(season_db.column_value)

        for group_db in groups_db:
            groups.append(group_db.column_value)

        for presence_db in presences_db:
            presences.append(presence_db.column_value)

        for provider_db in providers_db:
            providers.append(provider_db.provider_name)

        for sorting_db in sortings_db:
            if sorting_db.column_type == 'articul':
                sortings.update({'articul': sorting_db.order_number})

            if sorting_db.column_type == 'group':
                sortings.update({'group': sorting_db.order_number})

            if sorting_db.column_type == 'season':
                sortings.update({'season': sorting_db.order_number})

        price_info = {
            'from_date' : price.date_start.strftime("%Y-%m-%d"),
            'to_date' : price.date_end.strftime("%Y-%m-%d"),
            'seasons' : seasons,
            'groups' : groups,
            'presences' : presences,
            'providers' : providers,
            'sortings' : sortings,
            'photo' : price.photo
        }

        return price_info



    def delete_price(self, price_id):
        price_providers = Price_Provider.query.filter_by(price_id = price_id).all()
        for price_provider in price_providers:
            db.session.delete(price_provider)
            db.session.commit()
        filters = Price_Filter.query.filter_by(price_id = price_id).all()
        for filter_db in filters:
            db.session.delete(filter_db)
            db.session.commit()
        sorts = Price_Sort.query.filter_by(price_id = price_id).all()
        for sort in sorts:
            db.session.delete(sort)
            db.session.commit()
        price = Price.query.get(price_id)
        db.session.delete(price)
        db.session.commit()
        return


