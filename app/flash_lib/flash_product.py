from app.models import *
from dateutil import tz
import math
import os
from dateutil.relativedelta import relativedelta
import itertools


class Flash_Product():
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')

    server_zone = 'UTC'
    user_zone = 'Europe/Kiev'


    def __init__(self, user_id):
        self.user_id = user_id


    def set_user_zone(self, zone):
        self.user_zone = zone


    def get_list_from_filters(self, filters):
        filter_list = list()
        for filter_db in filters:
            filter_list.append(filter_db.column_value)
        return filter_list


    def get_product_by_objects(self, product, shoe, from_zone, to_zone, desc_short=False):

        diction = {
            'thumbnail' : product.thumbnail,
            'id' : product.id,
            'name' : (shoe.group + " " + product.brand_name).title(),
            'articul' : product.articul,
            'brand_name' : product.brand_name.title(),
            'provider_name' : Provider.query.get(product.provider_id).provider_name,
            'cost' : str(product.cost).replace('.',','),
            'cost_site' : str(product.cost_site).replace('.',','),
            'cost_uah' : str(product.cost).replace('.',',') if product.currency.lower() == 'грн' else '',
            'cost_site_uah' : str(product.cost_site).replace('.',',') if product.currency.lower() == 'грн' else '',
            'cost_doll' : str(product.cost).replace('.',',') if product.currency == '$' else '',
            'cost_site_doll' : str(product.cost_site).replace('.',',') if product.currency == '$' else '',
            'cost_euro' : str(product.cost).replace('.',',') if product.currency.lower() == 'евро' else '',
            'cost_site_euro' : str(product.cost_site).replace('.',',') if product.currency.lower() == 'евро' else '',
            'currency' : product.currency,
            'sex' : shoe.sex,
            'group' : shoe.group,
            'type' : shoe.type,
            'material_outside' : shoe.material_outside,
            'material_inside' : shoe.material_inside,
            'material_insole' : shoe.material_insole,
            'size' : shoe.size,
            'colour' : shoe.colour,
            'repeat' : shoe.repeat,
            'season' : shoe.season,
            'box' : shoe.box,
            'min_order' : shoe.min_order,
            'manufacturer' : shoe.manufacturer,
            'presence' : product.presence,
            'date' : product.date.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%d-%m-%Y"),
            'accept' : product.accept,
            'real_group' : shoe.group,
            'empty' : ""
            
        }   

        if desc_short:
            descreption_end = ''
            if len(product.descreption) > 20:
                descreption_end = '...'
            diction.update({'descreption' : product.descreption[:20] + descreption_end})
        else:
            diction.update({'descreption' : product.descreption})  

        if User.query.get(self.user_id).role == 'STORE':
            diction.update({'provider_name' : Store_Provider_Name.query.filter_by(store_id=self.user_id, provider_id=product.provider_id).first().provider_name})
            diction.update({'group' : Download_Feature.query.filter_by(store_id = self.user_id).first().feature_value + shoe.group})

        return diction


    def get_products_changes(self, admin, provider):
        products_info_query = db.session.query(Product, Shoes, Preparatory_Shoes).filter(Product.id==Shoes.id, Preparatory_Shoes.product_id==Product.id)

        if provider != 'all':
            products_info_query = products_info_query.filter(Product.provider_id == provider)

        if admin != 'all':
            if admin != 0:
                products_info_query = products_info_query.filter(Preparatory_Shoes.admin_id == admin)
            else:
                products_info_query = products_info_query.filter(Preparatory_Shoes.admin_id == None)

        products_info = products_info_query.all()

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)
        products = []
        for product, shoe, preparatory_shoe in products_info:
            product_diction = self.get_product_by_objects(product, shoe, from_zone, to_zone)
            preparatory_diction = {
                'new_articul' : preparatory_shoe.articul,
                'new_brand_name' : preparatory_shoe.brand_name.title(),
                'new_cost' : str(preparatory_shoe.cost).replace('.',','),
                'new_cost_site' : str(preparatory_shoe.cost_site).replace('.',','),
                'new_currency' : preparatory_shoe.currency,
                'new_sex' : preparatory_shoe.sex,
                'new_group' : preparatory_shoe.group,
                'new_type' : preparatory_shoe.type,
                'new_material_outside' : preparatory_shoe.material_outside,
                'new_material_inside' : preparatory_shoe.material_inside,
                'new_material_insole' : preparatory_shoe.material_insole,
                'new_size' : preparatory_shoe.size,
                'new_colour' : preparatory_shoe.colour,
                'new_repeat' : preparatory_shoe.repeat,
                'new_season' : preparatory_shoe.season,
                'new_box' : preparatory_shoe.box,
                'new_min_order' : preparatory_shoe.min_order,
                'new_manufacturer' : preparatory_shoe.manufacturer,
                'new_presence' : preparatory_shoe.presence,
            }

            product_diction.update(preparatory_diction)
            products.append(product_diction)

        return products

    def get_product_changes(self, id):
        diction = self.get_products_by_id(id)

        preparatory_shoe = Preparatory_Shoes.query.filter_by(product_id=id).first()
        if preparatory_shoe:
            prepare_diction = {
                'new_articul' : preparatory_shoe.articul,
                'new_brand_name' : preparatory_shoe.brand_name.title(),
                'new_cost' : str(preparatory_shoe.cost).replace('.',','),
                'new_cost_site' : str(preparatory_shoe.cost_site).replace('.',','),
                'new_currency' : preparatory_shoe.currency,
                'new_sex' : preparatory_shoe.sex,
                'new_group' : preparatory_shoe.group,
                'new_type' : preparatory_shoe.type,
                'new_material_outside' : preparatory_shoe.material_outside,
                'new_material_inside' : preparatory_shoe.material_inside,
                'new_material_insole' : preparatory_shoe.material_insole,
                'new_size' : preparatory_shoe.size,
                'new_colour' : preparatory_shoe.colour,
                'new_repeat' : preparatory_shoe.repeat,
                'new_season' : preparatory_shoe.season,
                'new_box' : preparatory_shoe.box,
                'new_min_order' : preparatory_shoe.min_order,
                'new_manufacturer' : preparatory_shoe.manufacturer,
                'new_presence' : preparatory_shoe.presence,
            }
            diction.update(prepare_diction)
        return diction



    def get_product(self, id):
        diction = self.get_products_by_id(id)

        preparatory_shoe = Preparatory_Shoes.query.filter_by(product_id=id).first()
        if preparatory_shoe:
            prepare_diction = {
                'articul' : preparatory_shoe.articul,
                'brand_name' : preparatory_shoe.brand_name.title(),
                'cost' : str(preparatory_shoe.cost).replace('.',','),
                'cost_site' : str(preparatory_shoe.cost_site).replace('.',','),
                'currency' : preparatory_shoe.currency,
                'sex' : preparatory_shoe.sex,
                'group' : preparatory_shoe.group,
                'type' : preparatory_shoe.type,
                'material_outside' : preparatory_shoe.material_outside,
                'material_inside' : preparatory_shoe.material_inside,
                'material_insole' : preparatory_shoe.material_insole,
                'size' : preparatory_shoe.size,
                'colour' : preparatory_shoe.colour,
                'repeat' : preparatory_shoe.repeat,
                'season' : preparatory_shoe.season,
                'box' : preparatory_shoe.box,
                'min_order' : preparatory_shoe.min_order,
                'manufacturer' : preparatory_shoe.manufacturer,
                'presence' : preparatory_shoe.presence,
            }
            diction.update(prepare_diction)
        return diction


    def get_products_by_id(self, id, desc_short=False):
        product = Product.query.get(id)
        shoe = Shoes.query.get(id)

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)   

        diction = self.get_product_by_objects(product, shoe, from_zone, to_zone, desc_short)
        return diction


    def get_photos_by_product_id(self, product_id):
        product = Product.query.get(product_id)
        result = None
        if product:
            result = product.photos.all()
        return result


    def get_products_by_news(self, news_id):
        new = News.query.get(news_id)
        if new is None:
            return False  

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)   

        if new.news_type == 1:
            products = db.session.query(Product, Shoes).filter(Product.news_product_id == news_id, Product.id == Shoes.id).all()
        elif new.news_type == 2:
            products = db.session.query(Product, Shoes, Old_Product_Presence).filter(Product.id == Old_Product_Presence.product_id, Product.id == Shoes.id, Old_Product_Presence.news_id == news_id).all()
        elif new.news_type == 3:
            products = db.session.query(Product, Shoes, Old_Product_Cost).filter(Product.id == Old_Product_Cost.product_id, Product.id == Shoes.id, Old_Product_Cost.news_id == news_id).all()
        elif new.news_type == 4:
            products = db.session.query(Product, Shoes).filter(Product.id == New_Photo_Product.product_id, New_Photo_Product.news_id == news_id, Product.id == Shoes.id).all()
        elif new.news_type == 5:
            products = db.session.query(Product, Shoes, Old_Product_Features).filter(Product.id == Old_Product_Features.product_id, Old_Product_Features.news_id == news_id, Product.id == Shoes.id).all()
        elif new.news_type == 9:
            products = db.session.query(Product, Shoes, New_Articul_Product).filter(Product.id == New_Articul_Product.product_id, New_Articul_Product.news_id == news_id, Product.id == Shoes.id).all()

        goods = []
        if new.news_type in [1, 4]:
            for product, shoe in products:
                goods.append(self.get_product_by_objects(product, shoe, from_zone, to_zone, True))   

        if new.news_type == 2:
            for product, shoe, old_presence in products:
                product_diction = self.get_product_by_objects(product, shoe, from_zone, to_zone, True)
                product_diction.update({'old_presence': old_presence.presence })
                goods.append(product_diction)   

        if new.news_type == 3:
            for product, shoe, old_cost in products:
                product_diction = self.get_product_by_objects(product, shoe, from_zone, to_zone, True)
                product_diction.update({'old_cost': old_cost.cost })
                product_diction.update({'old_cost_site': old_cost.cost_site })
                goods.append(product_diction)

        if new.news_type == 9:
            for product, shoe, new_articul in products:
                product_diction = self.get_product_by_objects(product, shoe, from_zone, to_zone, True)
                product_diction.update({'old_articul': new_articul.articul})
                goods.append(product_diction)

        if new.news_type == 5:
            for product, shoe, old_product_feature in products:
                product_diction = self.get_product_by_objects(product, shoe, from_zone, to_zone, True)
                product_diction.update({'old_brand_name' : old_product_feature.brand_name })
                product_diction.update({'old_group' : old_product_feature.group})
                product_diction.update({'old_type' : old_product_feature.type})
                product_diction.update({'old_repeat' : old_product_feature.repeat})
                product_diction.update({'old_sex' : old_product_feature.sex})
                product_diction.update({'old_min_order' : old_product_feature.min_order})
                product_diction.update({'old_material_outside' : old_product_feature.material_outside})
                product_diction.update({'old_material_inside' : old_product_feature.material_inside})
                product_diction.update({'old_material_insole' : old_product_feature.material_insole})
                product_diction.update({'old_colour' : old_product_feature.colour})
                product_diction.update({'old_size' : old_product_feature.size})
                product_diction.update({'old_season' : old_product_feature.season})
                product_diction.update({'old_box' : old_product_feature.box})
                product_diction.update({'old_manufacturer' : old_product_feature.manufacturer})
                goods.append(product_diction)



        return goods


    def get_products_by_filter(self, filter):
        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)

        try:
            category = int(filter.get('category'))
        except ValueError:
            return False

        try:
            page = int(filter.get('page'))
        except ValueError:
            return False

        if 1 < category < 1:
            return False

        option = Option.query.filter_by(column_option = filter.get('presence'), column_type = 'presence').first()
        if option is None:
            return False

        query_products = db.session.query(Product, Shoes).filter(Product.id == Shoes.id).filter(Product.category == category, db.func.lower(Product.presence) == filter.get('presence')).order_by(Product.id.desc())

        if not filter.get('provider') == "all":
            provider = Provider.query.filter_by(id=filter.get('provider')).first()
            if provider is None:
                return False

            query_products = query_products.filter(Product.provider_id == provider.id)

        if 'articul' in filter and filter.get('articul') != "":
            query_products = query_products.filter(db.func.lower(Product.articul).contains(filter.get('articul').lower()))

        elif 'flash_id' in filter and filter.get('flash_id') != "":
            try:
                search_id = int(filter.get('flash_id'))
            except ValueError:
                return False
            query_products = query_products.filter(Product.id == search_id)

        elif 'from_date' in filter and 'to_date' in filter:
            try:
                from_date = datetime.datetime.strptime(filter.get('from_date'), "%Y-%m-%d").replace(tzinfo=to_zone)
                to_date = datetime.datetime.strptime(filter.get('to_date'), "%Y-%m-%d").replace(tzinfo=to_zone).date()
            except ValueError:
                from_date = datetime.datetime.strptime(filter.get('from_date'), "%d-%m-%Y").replace(tzinfo=to_zone)
                to_date = datetime.datetime.strptime(filter.get('to_date'), "%d-%m-%Y").replace(tzinfo=to_zone).date()

            from_date -= relativedelta(days=1)
            from_date = from_date.replace(hour=22)
            
            query_products = query_products.filter(Product.date >= from_date, db.func.date(Product.date) <= to_date)


        if User.query.get(self.user_id).role == "STORE":
            constraints = Store_Provider.query.filter_by(store_id = self.user_id).all()
            for constraint in constraints:
                query_products = query_products.filter(Product.provider_id != constraint.provider_id)

        quantity_products = query_products.count()

        products = query_products.paginate(page, 50, error_out=True)

        quantity_pages = int(math.ceil(quantity_products/50))

        pages_li = [x for x in range(1, quantity_pages + 1) if math.fabs(x - page) < 3]

        prev = False if page == 1 else True

        next = False if page == quantity_pages or quantity_pages == 0 else True

        goods = []
        for product, shoe in products.items:
            goods.append(self.get_product_by_objects(product, shoe, from_zone, to_zone, True))


        return goods, pages_li, next, prev


    def get_products_for_provider(self, filter, provider_id):
        products_query = db.session.query(Product, Shoes).filter(Product.id == Shoes.id, Product.provider_id == provider_id)

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)

        try:
            page = int(filter.get('page'))
        except ValueError:
            return False

        if filter.get('articul'):
            products_query = products_query.filter(db.func.lower(Product.articul).contains(filter.get('articul').lower()))

        else:

            if filter.get('status') == 'true':
                products_query = products_query.filter(Product.accept == True)
            elif filter.get('status') == 'false':
                products_query = products_query.filter(Product.accept == False)
            else:
                return False    

            if filter.get('season') == 'all':
                products_query = products_query
            elif Option.query.filter_by(column_type = 'season', column_option = filter.get('season')).first():
                products_query = products_query.filter(Shoes.season == filter.get('season'))
            else:
                return False    

            if Option.query.filter_by(column_type = 'presence', column_option = filter.get('presence')).first():
                products_query = products_query.filter(Product.presence == filter.get('presence'))
            else:
                return False    
            
            products_query = products_query.filter(Product.date >= filter.get('from_date'), db.func.date(Product.date) <= filter.get('to_date'))

            if filter.get('sorting') == 'art':
                products_query = products_query.order_by(Product.articul)
            elif filter.get('sorting') == 'date_art':
                products_query = products_query.order_by(Product.date.desc(), Product.articul)
            else:
                return False

        
        quantity_products = products_query.count()

        products = products_query.paginate(page, 50, error_out=True)
        

        quantity_pages = int(math.ceil(quantity_products/50))

        pages_li = [x for x in range(1, quantity_pages + 1) if math.fabs(x - page) < 3]

        prev = False if page == 1 else True

        next = False if page == quantity_pages or quantity_pages == 0 else True

        

        goods = []
        for product, shoe in products.items:
            diction = self.get_product_by_objects(product, shoe, from_zone, to_zone, True)
            prep_shoes = Preparatory_Shoes.query.filter_by(product_id = diction.get('id')).first()
            if prep_shoes:
                diction.update({'cost': str(prep_shoes.cost).replace('.',','), 'cost_site': str(prep_shoes.cost_site).replace('.',','), 'presence': prep_shoes.presence })

            goods.append(diction)

        return goods, pages_li, next, prev


    def get_products_by_provider(self, provider_id, page, per_page, from_date, to_date, articul):

        if articul:
            products_query = db.session.query(Product, Shoes).filter(Product.id == Shoes.id, Product.provider_id == provider_id, db.func.lower(Product.articul).contains(articul.lower())).order_by(Shoes.season, Product.articul)
        else:
            products_query = db.session.query(Product, Shoes).filter(Product.id == Shoes.id, Product.provider_id == provider_id, db.func.lower(Product.presence) == 'есть', Product.date >= from_date, db.func.date(Product.date) <= to_date)

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)

        quantity_products = products_query.count()

        quantity_pages = int(math.ceil(quantity_products/per_page))

        pages_li = [x for x in range(1, quantity_pages + 1) if math.fabs(x - page) < 3]

        prev = False if page == 1 else True

        next = False if page == quantity_pages or quantity_pages == 0 else True

        products = products_query.paginate(page, per_page, error_out=True)

        goods = []
        for product, shoe in products.items:
            goods.append(self.get_product_by_objects(product, shoe, from_zone, to_zone, True))

        return goods, pages_li, next, prev


    def get_products_by_price_provider(self, price_id, provider_id):
        price = Price.query.get(price_id)

        price_filters_presence = Price_Filter.query.filter_by(column_type='presence', price_id=price_id).all()
        price_filters_group = Price_Filter.query.filter_by(column_type='group', price_id=price_id).all()
        price_filters_season = Price_Filter.query.filter_by(column_type='season', price_id=price_id).all()

        sorts = Price_Sort.query.filter_by(price_id=price_id).order_by(Price_Sort.order_number).all()

        presence_list = self.get_list_from_filters(price_filters_presence)
        group_list = self.get_list_from_filters(price_filters_group)
        season_list = self.get_list_from_filters(price_filters_season)

        products_query = db.session.query(Product, Shoes).filter(Product.id == Shoes.id, Product.provider_id == provider_id, db.func.date(Product.date) <= price.date_end.date(), Product.date >= price.date_start,
            db.func.lower(Product.presence).in_(presence_list), db.func.lower(Shoes.season).in_(season_list), db.func.lower(Shoes.group).in_(group_list))

        for sort in sorts:
            if sort.column_type == 'articul':
                products_query = products_query.order_by(Product.articul)
            elif sort.column_type == 'group':
                products_query = products_query.order_by(Shoes.group)
            elif sort.column_type == 'season':
                products_query = products_query.order_by(Shoes.season)

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)

        products = products_query.all()

        pages = []
        goods = []
        i = 0
        for product, shoe in products:
            i += 1
            goods.append(self.get_product_by_objects(product, shoe, from_zone, to_zone, True))

            if price.photo:
                if (i % 12 == 0 and i == 12) or ((i - 12) % 13 == 0 and i != 13) or i == len(products):
                    pages.append(goods)
                    goods = []

            else:
                if i % 114 == 0 or i == len(products):
                    first_column = []
                    second_column = []
                    for j in range(0, len(goods)):
                        if int(j/57) % 2 == 0:
                            first_column.append(goods[j])
                        else:
                            second_column.append(goods[j])
                    products_tuple = itertools.zip_longest(first_column, second_column)
                    pages.append(products_tuple)
                    goods = []

        return pages


    def get_displayed_fields(self):
        displayed_fields = {}

        displayed_fields_db = Displayed_Fields.query.filter_by(user_id=self.user_id).all()
        for field in displayed_fields_db:
            displayed_fields.update({field.field_name : field.flag})

        return displayed_fields


    def get_products_dates(self, provider_id):
        dates_db = db.session.query(db.func.date(Product.date)).distinct(db.func.date(Product.date)).filter(Product.provider_id == provider_id).limit(8).all()

        dates = []
        for date_db in dates_db:
            dates.append(date_db[0])

        return dates


    def delete_product_by_id(self, id):
        product = Product.query.get(id)
        if product:
            self.delete_product(product)
        return


    def delete_product(self, product):
        shoe = Shoes.query.get(product.id)

        photos = product.photos.all()

        old_product_costs = product.hist_change_cost.all()
        old_product_presences = product.hist_change_presence.all()
        old_product_features = product.hist_change_features.all()
        news_photo_product = product.hist_change_photos.all()
        news_articul_product = product.hist_change_articul.all()
        preparatories_shoes = Preparatory_Shoes.query.filter_by(product_id=product.id).all()

        db.session.delete(shoe)

        for preparatory_shoe in preparatories_shoes:
            db.session.delete(preparatory_shoe)

        for photo in photos:
            db.session.delete(photo)
            try:
                os.remove("/home/dev/flash/files/photographs/" + str(photo.id) + ".jpg" )
            except FileNotFoundError:
                continue

        for old_product_cost in old_product_costs:
            db.session.delete(old_product_cost)

        for old_product_presence in old_product_presences:
            db.session.delete(old_product_presence)

        for old_product_feature in old_product_features:
            db.session.delete(old_product_feature)

        for new_photo_product in news_photo_product:
            db.session.delete(new_photo_product)

        for new_articul_product in news_articul_product:
            db.session.delete(new_articul_product)

        os.remove("/home/dev/flash/files/thumbnails/" + str(product.thumbnail) )

        db.session.delete(product)

        db.session.commit()

        return