from app.models import *
from dateutil import tz
import os
import hashlib


class Flash_User():


    def get_max_user_id(self):
        try:
            max_user_id = db.session.query(db.func.max(User.id)).scalar() + 1
        except TypeError:
            max_user_id = 1
        return max_user_id


    def add_user_displayed_fields(self, user_id):
        field = Displayed_Fields(user_id=user_id, field_name='sex', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='group', flag=True)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='type', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='provider', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='brand', flag=True)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='material_outside', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='material_inside', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='material_insole', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='size', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='repeat', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='season', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='colour', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='box', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='min_order', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='manufacturer', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='descreption', flag=False)
        db.session.add(field)
        db.session.commit()
        field = Displayed_Fields(user_id=user_id, field_name='date', flag=False)
        db.session.add(field)
        db.session.commit()

        return


    def add_user_settings(self, user_id):

        field = Download_Field(user_id=user_id, number_column=1, name_column="id", column_type="id", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=2, name_column="Наименование", column_type="name", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=3, name_column="Артикул", column_type="articul", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=4, name_column="Бренд", column_type="brand_name", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=5, name_column="Поставщик", column_type="provider_name", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=6, name_column="Цена", column_type="cost", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=7, name_column="Цена сайт", column_type="cost_site", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=8, name_column="Цена грн", column_type="cost_uah", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=9, name_column="Цена сайт грн", column_type="cost_site_uah", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=10, name_column="Цена $", column_type="cost_doll", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=11, name_column="Цена сайт $", column_type="cost_site_doll", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=12, name_column="Цена евро", column_type="cost_euro", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=13, name_column="Цена сайт евро", column_type="cost_site_euro", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=14, name_column="Валюта", column_type="currency", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=15, name_column="Пол", column_type="sex", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=16, name_column="Группа", column_type="group", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=17, name_column="Тип", column_type="type", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=18, name_column="Материал верха", column_type="material_outside", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=19, name_column="Материал внутри", column_type="material_inside", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=20, name_column="Материал стельки", column_type="material_insole", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=21, name_column="Размер", column_type="size", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=22, name_column="Повторы", column_type="repeat", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=23, name_column="Сезон", column_type="season", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=24, name_column="Цвет", column_type="colour", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=25, name_column="Ящик", column_type="box", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=26, name_column="Минимальный заказ", column_type="min_order", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=27, name_column="Производитель", column_type="manufacturer", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=28, name_column="Описание", column_type="descreption", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=29, name_column="Наличие", column_type="presence", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=30, name_column="Фото1", column_type="photo_1", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=31, name_column="Фото2", column_type="photo_2", category=1)
        db.session.add(field)
        db.session.commit()
        field = Download_Field(user_id=user_id, number_column=32, name_column="Фото3", column_type="photo_3", category=1)
        db.session.add(field)
        db.session.commit()

        return


    def admin_upload_setting(self, user_admin_id):
        field = Upload_Field(user_id=user_admin_id, column_name="id", column_type="id", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Артикул", column_type="articul", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Бренд", column_type="brand_name", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Поставщик", column_type="provider_name", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Цена", column_type="cost", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Цена сайт", column_type="cost_site", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Валюта", column_type="currency", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Пол", column_type="sex", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Группа", column_type="group", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Тип", column_type="type", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Материал верха", column_type="material_outside", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Материал внутри", column_type="material_inside", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Материал стельки", column_type="material_insole", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Размер", column_type="size", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Повторы", column_type="repeat", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Сезон", column_type="season", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Цвет", column_type="colour", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Ящик", column_type="box", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Минимальный заказ", column_type="min_order", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Производитель", column_type="manufacturer", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Описание", column_type="descreption", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Наличие", column_type="presence", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Фото1", column_type="photo_1", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Фото2", column_type="photo_2", category=1)
        db.session.add(field)
        db.session.commit()
        field = Upload_Field(user_id=user_admin_id, column_name="Фото3", column_type="photo_3", category=1)
        db.session.add(field)
        db.session.commit()

        return


    def check_existing_user(self, user_name):
        user = User.query.filter(db.func.lower(User.user_name)==user_name.lower()).first()
        if user is None:
            return False
        else:
            return True


    def add_user_provider(self, user_name, password, provider_id, access_photo, locale):
        user_id = self.get_max_user_id()

        password_hash = hashlib.sha256(password.encode('utf-8')).hexdigest()

        user = User(id=user_id, user_name=user_name, password_hash=password_hash, role='PROVIDER')
        db.session.add(user)
        db.session.commit()

        user_provider_info = User_Provider_Info(id=user_id, provider_id=provider_id, access_photo=access_photo, provider_locale=locale)
        db.session.add(user_provider_info)
        db.session.commit()

        self.add_user_displayed_fields(user_id)

        return


    def add_user_store(self, user_name, password, phone_number, store_name, e_mail, director_name, director_phone, store_address):

        user_id = self.get_max_user_id()

        password_hash = hashlib.sha256(password.encode('utf-8')).hexdigest()

        user = User(id=user_id, user_name=user_name, password_hash=password_hash, role='STORE')
        db.session.add(user)
        db.session.commit() 

        user_store_info = User_Store_Info(id=user_id, phone_number=phone_number, store_name=store_name, e_mail=e_mail, 
            director_name=director_name, director_phone=director_phone, store_address=store_address)
        db.session.add(user_store_info)
        db.session.commit() 

        self.add_user_displayed_fields(user_id)
        self.add_user_settings(user_id)

        os.makedirs('/home/dev/flash/files/users/' + user.user_name)

        download_type = Download_Type(id = user_id, download_type=1, coding='utf-8', delimiter=',')
        db.session.add(download_type)
        db.session.commit()

        providers = Provider.query.all()
        for provider in providers:
            store_provider_name = Store_Provider_Name(store_id=user_id, provider_id = provider.id, provider_name=provider.provider_name)
            db.session.add(store_provider_name)
            db.session.commit()

        download_feature = Download_Feature(store_id=user_id, feature_name='before_group', feature_value='')
        db.session.add(download_feature)
        db.session.commit()

        return


    def add_user_admin(self, user_name, password, admin_name, admin_rights):

        user_id = self.get_max_user_id()

        password_hash = hashlib.sha256(password.encode('utf-8')).hexdigest()

        user = User(id=user_id, user_name=user_name, password_hash=password_hash, role='ADMIN')
        db.session.add(user)
        db.session.commit()
        user_admin_info = User_Admin_Info(id=user_id, admin_name=admin_name, rights=admin_rights)
        db.session.add(user_admin_info)
        db.session.commit()

        self.add_user_displayed_fields(user_id)
        self.add_user_settings(user_id)

        self.admin_upload_setting(user_id)

        return

    def get_user_by_credentials(self, user_name, password):
        password_hash = hashlib.sha256(password.encode('utf-8')).hexdigest()

        user = User.query.filter(User.password_hash==password_hash, db.func.lower(User.user_name)==user_name.lower()).first()
        return user

    def change_password_store(self, user_id, password, new_password, admin_flag=False):
        user = User.query.get(user_id)
        password_hash = hashlib.sha256(password.encode('utf-8')).hexdigest()
        new_password_hash = hashlib.sha256(new_password.encode('utf-8')).hexdigest()

        if not admin_flag and user.password_hash != password_hash:
            return False

        user.password_hash = new_password_hash
        db.session.commit()

        return True


