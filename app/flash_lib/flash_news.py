import xlsxwriter
import itertools
from app.models import *
from .flash_product import Flash_Product
from random import randint
import datetime
import os
import zipfile
import math
from dateutil import tz
from fpdf import FPDF


class Flash_News():
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')


    def __init__(self, user_id):
        self.user_id = user_id
        self.flash_product = Flash_Product(user_id)
        self.user = User.query.get(user_id)


    def set_user_zone(self, zone):
        self.to_zone = tz.gettz(zone)


    def get_news_types(self):
        news_types = {
            1 : 'Добавление нового товара',
            2 : 'Изменение наличия товара',
            3 : 'Изменение цены товара',
            4 : 'Изменение фотографии товара',
            5 : 'Изменение характеристик товара',
            6 : 'Изменение курса поставщика',
            7 : 'Изменение адреса поставщика',
            9 : 'Изменение артикула товара'
        }
        return news_types


    def get_all_news(self, filter):
        news_db_query = News.query.order_by(News.id.desc())

        try:
            page = int(filter.get('page'))
        except ValueError:
            return False


        if filter.get('provider') != 'all':
            try:
                news_db_query = news_db_query.filter(News.provider_id == int(filter.get('provider')))
            except ValueError:
                return False


        if filter.get('admin') != 'all':
            try:
                news_db_query = news_db_query.filter(News.admin_id == int(filter.get('admin')))
            except ValueError:
                return False


        if filter.get('type') != 'all':
            try:
                news_db_query = news_db_query.filter(News.news_type == int(filter.get('type')))
            except ValueError:
                return False

        news_db_query = news_db_query.filter(db.func.date(News.date) >= filter.get('from_date'), db.func.date(News.date) <= filter.get('to_date'))

        quantity_news = news_db_query.count()

        news_db = news_db_query.paginate(page, 10, error_out=True)

        quantity_pages = int(math.ceil(quantity_news/10))

        pages_li = [x for x in range(1, quantity_pages + 1) if math.fabs(x - page) < 3]

        prev = False if page == 1 else True

        next = False if page == quantity_pages or quantity_pages == 0 else True

        news_types = self.get_news_types()
        news = []

        for new in news_db.items:
            central = new.date.replace(tzinfo=self.from_zone).astimezone(self.to_zone)

            diction = {
                'id' : new.id,
                'datetime' : central.strftime("%d-%m-%Y %H:%M"),
                'visible' : new.visible,
                'news_type_int' : new.news_type,
                'admin_name' : User_Admin_Info.query.get(new.admin_id).admin_name if not new.admin_id is None else '' 
            }

            if new.news_type == 8:
                admin_message = Admin_Message.query.filter_by(news_id=new.id).first()
                diction.update({'news_type' : admin_message.tittle })
            elif new.news_type == 6:
                provider = Provider.query.get(new.provider_id)
                diction.update({'news_type' : news_types[new.news_type] + ' 1$=' + str(provider.doll_to_uah) + ' грн. 1евро=' + str(provider.euro_to_uah) + ' грн.'})
                diction.update({'provider_name' : provider.provider_name})
                diction.update({'provider_address' : provider.address})
            else:
                provider = Provider.query.get(new.provider_id)
                diction.update({'news_type' : news_types[new.news_type]})
                diction.update({'provider_name' : provider.provider_name})
                diction.update({'provider_address' : provider.address})

            

            news.append(diction)

        return news, pages_li, next, prev


    def get_visible_news(self, type_news, provider, from_date, to_date, page):
        try:
            page = int(page)
        except ValueError:
            return False

        news_db_query = News.query.filter(News.visible == True, db.func.date(News.date) >= from_date, db.func.date(News.date) <= to_date).order_by(News.id.desc())
        
        if type_news != 'all':
            try:
                type_news = int(type_news)
            except ValueError:
                return False
            if 1 < type_news > 10:
                return False
            else:
                if type_news == 10:
                    news_db_query = news_db_query.filter(News.news_type != 1)
                else:
                    news_db_query = news_db_query.filter(News.news_type == type_news)

        if provider != 'all':
            try:
                provider_id = int(provider)
            except ValueError:
                return False
            news_db_query = news_db_query.filter(News.provider_id == provider)

        constraints = Store_Provider.query.filter_by(store_id = self.user_id).all()
        for constraint in constraints:
            news_db_query = news_db_query.filter(News.provider_id != constraint.provider_id)

        quantity_news = news_db_query.count()

        news_db = news_db_query.paginate(page, 25, error_out=True)

        quantity_pages = int(math.ceil(quantity_news/25))

        pages_li = [x for x in range(1, quantity_pages + 1) if math.fabs(x - page) < 3]

        prev = False if page == 1 else True

        next = False if page == quantity_pages or quantity_pages == 0 else True

        news_types = self.get_news_types()

        news = []

        for new in news_db.items:
        # Convert time zone
            central = new.date.replace(tzinfo=self.from_zone).astimezone(self.to_zone)

            diction = {
                'id' : new.id,
                'datetime' : central.strftime("%d-%m-%Y %H:%M"),
                'visible' : new.visible,
                'news_type_int' : new.news_type
            }
            if new.news_type == 8:
                admin_message = Admin_Message.query.filter_by(news_id=new.id).first()
                diction.update({'news_type' : admin_message.tittle })
            elif new.news_type == 6:
                provider = Provider.query.get(new.provider_id)
                if self.user.role != 'PROVIDER':
                    diction.update({'news_type' : news_types[new.news_type] + ' 1$=' + str(provider.doll_to_uah) + ' грн. 1евро=' + str(provider.euro_to_uah) + ' грн.'})
                else:
                    diction.update({'news_type' : news_types[new.news_type]})
                diction.update({'provider_name' : provider.provider_name})
                diction.update({'provider_address' : provider.address})
            else:
                provider = Provider.query.get(new.provider_id)
                diction.update({'news_type' : news_types[new.news_type]})
                diction.update({'provider_name' : provider.provider_name})
                diction.update({'provider_address' : provider.address})

            if User.query.get(self.user_id).role == 'STORE':
                diction.update({'seen' : True if Seen_News.query.filter_by(store_id=self.user_id, news_id=new.id).first() else False})

            news.append(diction)

        return news, pages_li, next, prev


    def get_ids_from_news(self, ids_news):
        ids = dict()
        for id in ids_news:
            new = News.query.get(id)
            news_id = id
            if new.news_type == 1:
                ids_products = db.session.query(Product.id).filter(Product.news_product_id == news_id).all()
                for id_product in ids_products:
                    ids.update({id_product[0]:True})

            elif new.news_type == 2:
                ids_products = db.session.query(Product.id).filter(Product.id == Old_Product_Presence.product_id, Old_Product_Presence.news_id == news_id).all()
                for id_product in ids_products:
                    if not id_product[0] in ids:
                        ids.update({id_product[0]:False})

            elif new.news_type == 3:
                ids_products = db.session.query(Product.id).filter(Product.id == Old_Product_Cost.product_id, Old_Product_Cost.news_id == news_id).all()
                for id_product in ids_products:
                    if not id_product[0] in ids:
                        ids.update({id_product[0]:False})

            elif new.news_type == 4:
                ids_products = db.session.query(Product.id).filter(Product.id == New_Photo_Product.product_id, New_Photo_Product.news_id == news_id).all()
                for id_product in ids_products:
                    ids.update({id_product[0]:True})

            elif new.news_type == 5:
                ids_products = db.session.query(Product.id).filter(Product.id == Old_Product_Features.product_id, Old_Product_Features.news_id == news_id).all()
                for id_product in ids_products:
                    if not id_product[0] in ids:
                        ids.update({id_product[0]:False})

            elif new.news_type == 9:
                ids_products = db.session.query(Product.id).filter(Product.id == New_Articul_Product.product_id, New_Articul_Product.news_id == news_id).all()
                for id_product in ids_products:
                    ids.update({id_product[0]:True})

        return ids


    def get_providers_dates(self, provider_id):
        news_db = db.session.query(News).filter(News.news_type == 1, News.provider_id == provider_id).order_by(News.date.desc()).all()

        dates = []
        for new_db in news_db:
            central = new_db.date.replace(tzinfo=self.from_zone).astimezone(self.to_zone)
            dates.append(central.strftime("%Y-%m-%d"))

        dates = set(dates)
        return list(dates)


    def get_providers_dates_for_provider(self, provider_id):
        news_db = db.session.query(News).filter(News.news_type == 1, News.provider_id == provider_id).order_by(News.date.desc()).all()

        dates = []
        for new_db in news_db:
            central = new_db.date.replace(tzinfo=self.from_zone).astimezone(self.to_zone)
            dates.append(central.strftime("%d-%m-%Y"))

        return dates


    def get_products_upload_dates(self, provider_id):
        news_db = db.session.query(News).filter(News.news_type == 1, News.provider_id == provider_id).order_by(News.date.desc()).all()

        dates = []
        for new_db in news_db:
            dates.append(new_db.date.replace(tzinfo=self.from_zone).astimezone(self.to_zone))

        return dates


    def set_visible_news(self, ids, visible):

        for id in ids:
            new = News.query.get(id)

            if not new:
                return False

            new.visible = visible
            db.session.commit()

        return True


