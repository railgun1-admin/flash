from app.models import *
from dateutil import tz
import datetime
import math
from config import mail
from flask_mail import Message


class Flash_Feedback():
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')


    def new_feedback(self, name, phone, email, message):
        dttime = datetime.datetime.utcnow()
        feedback = Feed_Back(name=name, phone=phone, email=email, message=message, date=dttime)
        db.session.add(feedback)
        db.session.commit()

        # msg = Message("Обратная связь", sender='fleshka.studio@gmail.com', recipients=[Service_Setting.query.filter_by(setting_type='feedback_email').first().setting_value])

        # msg.body = name + "   " + phone + "   " + email + "   " + message
        # mail.send(msg)

        return 


    def get_feedback_dictionary(self, feedback, short=True):
        if short == True and len(feedback.message) > 20:
            message = feedback.message[:20] + '...'
        else:
            message = feedback.message

        feedback_dictionary = {
            'id' : feedback.id,
            'name' : feedback.name,
            'phone' : feedback.phone,
            'email' : feedback.email,
            'message' : message,
            'date' : feedback.date.replace(tzinfo=self.from_zone).astimezone(self.to_zone).strftime("%d-%m-%Y %H:%M")
        }
        return feedback_dictionary


    def get_all_feedbacks(self, page):
        quantity_feedbacks = Feed_Back.query.count()

        feedbacks_db = Feed_Back.query.order_by(Feed_Back.id.desc()).paginate(page, 10, error_out=True)

        quantity_pages = int(math.ceil(quantity_feedbacks/10))

        pages_li = [x for x in range(1, quantity_pages + 1) if math.fabs(x - page) < 3]

        prev = False if page == 1 else True

        next = False if page == quantity_pages or quantity_pages == 0 else True

        feedbacks = list()
        for feedback_db in feedbacks_db.items:
            feedbacks.append(self.get_feedback_dictionary(feedback_db))

        return feedbacks, next, prev, pages_li
