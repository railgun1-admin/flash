from app.models import *
from app.flash_lib.flash_user import Flash_User
import itertools


class Flash_Setting():


    def get_providers_settings(self):
        providers_settings_list = ['disp_provs_adrs', 'disp_provs_doll', 'disp_provs_euro', 'disp_provs_phon', 'disp_provs_vibr', 'disp_provs_mail']
        providers_settings = dict()
        for provider_setting in providers_settings_list:
            providers_settings.update({provider_setting:Service_Setting.query.filter_by(setting_type=provider_setting).first()})

        return providers_settings


    def get_all_options(self):
        options_db_brand = Option.query.filter_by(column_type='brand').order_by(Option.column_option).all()
        options_db_currency = Option.query.filter_by(column_type='currency').order_by(Option.column_option).all()
        options_db_sex = Option.query.filter_by(column_type='sex').order_by(Option.column_option).all()
        options_db_group = Option.query.filter_by(column_type='group').order_by(Option.column_option).all()
        options_db_type = Option.query.filter_by(column_type='type').order_by(Option.column_option).all()
        options_db_colour = Option.query.filter_by(column_type='colour').order_by(Option.column_option).all()
        options_db_season = Option.query.filter_by(column_type='season').order_by(Option.column_option).all()
        options_db_presence = Option.query.filter_by(column_type='presence').order_by(Option.column_option).all()
        options_db_min_order = Option.query.filter_by(column_type='min_order').order_by(Option.column_option).all()
        options_db_size = Option.query.filter_by(column_type='size').order_by(Option.column_option).all()
        options_db_repeat = Option.query.filter_by(column_type='repeat').order_by(Option.column_option).all()
        options_db_manufacturer = Option.query.filter_by(column_type='manufacturer').order_by(Option.column_option).all()
        options_db_box = Option.query.filter_by(column_type='box').order_by(Option.column_option).all()
        options_db_material_outside = Option.query.filter_by(column_type='material_outside').order_by(Option.column_option).all()
        options_db_material_inside = Option.query.filter_by(column_type='material_inside').order_by(Option.column_option).all()
        options_db_material_insole = Option.query.filter_by(column_type='material_insole').order_by(Option.column_option).all()
        

        options_first_db = itertools.zip_longest(options_db_currency, options_db_sex, options_db_type, 
            options_db_season, options_db_presence, options_db_min_order, options_db_box)

        options_second_db = itertools.zip_longest(options_db_group, options_db_colour, options_db_brand, options_db_size, options_db_repeat,
            options_db_manufacturer, options_db_material_outside, options_db_material_inside, options_db_material_insole)

        options_first = []
        options_second = []

        for option_db_currency, option_db_sex, option_db_type, option_db_season, option_db_presence, option_db_min_order, option_db_box in options_first_db:

            opt_dict = {
                'currency' : option_db_currency.column_option if option_db_currency else None,
                'currency_id' : option_db_currency.id if option_db_currency else None,

                'sex' : option_db_sex.column_option if option_db_sex else None,
                'sex_id' : option_db_sex.id if option_db_sex else None,

                'type' : option_db_type.column_option if option_db_type else None,
                'type_id' : option_db_type.id if option_db_type else None,

                'season' : option_db_season.column_option if option_db_season else None,
                'season_id' : option_db_season.id if option_db_season else None,

                'presence' : option_db_presence.column_option if option_db_presence else None,
                'presence_id' : option_db_presence.id if option_db_presence else None,

                'min_order' : option_db_min_order.column_option if option_db_min_order else None,
                'min_order_id' : option_db_min_order.id if option_db_min_order else None,

                'box' : option_db_box.column_option if option_db_box else None,
                'box_id' : option_db_box.id if option_db_box else None

            }
            options_first.append(opt_dict)

        for option_db_group, option_db_colour, option_db_brand, option_db_size, option_db_repeat, option_db_manufacturer, option_db_material_outside, option_db_material_inside, option_db_material_insole in options_second_db:

            opt_dict = {
                'group' : option_db_group.column_option if option_db_group else None,
                'group_id' : option_db_group.id if option_db_group else None,

                'colour' : option_db_colour.column_option if option_db_colour else None,
                'colour_id' : option_db_colour.id if option_db_colour else None,

                'brand' : option_db_brand.column_option if option_db_brand else None,
                'brand_id' : option_db_brand.id if option_db_brand else None,

                'size' : option_db_size.column_option if option_db_size else None,
                'size_id' : option_db_size.id if option_db_size else None,

                'repeat' : option_db_repeat.column_option if option_db_repeat else None,
                'repeat_id' : option_db_repeat.id if option_db_repeat else None,

                'manufacturer' : option_db_manufacturer.column_option if option_db_manufacturer else None,
                'manufacturer_id' : option_db_manufacturer.id if option_db_manufacturer else None,

                'material_outside' : option_db_material_outside.column_option if option_db_material_outside else None,
                'material_outside_id' : option_db_material_outside.id if option_db_material_outside else None,

                'material_inside' : option_db_material_inside.column_option if option_db_material_inside else None,
                'material_inside_id' : option_db_material_inside.id if option_db_material_inside else None,

                'material_insole' : option_db_material_insole.column_option if option_db_material_insole else None,
                'material_insole_id' : option_db_material_insole.id if option_db_material_insole else None

            }
            options_second.append(opt_dict)

        return options_first, options_second


    def init_settings_and_admin(self):
            flash_user = Flash_User()
            flash_user.add_user_admin(user_name='admin', password='admin', admin_name='admin', admin_rights='HIGH') 
            option = Option(column_type='currency', column_option='грн')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='currency', column_option='$')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='currency', column_option='евро')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='colour', column_option='красный')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='colour', column_option='черный')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='colour', column_option='синий')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='colour', column_option='белый')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='colour', column_option='бежевый')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='colour', column_option='бордовый')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='colour', column_option='мята')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='colour', column_option='розовый')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='group', column_option='ботинки')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='group', column_option='туфли')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='group', column_option='сапоги')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='group', column_option='кеды')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='group', column_option='кроссовки')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='sex', column_option='мальчик')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='sex', column_option='девочка')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='sex', column_option='унисекс')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='type', column_option='мужская')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='type', column_option='женская')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='type', column_option='детская')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='season', column_option='зима')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='season', column_option='лето')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='season', column_option='весна')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='season', column_option='осень')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='season', column_option='деми')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='presence', column_option='есть')
            db.session.add(option)
            db.session.commit()
            option = Option(column_type='presence', column_option='нет')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='manufacturer', column_option='китай')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='size', column_option='36-40')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='box', column_option='8')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='repeat', column_option='37-38-39')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='min_order', column_option='8')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='material_outside', column_option='замш иск')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='material_outside', column_option='кожзам')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='material_outside', column_option='джинс')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='material_outside', column_option='замш иск/кожзам')
            db.session.add(option)
            db.session.commit() 
            option = Option(column_type='material_inside', column_option='текстиль')
            db.session.add(option)
            db.session.commit() 
            service_setting = Service_Setting(setting_type='news_active_days', setting_value='10')
            db.session.add(service_setting)
            db.session.commit() 
            service_setting = Service_Setting(setting_type='disp_provs_adrs', setting_value='True')
            db.session.add(service_setting)
            db.session.commit()
            service_setting = Service_Setting(setting_type='disp_provs_doll', setting_value='True')
            db.session.add(service_setting)
            db.session.commit()
            service_setting = Service_Setting(setting_type='disp_provs_euro', setting_value='True')
            db.session.add(service_setting)
            db.session.commit()
            service_setting = Service_Setting(setting_type='disp_provs_phon', setting_value='True')
            db.session.add(service_setting)
            db.session.commit()
            service_setting = Service_Setting(setting_type='disp_provs_vibr', setting_value='True')
            db.session.add(service_setting)
            db.session.commit()
            service_setting = Service_Setting(setting_type='disp_provs_mail', setting_value='True')
            db.session.add(service_setting)
            db.session.commit()
            service_setting = Service_Setting(setting_type='feedback_email', setting_value='test@mail.com')
            db.session.add(service_setting)
            db.session.commit()
            service_setting = Service_Setting(setting_type='prod_id', setting_value='1')
            db.session.add(service_setting)
            db.session.commit()