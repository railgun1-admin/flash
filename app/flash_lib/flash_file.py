import xlsxwriter
import itertools
from app.models import *
from .flash_product import Flash_Product
from random import randint
from datetime import datetime
import os
import zipfile
from dateutil import tz
from dateutil.relativedelta import relativedelta
from fpdf import FPDF
import csv
import shutil
from PIL import Image, ImageEnhance


class Flash_File():
    server_zone = 'UTC'
    user_zone = 'Europe/Kiev'
    temp_directory = '/home/dev/flash/files/temporary'


    def __init__(self, user_id):
        self.user_id = user_id
        self.flash_product = Flash_Product(user_id)


    def set_user_zone(self, zone):
        self.user_zone = zone


    def set_temp_directory(self, path):
        self.temp_directory = path


    def get_rand_number(self, extension):
        rand_number = randint(1, 1000000)
        while os.path.isfile("{}/{}{}".format(self.temp_directory, rand_number, extension)) or os.path.isdir("{}/{}".format(self.temp_directory, rand_number)):
            rand_number = randint(1, 1000000)

        return rand_number


    def add_watermark_photo(self, image, watermark, opacity=1, wm_interval=0):
        assert opacity >= 0 and opacity <= 1
        if opacity < 1:
            if watermark.mode != 'RGBA':
                watermark = watermark.convert('RGBA')
            else:
                watermark = watermark.copy()
            alpha = watermark.split()[3]
            alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
            watermark.putalpha(alpha)
        layer = Image.new('RGBA', image.size, (0,0,0,0))
        watermark = watermark.resize((image.size[0], image.size[1]))
        layer.paste(watermark, (0, 0))
        return Image.composite(layer,  image,  layer)


    # get_store_csv
    def get_store_csv(self, ids, user_encoding, user_delimiter, photo):
        user = User.query.get(self.user_id)

        rand_number = self.get_rand_number('.zip')
        directory = "{}/{}".format(self.temp_directory, rand_number)

        os.makedirs(directory)

        provider_number = 0
        articul_number = 0

        photo_dir = "/home/dev/flash/files/photographs/"

        path = '/home/dev/flash/files/users/' + user.user_name + '/'

        f = open(directory + "/products.csv", "w", encoding=user_encoding)

        out = csv.writer(f, delimiter=user_delimiter, quoting=csv.QUOTE_ALL)

        field_names = Download_Field.query.filter_by(user_id=self.user_id).order_by(Download_Field.number_column).all()

        header = []
        j = 0
        for field_name in field_names:
            if field_name.column_type == 'articul':
                articul_number = j
            elif field_name.column_type == 'provider_name':
                provider_number = j

            header.append(field_name.name_column)

            j += 1

        out.writerow(header)

        if photo > 0:
            user_store_info = User_Store_Info.query.get(self.user_id)
            if not user_store_info.without_watermark:
                watermark = Image.open("/home/dev/flash/files/watermarks/" + str(user.id) + '.png')

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)

        all_data = []

        for id in ids:

            diction = self.flash_product.get_products_by_id(id)

            photos = self.flash_product.get_photos_by_product_id(diction.get('id'))

            data = []

            for field_name in field_names:
                if field_name.column_type != 'photo_1' and field_name.column_type != 'photo_2' and field_name.column_type != 'photo_3':
                    data.append(diction.get(field_name.column_type))
                else:
                    if photo == 1:

                        if field_name.column_type == 'photo_1':
                            if not photos[0] is None and ids[id]:
                                if user_store_info.without_watermark:
                                    shutil.copy2(photo_dir + photos[0].url, directory + '/' + photos[0].url)
                                else:
                                    image = Image.open(photo_dir + photos[0].url)
                                    image = self.add_watermark_photo(image, watermark)
                                    image.save(directory + '/' + photos[0].url)
                                data.append(photos[0].url)
                            else:
                                data.append('')
                        elif field_name.column_type == 'photo_2':
                            if len(photos) > 1 and ids[id]:
                                if user_store_info.without_watermark:
                                    shutil.copy2(photo_dir + photos[1].url, directory + '/' + photos[1].url)
                                else:
                                    image = Image.open(photo_dir + photos[1].url)
                                    image = self.add_watermark_photo(image, watermark)
                                    image.save(directory + '/' + photos[1].url)
                                data.append(photos[1].url)
                            else:
                                data.append("")
                        elif field_name.column_type == 'photo_3':
                            if len(photos) > 2 and ids[id]:
                                if user_store_info.without_watermark:
                                    shutil.copy2(photo_dir + photos[2].url, directory + '/' + photos[2].url)
                                else:
                                    image = Image.open(photo_dir + photos[2].url)
                                    image = self.add_watermark_photo(image, watermark)
                                    image.save(directory + '/' + photos[2].url)
                                data.append(photos[2].url)
                            else:
                                data.append("")

                    elif photo == 2:
                        if field_name.column_type == 'photo_1':
                            if not photos[0] is None:
                                if user_store_info.without_watermark:
                                    shutil.copy2(photo_dir + photos[0].url, path  + photos[0].url)
                                else:
                                    image = Image.open(photo_dir + photos[0].url)
                                    image = self.add_watermark_photo(image, watermark)
                                    image.save(path  + photos[0].url)
                                data.append('http://5.101.179.97/model/' + user.user_name + '/' + photos[0].url)
                            else:
                                data.append("")
                        elif field_name.column_type == 'photo_2':
                            if len(photos) > 1:
                                if user_store_info.without_watermark:
                                    shutil.copy2(photo_dir + photos[1].url, path  + photos[1].url)
                                else:
                                    image = Image.open(photo_dir + photos[1].url)
                                    image = self.add_watermark_photo(image, watermark)
                                    image.save(path + photos[1].url)
                                data.append('http://5.101.179.97/model/' + user.user_name + '/' + photos[1].url)
                            else:
                                data.append("")
                        elif field_name.column_type == 'photo_3':
                            if len(photos) > 2:
                                if user_store_info.without_watermark:
                                    shutil.copy2(photo_dir + photos[2].url, path  + photos[2].url)
                                else:
                                    image = Image.open(photo_dir + photos[2].url)
                                    image = self.add_watermark_photo(image, watermark)
                                    image.save(path + photos[2].url)
                                data.append('http://5.101.179.97/model/' + user.user_name + '/' + photos[2].url)
                            else:
                                data.append("")

                    else:
                        data.append("")

            all_data.append(data)

        all_data.sort(key=lambda all_data: (all_data[provider_number], all_data[articul_number]))

        for i in range(0, len(all_data)):
            out.writerow(all_data[i])

        f.close()

        return rand_number

    # get_store_xlsx
    def get_store_xlsx(self, ids, thumbs, photo):
        user = User.query.get(self.user_id)

        rand_number = self.get_rand_number('.zip')
        directory = "{}/{}".format(self.temp_directory, rand_number)

        os.makedirs(directory)

        file_name = "{}/products.xlsx".format(directory)

        photo_dir = "/home/dev/flash/files/photographs/"

        workbook = xlsxwriter.Workbook(file_name)
        worksheet = workbook.add_worksheet()

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)

        field_names = Download_Field.query.filter_by(user_id=self.user_id).order_by(Download_Field.number_column).all()

        if photo:
            user_store_info = User_Store_Info.query.get(self.user_id)
            if not user_store_info.without_watermark:
                watermark = Image.open("/home/dev/flash/files/watermarks/" + str(user.id) + '.png')

        quantity_fields = len(field_names)
        xl_letter = xlsxwriter.utility.xl_col_to_name(quantity_fields)
        worksheet.set_column("A:{}".format(xl_letter), 15)

        if thumbs:
            worksheet.write('A1', 'Изображение')

        for field_name in field_names:
            if not field_name.column_type in ['photo_1', 'photo_2', 'photo_3']:
                xl_letter = xlsxwriter.utility.xl_col_to_name(field_name.number_column - 1*(not thumbs)) 
                worksheet.write('{}1'.format(xl_letter), field_name.name_column)
            elif photo:
                xl_letter = xlsxwriter.utility.xl_col_to_name(field_name.number_column - 1*(not thumbs)) 
                worksheet.write('{}1'.format(xl_letter), field_name.name_column)

        goods = []
        for id in ids:
            goods.append(self.flash_product.get_products_by_id(id))

        goods.sort(key=lambda goods: (goods['provider_name'], goods['articul']))

        for i in range(0, len(goods)):
            worksheet.set_row(i + 1, 80)
            diction = goods[i]
            if thumbs:
                worksheet.insert_image('A' + str(i+2), "/home/dev/flash/files/thumbnails/" + diction.get('thumbnail'))

            if photo:
                photos = self.flash_product.get_photos_by_product_id(diction.get('id'))

            for field_name in field_names:
                if not field_name.column_type in ['photo_1', 'photo_2', 'photo_3']:
                    worksheet.write(xlsxwriter.utility.xl_col_to_name(field_name.number_column - 1*(not thumbs))  + str(i+2), diction.get(field_name.column_type))
                else:
                    if photo:

                        if field_name.column_type == 'photo_1' and not photos[0] is None and ids[diction.get('id')]:
                            if user_store_info.without_watermark:
                                shutil.copy2(photo_dir + photos[0].url, directory + '/' + photos[0].url)
                                worksheet.write(xlsxwriter.utility.xl_col_to_name(field_name.number_column - 1*(not thumbs))  + str(i+2), photos[0].url)
                            else:
                                image = Image.open(photo_dir + photos[0].url)
                                image = self.add_watermark_photo(image, watermark)
                                image.save(directory + '/' + photos[0].url)
                                worksheet.write(xlsxwriter.utility.xl_col_to_name(field_name.number_column - 1*(not thumbs))  + str(i+2), photos[0].url)
                
                        elif field_name.column_type == 'photo_2' and len(photos) > 1 and ids[diction.get('id')]:
                            if user_store_info.without_watermark:
                                shutil.copy2(photo_dir + photos[1].url, directory + '/' + photos[1].url)
                                worksheet.write(xlsxwriter.utility.xl_col_to_name(field_name.number_column - 1*(not thumbs))  + str(i+2), photos[1].url)
                            else:
                                image = Image.open(photo_dir + photos[1].url)
                                image = self.add_watermark_photo(image, watermark)
                                image.save(directory + '/' + photos[1].url)
                                worksheet.write(xlsxwriter.utility.xl_col_to_name(field_name.number_column - 1*(not thumbs))  + str(i+2), photos[1].url)
                        elif field_name.column_type == 'photo_3' and len(photos) > 2 and ids[diction.get('id')]:
                            if user_store_info.without_watermark:
                                shutil.copy2(photo_dir + photos[2].url, directory + '/' + photos[2].url)
                                worksheet.write(xlsxwriter.utility.xl_col_to_name(field_name.number_column - 1*(not thumbs))  + str(i+2), photos[2].url)
                            else:
                                image = Image.open(photo_dir + photos[2].url)
                                image = self.add_watermark_photo(image, watermark)
                                image.save(directory + '/' + photos[2].url)
                                worksheet.write(xlsxwriter.utility.xl_col_to_name(field_name.number_column - 1*(not thumbs))  + str(i+2), photos[2].url)
                            


        workbook.close()

        return rand_number


    def get_products_archive(self, ids):
        download_type = Download_Type.query.get(self.user_id)

        down_type = download_type.download_type
        code_type = download_type.coding
        deli_type = download_type.delimiter

        temp_ids = {}
        if not type(ids) is dict:
            for id in ids:
                temp_ids.update({id:True})

            ids = temp_ids

        if down_type == 1:
            rand_number = self.get_store_csv(ids, code_type, deli_type, 0)
        elif down_type == 2:
            rand_number = self.get_store_csv(ids, code_type, deli_type, 1)
        elif down_type == 3:
            rand_number = self.get_store_csv(ids, code_type, deli_type, 2)
        elif down_type == 4:
            rand_number = self.get_store_xlsx(ids, False, False)
        elif down_type == 5:
            rand_number = self.get_store_xlsx(ids, True, False)
        elif down_type == 6:
            rand_number = self.get_store_xlsx(ids, False, True)
        elif down_type == 7:
            rand_number = self.get_store_xlsx(ids, True, True)

        return rand_number



    def get_xlsx(self, ids):
        rand_number = self.get_rand_number('.xlsx')
        file_name = "{}/{}.xlsx".format(self.temp_directory, rand_number)

        workbook = xlsxwriter.Workbook(file_name)
        worksheet = workbook.add_worksheet()

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)

        field_names = Download_Field.query.filter_by(user_id=self.user_id).order_by(Download_Field.number_column).all()

        quantity_fields = len(field_names)
        xl_letter = xlsxwriter.utility.xl_col_to_name(quantity_fields)
        worksheet.set_column("A:{}".format(xl_letter), 20)
        
        worksheet.write('A1', 'Изображение')

        for field_name in field_names:
            if not field_name.column_type in ['photo_1', 'photo_2', 'photo_3']:
                xl_letter = xlsxwriter.utility.xl_col_to_name(field_name.number_column) 
                worksheet.write('{}1'.format(xl_letter), field_name.name_column)

        for i in range(0, len(ids)):
            worksheet.set_row(i + 1, 80)
            diction = self.flash_product.get_products_by_id(ids[i])
            worksheet.insert_image('A' + str(i+2), "/home/dev/flash/files/thumbnails/" + diction.get('thumbnail'))


            for field_name in field_names:
                if not field_name.column_type in ['photo_1', 'photo_2', 'photo_3']:
                    worksheet.write(xlsxwriter.utility.xl_col_to_name(field_name.number_column)  + str(i+2), diction.get(field_name.column_type))


        workbook.close()

        return file_name


    def get_pdf(self, ids, seasons, presences, groups, sorting, from_date, to_date, double_page):

        rand_number = self.get_rand_number('.pdf')
        file_name = "{}/{}.pdf".format(self.temp_directory, rand_number)

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)

        pdf = FPDF()
        pdf.set_left_margin(5)
        pdf.add_font('Sans', '', '/home/dev/flash/static/fonts/OpenSans-Regular.ttf', uni=True)

        for id in ids:
            provider = Provider.query.get(id)
            if provider is None:
                return False

            pdf.add_page()
            
            pdf.image('/home/dev/flash/static/img/logo_pdf.jpg', x = None, y = None, w = 20, h = 20)
            pdf.set_font('Sans', '', 14)
            pdf.set_y(10)
            pdf.cell(w=70, h=20, txt='Fleshka.od.ua', align='R', border=0)
            pdf.set_font('Sans', '', 12)
            pdf.set_text_color(r = 121, g = 60, b = 150)
            pdf.cell(w=20, h=20, txt='VIBER', align='R', border=0)
            pdf.set_text_color(r = 0, g = -1, b = -1)
            pdf.cell(w=35, h=20, txt='+380992267962', align='', border=0)
            pdf.set_text_color(r = 0, g = 180, b = 0)
            pdf.cell(w=20, h=20, txt='WeChat', align='', border=0)
            pdf.set_text_color(r = 0, g = -1, b = -1)
            pdf.cell(w=53, h=20, txt=datetime.utcnow().replace(tzinfo=from_zone).astimezone(to_zone).strftime("%d-%m-%Y %H:%M:%S"), align='R', border=0)
            pdf.set_font('Sans', '', 8)
            pdf.ln()

            products_query = db.session.query(Product, Shoes).filter(Product.provider_id == id, db.func.lower(Product.presence).in_(presences),
                Product.id == Shoes.id, db.func.lower(Shoes.season).in_(seasons), db.func.lower(Shoes.group).in_(groups), db.func.date(Product.date) <= to_date, Product.date >= from_date)

            for sort in sorting:
                products_query = products_query.order_by(sort)

            products = products_query.all()

            first_column = []
            second_column = []
            for i in range(0, len(products)):
                if int(i/57) % 2 == 0:
                    first_column.append(products[i])
                else:
                    second_column.append(products[i])

            products_tuple = itertools.zip_longest(first_column, second_column)

            i = 0
            for product_tuple in products_tuple:
                if i % 57 == 0:
                    if i != 0:
                        pdf.add_page()

                    pdf.cell(w=139, h=10, txt=provider.provider_name + " " + provider.address, align='C', border=1)
                    pdf.cell(w=59, h=10, txt='Лист ' + str(int(i/57) + 1), align='R', border=1)
                    pdf.ln()

                    for j in range(0, 2):
                        pdf.cell(w=30, h=5, txt='Артикул', align='C', border=1)
                        pdf.cell(w=10, h=5, txt='Разм.', align='C', border=1)
                        pdf.cell(w=7, h=5, txt='П.', align='C', border=1)
                        pdf.cell(w=7, h=5, txt='В.', align='C', border=1)
                        pdf.cell(w=10, h=5, txt='Цена', align='C', border=1)
                        pdf.cell(w=10, h=5, txt='Сайт', align='C', border=1)
                        pdf.cell(w=25, h=5, txt='NEW цена', align='C', border=1)
                        if j == 0:
                            pdf.set_line_width(0.8)
                            pdf.line(x1=pdf.get_x(), y1=pdf.get_y(), x2=pdf.get_x(), y2=pdf.get_y()+5)
                            pdf.set_line_width(0.2)

                    pdf.ln()

                for z in range(0, 2):
                    if product_tuple[z] is None:
                        continue
                    product, shoe = product_tuple[z]
                    pdf.cell(w=30, h=4, txt=product.articul[:16], border=1)
                    pdf.cell(w=10, h=4, txt=shoe.size, border=1)
                    pdf.cell(w=7, h=4, txt=shoe.box, border=1)
                    pdf.cell(w=7, h=4, txt=product.currency, border=1)
                    pdf.cell(w=10, h=4, txt=str(product.cost), border=1)
                    pdf.cell(w=10, h=4, txt=str(product.cost_site), border=1)
                    pdf.cell(w=25, h=4, txt='', border=1)
                    if z == 0:
                        pdf.set_line_width(0.8)
                        pdf.line(x1=pdf.get_x(), y1=pdf.get_y(), x2=pdf.get_x(), y2=pdf.get_y()+4)
                        pdf.set_line_width(0.2)

                pdf.ln()
                i += 1

            if double_page and (int(((i-1)/57)) + 1) % 2 == 1 and len(ids) > 1:
                pdf.add_page()

        pdf.output(file_name, 'F')
        return file_name


    def get_pdf_with_photo(self, ids, seasons, presences, groups, sorting, from_date, to_date, double_page):

        rand_number = self.get_rand_number('.pdf')
        file_name = "{}/{}.pdf".format(self.temp_directory, rand_number)

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)

        pdf = FPDF()
        pdf.set_left_margin(5)
        pdf.set_auto_page_break(False, 0.0)
        pdf.add_font('Sans', '', '/home/dev/flash/static/fonts/OpenSans-Regular.ttf', uni=True)

        for id in ids:
            provider = Provider.query.get(id)
            if provider is None:
                return False

            pdf.add_page()
            
            pdf.image('/home/dev/flash/static/img/logo_pdf.jpg', x = None, y = None, w = 20, h = 20)
            pdf.set_font('Sans', '', 14)
            pdf.set_y(10)
            pdf.cell(w=70, h=20, txt='Fleshka.od.ua', align='R', border=0)
            pdf.set_font('Sans', '', 12)
            pdf.set_text_color(r = 121, g = 60, b = 150)
            pdf.cell(w=20, h=20, txt='VIBER', align='R', border=0)
            pdf.set_text_color(r = 0, g = -1, b = -1)
            pdf.cell(w=35, h=20, txt='+380992267962', align='', border=0)
            pdf.set_text_color(r = 0, g = 180, b = 0)
            pdf.cell(w=20, h=20, txt='WeChat', align='', border=0)
            pdf.set_text_color(r = 0, g = -1, b = -1)
            pdf.cell(w=53, h=20, txt=datetime.utcnow().replace(tzinfo=from_zone).astimezone(to_zone).strftime("%d-%m-%Y %H:%M:%S"), align='R', border=0)
            pdf.set_font('Sans', '', 16)
            pdf.ln()

            products_query = db.session.query(Product, Shoes).filter(Product.provider_id == id, db.func.lower(Product.presence).in_(presences),
                Product.id == Shoes.id, db.func.lower(Shoes.season).in_(seasons), db.func.lower(Shoes.group).in_(groups), db.func.date(Product.date) <= to_date, Product.date >= from_date)

            for sort in sorting:
                products_query = products_query.order_by(sort)

            products = products_query.all()


            i = 0
            for product, shoe in products:
                if i == 0 or (i == 12) or ((i - 12) % 13 == 0 and i != 13):
                    if i != 0:
                        pdf.add_page()

                    page_num = 0 if i == 12 else int((i - 12) % 13)

                    pdf.cell(w=138, h=10, txt=provider.provider_name + " " + provider.address, align='C', border=1)
                    pdf.cell(w=60, h=10, txt='Лист ' + str(page_num + 1), align='R', border=1)
                    pdf.ln()

                    pdf.cell(w=20, h=10, txt='Фото', align='C', border=1)
                    pdf.cell(w=50, h=10, txt='Артикул', align='C', border=1)
                    pdf.cell(w=20, h=10, txt='Разм.', align='C', border=1)
                    pdf.cell(w=14, h=10, txt='П.', align='C', border=1)
                    pdf.cell(w=14, h=10, txt='В.', align='C', border=1)
                    pdf.cell(w=20, h=10, txt='Цена', align='C', border=1)
                    pdf.cell(w=20, h=10, txt='Сайт', align='C', border=1)
                    pdf.cell(w=40, h=10, txt='NEW цена', align='C', border=1)

                    pdf.ln()


                y = pdf.get_y()
                x = pdf.get_x()
                pdf.image('/home/dev/flash/files/thumbnails/' + product.thumbnail, x = None, y = None, w = 20, h = 20)
                pdf.set_x(x+10)
                pdf.set_y(y)
                pdf.cell(w=20, h=20, txt="", border=1)
                pdf.cell(w=50, h=20, txt=product.articul, border=1)
                pdf.cell(w=20, h=20, txt=shoe.size, border=1)
                pdf.cell(w=14, h=20, txt=shoe.box, border=1)
                pdf.cell(w=14, h=20, txt=product.currency, border=1)
                pdf.cell(w=20, h=20, txt=str(product.cost), border=1)
                pdf.cell(w=20, h=20, txt=str(product.cost_site), border=1)
                pdf.cell(w=40, h=20, txt='', border=1)

                pdf.ln()
                i += 1

            if double_page and (((len(ids) - 12) % 13) > 1 or len(ids) < 13):
                pdf.add_page()

        pdf.output(file_name, 'F')
        return file_name


    def get_zip_photo(self, ids):
        user = User.query.get(self.user_id)
        user_provider_info = User_Provider_Info.query.get(user.id)

        rand_number = self.get_rand_number('.zip')
        directory = "{}/{}".format(self.temp_directory, rand_number)

        os.makedirs(directory)

        photo_dir = "/home/dev/flash/files/photographs/"

        for id in ids:
            product = Product.query.get(id)

            if product.provider_id != user_provider_info.provider_id:
                continue

            photos = self.flash_product.get_photos_by_product_id(id)
            
            for i in range(0, len(photos)):
                if i == 0:
                    photo_name = directory + '/' + product.articul.replace(' ', '_') + ".jpg"
                else:
                    photo_name = directory + '/' + product.articul.replace(' ', '_') + '(' + str(i) + ").jpg"

                shutil.copy2(photo_dir + photos[i].url, photo_name)

        return rand_number


    def get_zip(self, ids):

        user = User.query.get(self.user_id)

        rand_number = self.get_rand_number('.zip')
        directory = "{}/{}".format(self.temp_directory, rand_number)

        os.makedirs(directory)

        photo_dir = "/home/dev/flash/files/photographs/"

        f = open(directory + "/products.csv","w")

        out = csv.writer(f, delimiter=',', quoting=csv.QUOTE_ALL)

        field_names = Download_Field.query.filter_by(user_id=self.user_id).order_by(Download_Field.number_column).all()

        header = []
        for field_name in field_names:
            header.append(field_name.name_column)

        out.writerow(header)

        if user.role == "STORE":
            watermark = Image.open("/home/dev/flash/files/watermarks/" + user.user_name + '.png')

        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)

        for i in range(0, len(ids)):

            diction = self.flash_product.get_products_by_id(ids[i])

            photos = self.flash_product.get_photos_by_product_id(diction.get('id'))

            data = []

            for field_name in field_names:
                if field_name.column_type != 'photo_1':
                    data.append(diction.get(field_name.column_type))
                else:
                    photo_data = []
                    for photo in photos:
                        image = Image.open(photo_dir + photo.url)
                        if user.role == "STORE":
                            image = self.add_watermark_photo(image, watermark)
                        image.save(directory + '/' + photo.url)
                        data.append(photo.url)
            out.writerow(data)

        f.close()

        return rand_number

    def get_zip_unload(self, provider_id, seasons, groups, presences, from_date, to_date, thumbs):
        from_zone = tz.gettz(self.server_zone)
        to_zone = tz.gettz(self.user_zone)

        from_date = datetime.strptime(from_date, "%Y-%m-%d").replace(tzinfo=to_zone)
        from_date -= relativedelta(days=1)
        from_date = from_date.replace(hour=22)
        to_date = datetime.strptime(to_date, "%Y-%m-%d").replace(tzinfo=to_zone).date()

        products = db.session.query(Product, Shoes).filter(db.func.lower(Product.presence).in_(presences), Product.id == Shoes.id, Product.provider_id == int(provider_id),
            db.func.lower(Shoes.season).in_(seasons), db.func.lower(Shoes.group).in_(groups), Product.date >= from_date, db.func.date(Product.date) <= to_date).all()

        uploads_fields = Upload_Field.query.filter_by(user_id=self.user_id).all()

        if thumbs:
            rand_number = self.get_rand_number('.zip')
            directory = "{}/{}".format(self.temp_directory, rand_number)
            os.makedirs(directory)

            file_name = "{}/products.xlsx".format(directory)

            workbook = xlsxwriter.Workbook(file_name)
            worksheet = workbook.add_worksheet()

            quantity_fields = len(uploads_fields)
            xl_letter = xlsxwriter.utility.xl_col_to_name(quantity_fields)
            worksheet.set_column("A:{}".format(xl_letter), 20)
        
            worksheet.write('A1', 'Изображение')

            for i in range(0, len(uploads_fields)):
                xl_letter = xlsxwriter.utility.xl_col_to_name(i+1) 
                worksheet.write('{}1'.format(xl_letter), uploads_fields[i].column_name)

            for i in range(0, len(products)):
                product, shoe = products[i]
                worksheet.set_row(i + 1, 80)
                diction = self.flash_product.get_product_by_objects(product, shoe, from_zone, to_zone)
                worksheet.insert_image('A' + str(i+2), "/home/dev/flash/files/thumbnails/" + diction.get('thumbnail'))

                for j in range(0, len(uploads_fields)):
                    if not uploads_fields[j].column_type in ['photo_1', 'photo_2', 'photo_3']:
                        worksheet.write(xlsxwriter.utility.xl_col_to_name(j+1) + str(i+2), diction.get(uploads_fields[j].column_type))
                    else:
                        worksheet.write(xlsxwriter.utility.xl_col_to_name(j+1) + str(i+2), "")

            workbook.close()

            return rand_number

        else:
            rand_number = self.get_rand_number('.zip')
            directory = "{}/{}".format(self.temp_directory, rand_number)
            os.makedirs(directory)
            photo_dir = "/home/dev/flash/files/photographs/"
            f = open(directory + "/products.csv","w")
            out = csv.writer(f, delimiter=',', quoting=csv.QUOTE_ALL)
            header = []
            for upload_field in uploads_fields:
                header.append(upload_field.column_name)
            out.writerow(header)
            for product, shoe in products:
                diction = self.flash_product.get_product_by_objects(product, shoe, from_zone, to_zone)
                data = []
                for upload_field in uploads_fields:
                    if not upload_field.column_type in ['photo_1', 'photo_2', 'photo_3']:
                        data.append(diction.get(upload_field.column_type))
                    else:
                        data.append("")
                out.writerow(data)
            f.close()

        return rand_number
