import xlsxwriter
import itertools
from app.models import *
from .flash_product import Flash_Product
from random import randint
import datetime
import os
import zipfile
import math
import json
from dateutil import tz
from fpdf import FPDF


class Flash_Locale():

    def __init__(self):
        self.data = json.load(open('static/fonts/language.json'))
        

    def get_text(self, locale, key):

        text = self.data.get(locale).get(key)
        if not text:
            text = key

        return text