from flask import Blueprint, redirect, url_for, render_template, abort, send_file, after_this_request, request
from flask_login import login_user, current_user, logout_user, AnonymousUserMixin
from app.models import *
from config import login_manager
from functools import wraps
from app.flash_lib.flash_product import Flash_Product
from app.flash_lib.flash_locale import Flash_Locale
from app.flash_lib.flash_news import Flash_News
from app.flash_lib.flash_file import Flash_File
from dateutil import tz
from dateutil.relativedelta import relativedelta
import datetime
import shutil
import os
import hashlib

provider = Blueprint('provider', __name__, template_folder='../../templates/provider')

flash_locale = Flash_Locale()


def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def get_options(column_type):
    options_db = Option.query.filter_by(column_type=column_type).order_by(Option.column_option).all()
    options = []
    for option_db in options_db:
        options.append(option_db.column_option)
    return options


def get_max_entity_id(entity):
    max_enity_id = 0
    try:
        max_enity_id = db.session.query(db.func.max(entity.id)).scalar() + 1
    except TypeError:
        max_enity_id = 1
    return max_enity_id


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if current_user.is_anonymous:
                return redirect(url_for('main.login'))
            urole = current_user.get_urole()
            if ( (urole != role) and (role != "ANY")):
                return login_manager.unauthorized()
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


@provider.route("/provider/products")
@login_required(role='PROVIDER')
def provider_products():
    user_id = current_user.get_id()
    user_provider_info = User_Provider_Info.query.get(user_id)
    provider = Provider.query.get(user_provider_info.provider_id)

    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')

    max_date_db = db.session.query(db.func.max(Product.date)).filter(Product.provider_id == provider.id).first()[0]
    min_date_db = db.session.query(db.func.min(Product.date)).filter(Product.provider_id == provider.id).first()[0]

    if not max_date_db or not min_date_db:
        max_date = datetime.datetime.utcnow().replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")
        min_date = datetime.datetime.utcnow().replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")
    else:
        max_date = max_date_db.strftime("%Y-%m-%d")
        min_date = min_date_db.strftime("%Y-%m-%d")

    if not 'page' in request.args or not 'from_date' in request.args or not 'to_date' in request.args or not 'presence' in request.args or not 'season' in request.args or not 'status' in request.args or not 'sorting' in request.args:
        return redirect('/provider/products?page=1&from_date={}&to_date={}&presence=есть&season=all&status=true&sorting=art'.format(min_date, max_date))

    try:
        from_date = datetime.datetime.strptime(request.args.get('from_date'), "%Y-%m-%d").replace(tzinfo=to_zone)
        to_date = datetime.datetime.strptime(request.args.get('to_date'), "%Y-%m-%d").replace(tzinfo=to_zone).date()
    except ValueError:
        from_date = datetime.datetime.strptime(request.args.get('from_date'), "%d-%m-%Y").replace(tzinfo=to_zone)
        to_date = datetime.datetime.strptime(request.args.get('to_date'), "%d-%m-%Y").replace(tzinfo=to_zone).date()

    from_date_view = from_date.strftime("%Y-%m-%d")

    from_date -= relativedelta(days=1)
    from_date = from_date.replace(hour=22)

    filter = {
        'page' : request.args.get('page'),
        'from_date' : from_date,
        'to_date' : to_date,
        'presence' : request.args.get('presence'),
        'season' : request.args.get('season'),
        'status' : request.args.get('status'),
        'sorting' : request.args.get('sorting'),
        'articul' : request.args.get('articul') if 'articul' in request.args else ""
    }


    flash_product = Flash_Product(user_id)
    displayed_fields = flash_product.get_displayed_fields()

    response = flash_product.get_products_for_provider(filter, provider.id)
    
    if not response:
        abort(404)

    products, pages_li, next, prev = response

    presence_options = get_options('presence')
    season_options = get_options('season')

    dates = flash_product.get_products_dates(provider.id)

    dates_new = []
    for date in dates:
        date_diction = {
            'date' : date.strftime("%d-%m-%Y"),
            'range' : date >= from_date.date() and date <= to_date
        }
        if not date_diction in dates_new:
            dates_new.append(date_diction)

    return render_template('provider_products.html', flash_locale=flash_locale, locale=user_provider_info.provider_locale, dates=dates_new,
        products=products, next=next, prev=prev, page_pag=int(filter.get('page')), pages_li=pages_li, displayed_fields=displayed_fields, page='products',
        presence_options=presence_options, min_date=min_date, max_date=max_date, season_options=season_options, from_date=from_date_view,
        to_date=to_date.strftime("%Y-%m-%d"), presence=filter.get('presence'), season=filter.get('season'), status=filter.get('status'), articul=filter.get('articul'),
        sorting=filter.get('sorting'), url=request.url, provider=provider)


@provider.route("/provider/products/act", methods=['POST'])
@login_required(role='PROVIDER')
def provider_products_action():
    user_id = current_user.get_id()
    user_provider_info = User_Provider_Info.query.get(user_id)
    provider = Provider.query.get(user_provider_info.provider_id)

    action = request.form['action']

    page = request.form['page']
    from_date = request.form['from_date']
    to_date = request.form['to_date']
    sorting = request.form['sorting']
    season = request.form['season']
    status = request.form['status']
    presence = request.form['presence']

    redirect_url = "/provider/products?page={}&from_date={}&to_date={}&sorting={}&season={}&status={}&presence={}".format(page, from_date, to_date, sorting, season, status, presence)

    if action == 'accept':

        if 'ids' in request.form:
            ids = request.form.getlist('ids')
        else:
            return redirect(redirect_url)
        if not ids:
            return redirect(redirect_url)

        for id in ids:
            product = Product.query.get(id)

            product.accept = True
            db.session.commit()

        return redirect(redirect_url)

    elif action == 'download':

        if 'ids' in request.form:
            ids = request.form.getlist('ids')
        else:
            return redirect(redirect_url)
        if not ids:
            return redirect(redirect_url)

        flash_file = Flash_File(user_id)
        rand_number = flash_file.get_zip_photo(ids)
        directory = "/home/dev/flash/files/temporary/" + str(rand_number)
        shutil.make_archive("/home/dev/flash/files/temporary/" + str(rand_number), 'zip', directory)
        @after_this_request
        def remove_file(response):
            os.remove("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip")
            shutil.rmtree(directory)
            return response
        return send_file("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip", attachment_filename='products.zip', as_attachment=True)

    elif action == 'download_all':

        ids_db = db.session.query(Product.id).filter(Product.provider_id == provider.id).all()

        ids = []
        for id_db in ids_db:
            ids.append(id_db[0])

        flash_file = Flash_File(user_id)
        rand_number = flash_file.get_zip_photo(ids)
        directory = "/home/dev/flash/files/temporary/" + str(rand_number)
        shutil.make_archive("/home/dev/flash/files/temporary/" + str(rand_number), 'zip', directory)
        @after_this_request
        def remove_file(response):
            os.remove("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip")
            shutil.rmtree(directory)
            return response
        return send_file("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip", attachment_filename='products.zip', as_attachment=True)


    elif action == 'make_news':

        print("ok")

        flash_product = Flash_Product(user_id)

        ids_db = db.session.query(Product.id).filter(Product.provider_id == provider.id, Product.id == Preparatory_Shoes.product_id).all()

        ids = []
        for id_db in ids_db:
            ids.append(id_db[0])

        products = []
        for id in ids:
            products.append(flash_product.get_product_changes(id))


        providers_name_id = {}
        providers_news = {}

        for product in products:
            if not product.get('provider_name') in providers_name_id:
                providers_name_id.update({product.get('provider_name'):Provider.query.filter_by(provider_name=product.get('provider_name')).first().id})

            if not product.get('provider_name') in providers_news:
                providers_news.update({product.get('provider_name'):{}})

        for product in products:
            diction_news = providers_news.get(product.get('provider_name'))

            if product.get('presence').lower() != product.get('new_presence').lower():
                if not 'presence_news' in diction_news:
                    news_id = get_max_entity_id(News)
                    dttime = datetime.datetime.utcnow()
                    new = News(id=news_id, category=1, provider_id=providers_name_id.get(product.get('provider_name')), news_type=2, visible=True, date=dttime)
                    db.session.add(new)
                    db.session.commit()
                    diction_news.update({'presence_news':news_id})
                    providers_news.update({product.get('provider_name'):diction_news})
                old_product_presence = Old_Product_Presence(id=get_max_entity_id(Old_Product_Presence), product_id=product.get('id'), news_id=diction_news.get('presence_news'), presence=product.get('presence'))
                db.session.add(old_product_presence)
                db.session.commit()


            if (float(product.get('cost').replace(',', '.')) != float(product.get('new_cost').replace(',', '.')) or 
                float(product.get('cost_site').replace(',', '.')) != float(product.get('new_cost_site').replace(',', '.')) or 
                product.get('currency').lower() != product.get('new_currency').lower()):
                if not 'cost_news' in diction_news:
                    news_id = get_max_entity_id(News)
                    dttime = datetime.datetime.utcnow()
                    new = News(id=news_id, category=1, provider_id=providers_name_id.get(product.get('provider_name')), news_type=3, visible=True, date=dttime)
                    db.session.add(new)
                    db.session.commit()
                    diction_news.update({'cost_news':news_id})
                    providers_news.update({product.get('provider_name'):diction_news})
                old_product_cost = Old_Product_Cost(id=get_max_entity_id(Old_Product_Cost), product_id=product.get('id'), news_id=diction_news.get('cost_news'), cost=float(product.get('cost').replace(',', '.')), cost_site=float(product.get('cost_site').replace(',', '.')))
                db.session.add(old_product_cost)
                db.session.commit()

            product_db = Product.query.get(product.get('id'))
            shoe_db = Shoes.query.get(product.get('id'))

            product_db.cost = float(product.get('new_cost').replace(',', '.'))
            product_db.cost_site = float(product.get('new_cost_site').replace(',', '.'))

            product_db.presence = product.get('new_presence')

            db.session.commit()


        for id in ids:
            preparatory_shoe = Preparatory_Shoes.query.filter_by(product_id=id).first()
            db.session.delete(preparatory_shoe)
            db.session.commit()

        return redirect(redirect_url)

    else:
        abort(404)


@provider.route("/provider/products/change_presence", methods=['POST'])
@login_required(role='PROVIDER')
def change_presence():
    product_id = int(request.values.get('product_id'))
    presence = request.values.get('presence')

    product = Product.query.get(product_id)
    shoe = Shoes.query.get(product_id)

    preparatory_shoes = Preparatory_Shoes.query.filter_by(product_id=product_id).first()
    if preparatory_shoes is None:
            
        preparatory_shoes = Preparatory_Shoes(id=get_max_entity_id(Preparatory_Shoes), product_id=product.id, articul=product.articul,
            cost=product.cost, cost_site=product.cost_site, currency=product.currency, presence=product.presence, brand_name=product.brand_name,
            group=shoe.group, type=shoe.type, repeat=shoe.repeat, sex=shoe.sex, min_order=shoe.min_order, material_outside=shoe.material_outside,
            material_inside=shoe.material_inside, material_insole=shoe.material_insole, colour=shoe.colour, size=shoe.size, season=shoe.season, box=shoe.box,
            manufacturer=shoe.manufacturer)
        db.session.add(preparatory_shoes)
        db.session.commit() 

    preparatory_shoes = Preparatory_Shoes.query.filter_by(product_id=product_id).first()
    preparatory_shoes.presence = presence
    db.session.commit() 

    return "ok", 200


@provider.route("/provider/products/change_cost", methods=['POST'])
@login_required(role='PROVIDER')
def change_cost():
    product_id = int(request.values.get('product_id'))
    try:
        cost = float(request.values.get('cost').replace(",", "."))
        cost_site = float(request.values.get('cost_site').replace(",", "."))
    except ValueError:
        return "ok", 200

    ids = request.values.getlist('ids')

    product = Product.query.get(product_id)
    shoe = Shoes.query.get(product_id)

    preparatory_shoes = Preparatory_Shoes.query.filter_by(product_id=product_id).first()
    if preparatory_shoes is None:
            
        preparatory_shoes = Preparatory_Shoes(id=get_max_entity_id(Preparatory_Shoes), product_id=product.id, articul=product.articul,
            cost=product.cost, cost_site=product.cost_site, currency=product.currency, presence=product.presence, brand_name=product.brand_name,
            group=shoe.group, type=shoe.type, repeat=shoe.repeat, sex=shoe.sex, min_order=shoe.min_order, material_outside=shoe.material_outside,
            material_inside=shoe.material_inside, material_insole=shoe.material_insole, colour=shoe.colour, size=shoe.size, season=shoe.season, box=shoe.box,
            manufacturer=shoe.manufacturer)
        db.session.add(preparatory_shoes)
        db.session.commit() 

    preparatory_shoes = Preparatory_Shoes.query.filter_by(product_id=product_id).first()
    preparatory_shoes.cost = cost
    preparatory_shoes.cost_site = cost_site
    db.session.commit()

    for id in ids:
        product = Product.query.get(id)
        shoe = Shoes.query.get(id)

        preparatory_shoes = Preparatory_Shoes.query.filter_by(product_id=id).first()
        if preparatory_shoes is None:
            preparatory_shoes = Preparatory_Shoes(id=get_max_entity_id(Preparatory_Shoes), product_id=product.id, articul=product.articul,
            cost=product.cost, cost_site=product.cost_site, currency=product.currency, presence=product.presence, brand_name=product.brand_name,
            group=shoe.group, type=shoe.type, repeat=shoe.repeat, sex=shoe.sex, min_order=shoe.min_order, material_outside=shoe.material_outside,
            material_inside=shoe.material_inside, material_insole=shoe.material_insole, colour=shoe.colour, size=shoe.size, season=shoe.season, box=shoe.box,
            manufacturer=shoe.manufacturer)
            db.session.add(preparatory_shoes)
            db.session.commit() 

        preparatory_shoes = Preparatory_Shoes.query.filter_by(product_id=id).first()
        preparatory_shoes.cost = cost
        preparatory_shoes.cost_site = cost_site
        db.session.commit()

        print("ok")

    return "ok", 200


@provider.route("/provider/news")
@login_required(role='PROVIDER')
def provider_news():
    user_id = current_user.get_id()
    user_provider_info = User_Provider_Info.query.get(user_id)
    provider = Provider.query.get(user_provider_info.provider_id)

    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')

    max_date_db = db.session.query(db.func.max(News.date)).first()[0]
    min_date_db = db.session.query(db.func.min(News.date)).first()[0]

    if not max_date_db or not min_date_db:
        max_date_db = datetime.datetime.utcnow()
        min_date_db = datetime.datetime.utcnow()

    max_date = max_date_db.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")
    min_date = min_date_db.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")

    if not 'type' in request.args or not 'from_date' in request.args or not 'to_date' in request.args or not 'page' in request.args:
        return redirect('/provider/news?type=all&from_date=' + min_date + '&to_date=' + max_date + '&page=1')

    from_date = datetime.datetime.strptime(request.args.get('from_date'), "%Y-%m-%d").replace(tzinfo=to_zone).date()
    to_date = datetime.datetime.strptime(request.args.get('to_date'), "%Y-%m-%d").replace(tzinfo=to_zone).date()

    type_news = request.args.get('type')
    page = request.args.get('page')

    flash_news = Flash_News(user_id)
    response = flash_news.get_visible_news(type_news, provider.id, from_date, to_date, page)

    if response == False:
        abort(404)

    news, pages_li, next, prev = response

    return render_template('provider_news.html', flash_locale=flash_locale, locale=user_provider_info.provider_locale,
        news=news, page='news', next=next, prev=prev, pages_li=pages_li, page_pag=int(page), type=type_news, from_date=from_date,
        to_date=to_date, min_date=min_date, max_date=max_date, url=request.url)


@provider.route("/provider/news/detail/<news_id>")
@login_required(role='PROVIDER')
def news_detail_provider(news_id):
    user_id = current_user.get_id()
    user_provider_info = User_Provider_Info.query.get(user_id)
    provider = Provider.query.get(user_provider_info.provider_id)
    
    new = News.query.get(news_id)

    if provider.id != new.provider_id:
        abort(404)

    if new.news_type != 8:
        flash_product = Flash_Product(user_id)
        products = flash_product.get_products_by_news(news_id)
        if products == False:
            abort(404)

        displayed_fields = flash_product.get_displayed_fields()

        return render_template('provider_news_detail.html', flash_locale=flash_locale, locale=user_provider_info.provider_locale, 
            products=products,  displayed_fields=displayed_fields, page='news', news_type=new.news_type, url=request.url)
    else:
        abort(404)


@provider.route("/provider/profile", methods=['GET', 'POST'])
@login_required(role='PROVIDER')
def provider_profile():
    user_id = current_user.get_id()
    user_provider_info = User_Provider_Info.query.get(user_id)
    provider = Provider.query.get(user_provider_info.provider_id)

    if request.method == 'GET':
        return render_template('provider_profile.html', flash_locale=flash_locale, locale=user_provider_info.provider_locale,
         provider=provider, user_provider_info=user_provider_info, page='profile', url=request.url)

    if request.method == 'POST':
        error_log = None

        action = request.form['action']

        if action == 'profile':
            address = request.form['address']
            phone = request.form['phone']
            viber = request.form['viber']
            email = request.form['email']
            doll_to_uah = request.form['doll_to_uah']
            euro_to_uah = request.form['euro_to_uah']
            locale = request.form['locale']

            try:
                doll_to_uah = float(doll_to_uah.replace(",", "."))
            except ValueError:
                error_log = 'Не удаётся преобразовать курс доллара в число.'

            try:
                euro_to_uah = float(euro_to_uah.replace(",", "."))
            except ValueError:
                error_log = 'Не удаётся преобразовать курс евро в число.'

            if error_log:
                return render_template('provider_profile.html', flash_locale=flash_locale, locale=user_provider_info.provider_locale,
                 error_log_profile=error_log, provider=provider, user_provider_info=user_provider_info, page='profile', url=request.url)

            market_rate = False
            if 'market_rate' in request.form:
                market_rate = True

            if provider.address != address:
                provider.address = address
                dttime = datetime.datetime.utcnow()
                news = News(id=get_max_entity_id(News), provider_id=provider.id, news_type=7, visible=False, date=dttime, category=0)
                db.session.add(news)
                db.session.commit()

            if provider.doll_to_uah != doll_to_uah or provider.euro_to_uah != euro_to_uah:
                provider.doll_to_uah = doll_to_uah
                provider.euro_to_uah = euro_to_uah
                dttime = datetime.datetime.utcnow()
                news = News(id=get_max_entity_id(News), provider_id=provider.id, news_type=6, visible=False, date=dttime, category=0)
                db.session.add(news)
                db.session.commit()

            provider.provider_phone = phone
            provider.provider_viber = viber
            provider.provider_mail = email
            provider.market_rate = market_rate

            user_provider_info.provider_locale = locale
            db.session.commit()
            return render_template('provider_profile.html', flash_locale=flash_locale, locale=user_provider_info.provider_locale,
             success_log_profile=True, provider=provider, user_provider_info=user_provider_info, page='profile', url=request.url)

        elif action == 'password':
            error_log_password = ''
            password_old = request.form['password_old']
            password_new = request.form['password_new']
            password_confirm = request.form['password_confirm']

            if password_confirm != password_new:
                error_log_password = 'Пароли не совпадают' 

            user = User.query.get(user_id)

            pass_hash = hashlib.sha256(password_old.encode('utf-8')).hexdigest()

            if pass_hash != user.password_hash:
                error_log_password = 'Старый пароль введён неверно'

            if error_log_password:
                return render_template('provider_profile.html', flash_locale=flash_locale, locale=user_provider_info.provider_locale,
                 error_log_password=error_log_password, provider=provider, user_provider_info=user_provider_info, page='profile', url=request.url)

            pass_hash = hashlib.sha256(password_new.encode('utf-8')).hexdigest()

            user.password_hash = pass_hash
            db.session.commit()

            return render_template('provider_profile.html', flash_locale=flash_locale, locale=user_provider_info.provider_locale,
             success_log_password=True, provider=provider, user_provider_info=user_provider_info, page='profile', url=request.url)




@provider.route("/provider/settings/displayed_fields", methods=['GET', 'POST'])
@login_required(role='PROVIDER')
def provider_settings_displayed_fields():
    user_id = current_user.get_id()
    user_provider_info = User_Provider_Info.query.get(user_id)
    success_log = False

    if request.method == 'POST':
        success_log = True
        displayed_fields_db = Displayed_Fields.query.filter_by(user_id=user_id).all()
        for displayed_field in displayed_fields_db:
            displayed_field.flag = True if displayed_field.field_name in request.form else False
        db.session.commit()

    flash_product = Flash_Product(user_id)
    displayed_fields = flash_product.get_displayed_fields()

    return render_template('provider_settings_displayed_fields.html', flash_locale=flash_locale, locale=user_provider_info.provider_locale,
     success_log=success_log, page='setting_displayed_fields', displayed_fields=displayed_fields, url=request.url)


@provider.route("/provider/settings/change/language")
@login_required(role='PROVIDER')
def provider_settings_change_language():
    user_id = current_user.get_id()
    user_provider_info = User_Provider_Info.query.get(user_id)

    if user_provider_info.provider_locale == 'Ru':
        user_provider_info.provider_locale = 'Ch'
    else:
        user_provider_info.provider_locale = 'Ru'

    db.session.commit()

    success_log = False

    url = request.args.get('url')
    return redirect(url)
