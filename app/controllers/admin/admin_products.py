from flask import Blueprint, redirect, url_for, render_template, abort, send_file, after_this_request, request
from functools import wraps
from app.models import *
from config import login_manager
from flask_login import login_user, current_user, logout_user, AnonymousUserMixin
from PIL import Image, ImageEnhance
from random import randint
from app.flash_lib.flash_file import Flash_File
from app.flash_lib.flash_product import Flash_Product
from app.flash_lib.flash_news import Flash_News
from app.flash_lib.flash_user import Flash_User
from app.flash_lib.flash_setting import Flash_Setting
from app.flash_lib.flash_feedback import Flash_Feedback
from urllib.parse import urlencode, unquote, quote
import datetime
from dateutil import tz
import csv
import itertools
import zipfile
import os
import shutil
import pickle


admin_products = Blueprint('admin_products', __name__, template_folder='../../templates/admin')


def error_upload(error_log, name, directory):
    os.remove(name)
    shutil.rmtree(directory)
    return render_template('products_upload.html', error_log=error_log, page='products_upload')


def init_columns_names(user_id):
    uploads_fields = dict()

    uploads_fields_db = Upload_Field.query.filter_by(user_id = user_id).all()
    for upload_field_db in uploads_fields_db:
        uploads_fields.update({upload_field_db.column_type:upload_field_db.column_name})

    return uploads_fields


def init_list_fields_name():
    list_fields_names = [ 'id', 'articul', 'provider_name', 'brand_name', 'cost', 'cost_site', 'currency', 'sex', 'group', 'type', 'material_outside', 'material_inside', 
    'material_insole', 'size', 'repeat', 'season', 'box', 'min_order', 'manufacturer', 'colour', 'descreption', 'presence', 'photo_1', 'photo_2', 'photo_3']
    return list_fields_names


def init_short_list_fields_name():
    list_fields_names = ['articul', 'currency', 'sex', 'group', 'type', 'material_outside', 'material_inside', 'material_insole', 'size', 'repeat', 'season', 'box', 
    'min_order', 'manufacturer', 'colour', 'presence']
    return list_fields_names


def init_not_empty_fields_name():
    list_fields_names = ['articul', 'provider_name', 'brand_name', 'cost', 'cost_site', 'currency', 'group', 'type', 'size', 'season', 'box', 'min_order', 
    'presence']
    return list_fields_names


def update_column_numbers(column_numbers, columns_names):
    list_fields_names = init_list_fields_name()

    column_numbers_updated = {}
    for field_name in list_fields_names:
        column_numbers_updated.update({field_name:column_numbers.get(columns_names.get(field_name))})

    return column_numbers_updated


def check_errors(lines, column_numbers, columns_names, rand_number):
    presence_options = get_options('presence')
    currency_options = get_options('currency')
    directory = "/home/dev/flash/files/temporary/" + str(rand_number) + '/'
    list_fields_names = init_list_fields_name()
    for name_field in list_fields_names:
        if column_numbers.get(name_field) == None:
            return 'Столбец ' + columns_names.get(name_field) + ' не найден'

    not_empty_fields = init_not_empty_fields_name()
    short_fields = init_short_list_fields_name()
    for i in range(1, len(lines)):
        provider = Provider.query.filter(db.func.lower(Provider.provider_name) == lines[i][column_numbers.get('provider_name')].lower()).first()
        if provider is None:
            return 'Строка ' + str(i) + ': поставщик ' + lines[i][column_numbers.get('provider_name')] + ' не найден. Добавьте поставщика и повторите снова.'

        for not_empty_field in not_empty_fields:
            if lines[i][column_numbers.get(not_empty_field)] == "":
                return 'Ячейка (' + str(i) + ':' + columns_names.get(not_empty_field) + ') не может быть пустой.'

        if lines[i][column_numbers.get('id')] == "" and lines[i][column_numbers.get('photo_1')] == "":
            return 'Строка ' + str(i) + ': ячейка с фото не может быть пустой.'

        for short_field in short_fields:
            if len(lines[i][column_numbers.get(short_field)]) > 40:
                return 'Ячейка (' + str(i) + ':' + columns_names.get(short_field) + ') имеет длину больше 20 символов.'

        if lines[i][column_numbers.get('sex')] == '' and lines[i][column_numbers.get('type')].lower() == 'детская':
            return 'Ячейка (' + str(i) + ':' + columns_names.get('sex') + ') не может быть пустой, при типе равном детская'

        if check_belonging(lines[i][column_numbers.get('currency')], currency_options):
            return 'Ячейка с валютой (' + str(i) + ':' + columns_names.get('currency') + ') имеет некорректное значение.'

        if check_belonging(lines[i][column_numbers.get('presence')], presence_options):
            return 'Ячейка с наличием (' + str(i) + ':' + columns_names.get('presence') + ') имеет некорректное значение.'

        for j in range(i, len(lines)):
            if lines[i][column_numbers.get('articul')].lower() == lines[j][column_numbers.get('articul')].lower() and i != j:
                return 'Артикул на строках ' + str(i) + " " + str(j) + ' одинаковые. Проверьте правильность данных.'
            
        if lines[i][column_numbers.get('id')] != "":
            try:
                prod_id = int(lines[i][column_numbers.get('id')])
                if Product.query.get(prod_id) is None:
                    return 'На строке ' + str(i) + ' по указанному id не удаётся найти товар. Пожалуйста, проверьте правильность данных.'
            except ValueError:
                return 'На строке ' + str(i) + ' id не удаётся преобразовать в число. Пожалуйста, проверьте правильность данных.'

            # if Product.query.filter_by(id=prod_id, articul=lines[i][column_numbers.get('articul')]).first() is None:
            #     return 'На строке ' + str(i) + ' по id и артикулу не удаётся найти товар. Пожалуйста, проверьте правильность данных.'

        try:
            cost = float(lines[i][column_numbers.get('cost')].replace(',', '.'))
            cost_site = float(lines[i][column_numbers.get('cost_site')].replace(',', '.'))
        except ValueError:
            return 'На строке ' + str(i) + ' цену не удаётся преобразовать в число. Пожалуйста, проверьте правильность данных.'

        if cost_site <= cost:
            return 'На строке ' + str(i) + ' цена сайт не может быть меньше либо равна цене. Пожалуйста, проверьте правильность данных.'


    return None


def check_images(lines, column_numbers, columns_names, rand_number):
    directory = "/home/dev/flash/files/temporary/" + str(rand_number) + '/'
    for i in range(1, len(lines)):
        if lines[i][column_numbers.get('photo_1')] != '':
            try:
                image = Image.open(directory + lines[i][column_numbers.get('photo_1')])
            except (FileNotFoundError, IOError):
                return 'На строке ' + str(i) + ' файл с фотографией не обнаружен или повреждён. Пожалуйста, проверьте правильность данных.'

    return None

def check_belonging(value, options):
    value = value.lower()
    result = False if value in options else True
    return result


def get_options(column_type):
    options_db = Option.query.filter_by(column_type=column_type).order_by(Option.column_option).all()
    options = []
    for option_db in options_db:
        options.append(option_db.column_option)
    return options


def check_warnings(lines, column_numbers):
    warnings = []
    double_new_fields = []

    brand_options = get_options('brand')
    presence_options = get_options('presence')
    currency_options = get_options('currency')
    season_options = get_options('season')
    colour_options = get_options('colour')
    sex_options = get_options('sex')
    group_options = get_options('group')
    type_options = get_options('type')
    min_order_options = get_options('min_order')
    size_options = get_options('size')
    repeat_options = get_options('repeat')
    manufacturer_options = get_options('manufacturer')
    box_options = get_options('box')
    material_outside_options = get_options('material_outside')
    material_inside_options = get_options('material_inside')
    material_insole_options = get_options('material_insole')

    for i in range(1, len(lines)):
        warning_dict = {}
        if check_belonging(lines[i][column_numbers.get('brand_name')], brand_options):
            warning_dict.update({'brand': True})
            double_new_fields.append(['brand' , lines[i][column_numbers.get('brand_name')]])
        else:
            warning_dict.update({'brand': False})

        if check_belonging(lines[i][column_numbers.get('season')], season_options):
            warning_dict.update({'season': True})
            double_new_fields.append(['season' , lines[i][column_numbers.get('season')]])
        else:
            warning_dict.update({'season': False})

        if check_belonging(lines[i][column_numbers.get('type')], type_options):
            warning_dict.update({'type': True})
            double_new_fields.append(['type' , lines[i][column_numbers.get('type')]])
        else:
            warning_dict.update({'type': False})

        if check_belonging(lines[i][column_numbers.get('group')], group_options):
            warning_dict.update({'group': True})
            double_new_fields.append(['group' , lines[i][column_numbers.get('group')]])
        else:
            warning_dict.update({'group': False})

        if check_belonging(lines[i][column_numbers.get('min_order')], min_order_options):
            warning_dict.update({'min_order': True})
            double_new_fields.append(['min_order' , lines[i][column_numbers.get('min_order')]])
        else:
            warning_dict.update({'min_order': False})

        if check_belonging(lines[i][column_numbers.get('box')], box_options):
            warning_dict.update({'box': True})
            double_new_fields.append(['box' , lines[i][column_numbers.get('box')]])
        else:
            warning_dict.update({'box': False})

        if check_belonging(lines[i][column_numbers.get('size')], size_options):
            warning_dict.update({'size': True})
            double_new_fields.append(['size' , lines[i][column_numbers.get('size')]])
        else:
            warning_dict.update({'size': False})

        if check_belonging(lines[i][column_numbers.get('colour')], colour_options) and lines[i][column_numbers.get('colour')] != '':
            warning_dict.update({'colour': True})
            double_new_fields.append(['colour' , lines[i][column_numbers.get('colour')]])
        else:
            warning_dict.update({'colour': False})

        if check_belonging(lines[i][column_numbers.get('manufacturer')], manufacturer_options) and lines[i][column_numbers.get('manufacturer')] != '':
            warning_dict.update({'manufacturer': True})
            double_new_fields.append(['manufacturer' , lines[i][column_numbers.get('manufacturer')]])
        else:
            warning_dict.update({'manufacturer': False})

        if check_belonging(lines[i][column_numbers.get('sex')], sex_options) and lines[i][column_numbers.get('sex')] != '':
            warning_dict.update({'sex': True})
            double_new_fields.append(['sex' , lines[i][column_numbers.get('sex')]])
        else:
            warning_dict.update({'sex': False})

        if check_belonging(lines[i][column_numbers.get('repeat')], repeat_options) and lines[i][column_numbers.get('repeat')] != '':
            warning_dict.update({'repeat': True})
            double_new_fields.append(['repeat' , lines[i][column_numbers.get('repeat')]])
        else:
            warning_dict.update({'repeat': False})

        if check_belonging(lines[i][column_numbers.get('material_outside')], material_outside_options) and lines[i][column_numbers.get('material_outside')] != '':
            warning_dict.update({'material_outside': True})
            double_new_fields.append(['material_outside' , lines[i][column_numbers.get('material_outside')]])
        else:
            warning_dict.update({'material_outside': False})

        if check_belonging(lines[i][column_numbers.get('material_inside')], material_inside_options) and lines[i][column_numbers.get('material_inside')] != '':
            warning_dict.update({'material_inside': True})
            double_new_fields.append(['material_inside' , lines[i][column_numbers.get('material_inside')]])
        else:
            warning_dict.update({'material_inside': False})

        if check_belonging(lines[i][column_numbers.get('material_insole')], material_insole_options) and lines[i][column_numbers.get('material_insole')] != '':
            warning_dict.update({'material_insole': True})
            double_new_fields.append(['material_insole' , lines[i][column_numbers.get('material_insole')]])
        else:
            warning_dict.update({'material_insole': False})

        warnings.append(warning_dict)

    new_fields = []
    for field in double_new_fields:
        if not field in new_fields:
            new_fields.append(field)

    return warnings, new_fields

def check_doubles(lines, column_numbers):
    doubles = []

    for i in range(1, len(lines)):
        provider = Provider.query.filter(db.func.lower(Provider.provider_name)==lines[i][column_numbers.get('provider_name')].lower()).first()
        if provider:
            product = db.session.query(Product).filter(Product.provider_id==provider.id, db.func.lower(Product.articul)==lines[i][column_numbers.get('articul')].lower()).first()
            if product and lines[i][column_numbers.get('id')] == '':
                double_dict = {
                    'number' : i,
                    'id' : product.id,
                    'presence' : product.presence
                }
                doubles.append(double_dict)

    return doubles


def check_characteristic(shoe, form):
    if shoe.group.lower() != form['group']:
        return True

    if shoe.type.lower() != form['type']:
        return True

    if shoe.repeat.lower() != form['repeat']:
        return True

    if shoe.sex.lower() != form['sex']:
        return True

    if shoe.min_order.lower() != form['min_order']:
        return True

    if shoe.material_outside.lower() != form['material_outside']:
        return True

    if shoe.material_inside.lower() != form['material_inside']:
        return True

    if shoe.material_insole.lower() != form['material_insole']:
        return True

    if shoe.colour.lower() != form['colour']:
        return True

    if shoe.size.lower() != form['size']:
        return True

    if shoe.season.lower() != form['season']:
        return True

    if shoe.box.lower() != form['box']:
        return True

    if shoe.manufacturer.lower() != form['manufacturer']:
        return True


def check_csv_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ['csv']

def allowed_watermark(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ['png']

def allowed_user_files(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ['jpg', 'jpeg', 'csv']

def allowed_products(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ['zip']

def get_max_entity_id(entity):
    max_enity_id = 0
    try:
        max_enity_id = db.session.query(db.func.max(entity.id)).scalar() + 1
    except TypeError:
        max_enity_id = 1
    return max_enity_id


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if current_user.is_anonymous:
                return redirect(url_for('main.login'))
            urole = current_user.get_urole()
            if ( (urole != role) and (role != "ANY")):
                return login_manager.unauthorized()      
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


def admin_access_required():
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            user_id = current_user.get_id()
            user = User.query.get(user_id)
            if user.role == 'ADMIN':
                user_admin_info = User_Admin_Info.query.get(user_id)
                if user_admin_info.rights == 'BLOCK':
                    return redirect(url_for('main.logout'))
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


@admin_products.route("/admin/products")
@login_required(role='ADMIN')
@admin_access_required()
def products():
    user_id = current_user.get_id()

    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')

    max_date_db = db.session.query(db.func.max(Product.date)).first()[0]
    min_date_db = db.session.query(db.func.min(Product.date)).first()[0]

    if not max_date_db or not min_date_db:
        max_date = datetime.datetime.utcnow().replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")
        min_date = datetime.datetime.utcnow().replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")
    else:
        max_date = max_date_db.strftime("%Y-%m-%d")
        min_date = min_date_db.strftime("%Y-%m-%d")

    if not 'category' in request.args or not 'provider' in request.args or not 'presence' in request.args or not 'page' in request.args or not 'from_date' in request.args or not 'to_date' in request.args:
        return redirect("{}?provider=all&category=1&presence=есть&page=1&from_date={}&to_date={}".format(url_for('admin_products.products'), min_date, max_date))

    filter = {
        'category' : request.args.get('category'),
        'provider' : request.args.get('provider'),
        'presence' : request.args.get('presence'),
        'page' : request.args.get('page'),
        'from_date' : request.args.get('from_date'),
        'to_date' : request.args.get('to_date')
    }

    if 'articul' in request.args:
        articul = request.args.get('articul')
    else:
        articul = ""

    if 'flash_id' in request.args:
        flash_id = request.args.get('flash_id')
    else:
        flash_id = ""

    filter.update({'flash_id':flash_id})
    filter.update({'articul':articul})
    flash_product = Flash_Product(user_id)
    response = flash_product.get_products_by_filter(filter)

    if response == False:
        abort(404)

    products, pages_li, next, prev = response

    displayed_fields = flash_product.get_displayed_fields()

    presence_options = get_options('presence')

    providers = Provider.query.order_by(Provider.provider_name).all()

    provider = filter.get('provider') if filter.get('provider') == 'all' else int(filter.get('provider'))

    return render_template('products.html', displayed_fields=displayed_fields, provider=provider, presence=filter.get('presence'), articul=articul, flash_id=flash_id,
        providers=providers, category=filter.get('category'), products=products, page='products', presence_options=presence_options, pages_li=pages_li,
        prev=prev, next=next, page_pag=int(filter.get('page')), from_date=filter.get('from_date'), to_date=filter.get('to_date'), min_date=min_date, max_date=max_date)


@admin_products.route("/admin/products/download", methods=['POST'])
@login_required(role='ADMIN')
@admin_access_required()
def products_download():
    user_id = current_user.get_id()

    action = request.form['action']

    ids = None
    if 'ids' in request.form:
        ids = request.form.getlist('ids')
    else:
        return redirect(url_for('admin_products.products'))
    if not ids:
        return redirect(url_for('admin_products.products'))

    if action == 'xlsx':
        flash_file = Flash_File(user_id)
        file_name = flash_file.get_xlsx(ids)
        @after_this_request
        def remove_file(response):
            os.remove(file_name)
            return response
        return send_file(file_name, attachment_filename='products.xlsx', as_attachment=True)
    elif action == 'zip':
        flash_file = Flash_File(user_id)
        rand_number = flash_file.get_zip(ids)
        directory = "/home/dev/flash/files/temporary/" + str(rand_number)
        shutil.make_archive("/home/dev/flash/files/temporary/" + str(rand_number), 'zip', directory)
        @after_this_request
        def remove_file(response):
            os.remove("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip")
            shutil.rmtree(directory)
            return response
        return send_file("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip", attachment_filename='products.zip', as_attachment=True)
    elif action == 'delete':
        flash_product = Flash_Product(user_id)
        for id in ids:
            flash_product.delete_product_by_id(id)
        return redirect(url_for('admin_products.products'))
    else:
        return redirect(url_for('admin_products.products'))


@admin_products.route("/admin/products/edit/<product_id>", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def edit_product(product_id):
    user_id = current_user.get_id()
    flash_product = Flash_Product(user_id)
    
    brand_options = get_options('brand')
    presence_options = get_options('presence')
    currency_options = get_options('currency')
    season_options = get_options('season')
    colour_options = get_options('colour')
    sex_options = get_options('sex')
    group_options = get_options('group')
    type_options = get_options('type')
    min_order_options = get_options('min_order')
    size_options = get_options('size')
    repeat_options = get_options('repeat')
    manufacturer_options = get_options('manufacturer')
    box_options = get_options('box')
    material_outside_options = get_options('material_outside')
    material_inside_options = get_options('material_inside')
    material_insole_options = get_options('material_insole')

    if request.method == 'GET':
        product = flash_product.get_product(product_id)
        return render_template('edit_product.html', product=product, page='products', presence_options=presence_options,
        currency_options=currency_options, season_options=season_options, colour_options=colour_options, min_order_options=min_order_options,
        sex_options=sex_options, group_options=group_options, type_options=type_options, brand_options=brand_options, size_options=size_options,
        repeat_options=repeat_options, manufacturer_options=manufacturer_options, box_options=box_options, material_outside_options=material_outside_options, 
        material_inside_options=material_inside_options, material_insole_options=material_insole_options)


    if request.method == 'POST':
        articul = request.form['articul']
        error_log = None

        try:
            cost = float(request.form['cost'].replace(',', '.'))
            cost_site = float(request.form['cost_site'].replace(',', '.'))
        except ValueError:
            error_log = 'Не удаётся преобразовать цену в число'

        if cost_site <= cost:
            error_log = 'Цена сайт не может быть меньше либо равна цене. Пожалуйста, проверьте правильность данных.'

        product = Product.query.get(product_id)
        if articul != product.articul and db.session.query(Product).filter(db.func.lower(Product.articul)==articul.lower(), Product.provider_id==product.provider_id).first():
            error_log = "Данный артикул у поставщика уже существует"

        if error_log:
            product = flash_product.get_product(product_id)
            return render_template('edit_product.html', product=product, page='products', presence_options=presence_options, error_log=error_log,
                currency_options=currency_options, season_options=season_options, colour_options=colour_options, min_order_options=min_order_options,
                sex_options=sex_options, group_options=group_options, type_options=type_options, brand_options=brand_options, size_options=size_options,
                repeat_options=repeat_options, manufacturer_options=manufacturer_options, box_options=box_options, material_outside_options=material_outside_options, 
                material_inside_options=material_inside_options, material_insole_options=material_insole_options)

        
        brand_name = request.form['brand']
        currency= request.form['currency']
        presence = request.form['presence']
        sex = request.form['sex'] 
        group = request.form['group'] 
        type = request.form['type']
        size = request.form['size'] 
        repeat = request.form['repeat'] 
        material_outside = request.form['material_outside'] 
        material_inside = request.form['material_inside'] 
        material_insole = request.form['material_insole'] 
        season = request.form['season'] 
        box = request.form['box'] 
        colour = request.form['colour']
        min_order = request.form['min_order']
        manufacturer = request.form['manufacturer'] 

        preparatory_shoe = Preparatory_Shoes.query.filter_by(product_id=product_id).first()
        if preparatory_shoe is None:
            preparatory_shoe = Preparatory_Shoes(id=get_max_entity_id(Preparatory_Shoes), product_id=product_id, admin_id=user_id, articul=articul, brand_name=brand_name, cost=cost, cost_site=cost_site, 
                currency=currency, presence=presence, sex=sex, group=group, type=type, size=size, repeat=repeat, material_outside=material_outside, 
                material_inside=material_inside, material_insole=material_insole, season=season, box=box, colour=colour, min_order=min_order, manufacturer=manufacturer)
            db.session.add(preparatory_shoe)
            db.session.commit()
        else:
            preparatory_shoe.articul = articul
            preparatory_shoe.brand_name = brand_name
            preparatory_shoe.cost = cost
            preparatory_shoe.cost_site = cost_site 
            preparatory_shoe.currency = currency
            preparatory_shoe.presence = presence
            preparatory_shoe.sex = sex
            preparatory_shoe.group = group
            preparatory_shoe.type = type
            preparatory_shoe.size = size
            preparatory_shoe.repeat = repeat
            preparatory_shoe.material_outside = material_outside
            preparatory_shoe.material_inside = material_inside
            preparatory_shoe.material_insole = material_insole
            preparatory_shoe.season = season
            preparatory_shoe.box = box
            preparatory_shoe.colour = colour
            preparatory_shoe.min_order = min_order
            preparatory_shoe.manufacturer = manufacturer

            db.session.commit()
    
        product = flash_product.get_product(product_id)

        success_log = 'Запись изменения успешно добавлена'
        return render_template('edit_product.html', product=product, page='products', presence_options=presence_options, error_log=error_log, edit=True,
            currency_options=currency_options, season_options=season_options, colour_options=colour_options, min_order_options=min_order_options, success_log=success_log,
            sex_options=sex_options, group_options=group_options, type_options=type_options, brand_options=brand_options, size_options=size_options,
            repeat_options=repeat_options, manufacturer_options=manufacturer_options, box_options=box_options, material_outside_options=material_outside_options, 
            material_inside_options=material_inside_options, material_insole_options=material_insole_options)


@admin_products.route("/admin/products/changes")
@login_required(role='ADMIN')
@admin_access_required()
def products_changes():
    user_id = current_user.get_id()
    if not 'admin' in request.args or not 'provider' in request.args:
        return redirect("/admin/products/changes?admin=" + str(user_id) + "&provider=all")

    admin = request.args.get('admin')
    provider = request.args.get('provider')

    if provider != 'all':
        provider = int(provider)

    if admin != 'all':
        admin = int(admin)
    
    flash_product = Flash_Product(user_id)
    products = flash_product.get_products_changes(admin, provider)
    providers = Provider.query.order_by(Provider.provider_name).all()
    admins = User_Admin_Info.query.all()

    return render_template('admin_products_changes.html', providers=providers, provider=provider, admin=admin, admins=admins, products=products, page='products_changes')


@admin_products.route("/admin/products/changes/action", methods=['POST'])
@login_required(role='ADMIN')
@admin_access_required()
def products_changes_action():
    user_id = current_user.get_id()

    action = request.form['action']
    admin = request.form['admin']
    provider = request.form['provider']

    flash_product = Flash_Product(user_id)

    if 'ids' in request.form:
        ids = request.form.getlist('ids')
    else:
        return redirect("/admin/products/changes?admin=" + str(admin) + "&provider=" + str(provider))
    if not ids:
        return redirect("/admin/products/changes?admin=" + str(admin) + "&provider=" + str(provider))

    if action == 'delete':
        for id in ids:
            preparatory_shoe = Preparatory_Shoes.query.filter_by(product_id=id).first()
            db.session.delete(preparatory_shoe)
            db.session.commit()

    elif action == 'accept':
        products = []
        for id in ids:
            products.append(flash_product.get_product_changes(id))


        providers_name_id = {}
        providers_news = {}

        for product in products:
            if not product.get('provider_name') in providers_name_id:
                providers_name_id.update({product.get('provider_name'):Provider.query.filter_by(provider_name=product.get('provider_name')).first().id})

            if not product.get('provider_name') in providers_news:

                providers_news.update({product.get('provider_name'):{}})

        for product in products:
            diction_news = providers_news.get(product.get('provider_name'))

            if product.get('articul') != product.get('new_articul'):
                if not 'articul_news' in diction_news:
                    news_id = get_max_entity_id(News)
                    dttime = datetime.datetime.utcnow()
                    new = News(id=news_id, category=1, admin_id=user_id, provider_id=providers_name_id.get(product.get('provider_name')), news_type=9, visible=True, date=dttime)
                    db.session.add(new)
                    db.session.commit()
                    diction_news.update({'articul_news':news_id})
                    providers_news.update({product.get('provider_name'):diction_news})

                new_articul_product = New_Articul_Product(id=get_max_entity_id(New_Articul_Product), product_id=product.get('id'), news_id=diction_news.get('articul_news'), articul=product.get('articul'))
                db.session.add(new_articul_product)
                db.session.commit()


            if product.get('presence').lower() != product.get('new_presence').lower():
                if not 'presence_news' in diction_news:
                    news_id = get_max_entity_id(News)
                    dttime = datetime.datetime.utcnow()
                    new = News(id=news_id, category=1, admin_id=user_id, provider_id=providers_name_id.get(product.get('provider_name')), news_type=2, visible=True, date=dttime)
                    db.session.add(new)
                    db.session.commit()
                    diction_news.update({'presence_news':news_id})
                    providers_news.update({product.get('provider_name'):diction_news})

                old_product_presence = Old_Product_Presence(id=get_max_entity_id(Old_Product_Presence), product_id=product.get('id'), news_id=diction_news.get('presence_news'), presence=product.get('presence'))
                db.session.add(old_product_presence)
                db.session.commit()


            if (float(product.get('cost').replace(',', '.')) != float(product.get('new_cost').replace(',', '.')) or 
                float(product.get('cost_site').replace(',', '.')) != float(product.get('new_cost_site').replace(',', '.')) or 
                product.get('currency').lower() != product.get('new_currency').lower()):
                if not 'cost_news' in diction_news:
                    news_id = get_max_entity_id(News)
                    dttime = datetime.datetime.utcnow()
                    new = News(id=news_id, category=1, admin_id=user_id, provider_id=providers_name_id.get(product.get('provider_name')), news_type=3, visible=True, date=dttime)
                    db.session.add(new)
                    db.session.commit()
                    diction_news.update({'cost_news':news_id})
                    providers_news.update({product.get('provider_name'):diction_news})

                old_product_cost = Old_Product_Cost(id=get_max_entity_id(Old_Product_Cost), product_id=product.get('id'), news_id=diction_news.get('cost_news'), cost=float(product.get('cost').replace(',', '.')), cost_site=float(product.get('cost_site').replace(',', '.')))
                db.session.add(old_product_cost)
                db.session.commit()

            if (product.get('group').lower() != product.get('new_group').lower() or
                product.get('type').lower() != product.get('new_type').lower() or
                product.get('repeat').lower() != product.get('new_repeat').lower() or
                product.get('sex').lower() != product.get('new_sex').lower() or
                product.get('min_order').lower() != product.get('new_min_order').lower() or
                product.get('material_outside').lower() != product.get('new_material_outside').lower() or
                product.get('material_inside').lower() != product.get('new_material_inside').lower() or
                product.get('material_insole').lower() != product.get('new_material_insole').lower() or
                product.get('colour').lower() != product.get('new_colour').lower() or
                product.get('size').lower() != product.get('new_size').lower() or
                product.get('season').lower() != product.get('new_season').lower() or
                product.get('box').lower() != product.get('new_box').lower() or
                product.get('manufacturer').lower() != product.get('new_manufacturer').lower() or
                product.get('brand_name').lower() != product.get('new_brand_name').lower()):
                if not 'chars_news' in diction_news:
                    news_id = get_max_entity_id(News)
                    dttime = datetime.datetime.utcnow()
                    new = News(id=news_id, category=1, admin_id=user_id, provider_id=providers_name_id.get(product.get('provider_name')), news_type=5, visible=True, date=dttime)
                    db.session.add(new)
                    db.session.commit()
                    diction_news.update({'chars_news':news_id})
                    providers_news.update({product.get('provider_name'):diction_news})

                old_product_features = Old_Product_Features(id=get_max_entity_id(Old_Product_Features), product_id=product.get('id'), news_id=diction_news.get('chars_news'),
                    brand_name=product.get('brand_name'), group=product.get('group'), type=product.get('type'), repeat=product.get('repeat'), sex=product.get('sex'),
                    min_order=product.get('min_order'), material_outside=product.get('material_outside'), material_inside=product.get('material_inside'), box=product.get('box'),
                    material_insole=product.get('material_insole'), colour=product.get('colour'), size=product.get('size'), season=product.get('season'), manufacturer=product.get('manufacturer'))
                db.session.add(old_product_features)
                db.session.commit()

            product_db = Product.query.get(product.get('id'))
            shoe_db = Shoes.query.get(product.get('id'))

            product_db.articul = product.get('new_articul')

            product_db.cost = float(product.get('new_cost').replace(',', '.'))
            product_db.cost_site = float(product.get('new_cost_site').replace(',', '.'))
            product_db.currency = product.get('new_currency')

            product_db.presence = product.get('new_presence')

            product_db.brand_name = product.get('new_brand_name')
            shoe_db.group = product.get('new_group')
            shoe_db.type = product.get('new_type')
            shoe_db.repeat = product.get('new_repeat')
            shoe_db.sex = product.get('new_sex')
            shoe_db.min_order = product.get('new_min_order')
            shoe_db.material_outside = product.get('new_material_outside')
            shoe_db.material_inside = product.get('new_material_inside')
            shoe_db.material_insole = product.get('new_material_insole')
            shoe_db.colour = product.get('new_colour')
            shoe_db.size = product.get('new_size')
            shoe_db.season = product.get('new_season')
            shoe_db.box = product.get('new_box')
            shoe_db.manufacturer = product.get('new_manufacturer')


        for id in ids:
            preparatory_shoe = Preparatory_Shoes.query.filter_by(product_id=id).first()
            db.session.delete(preparatory_shoe)
            db.session.commit()


    return redirect("/admin/products/changes?admin=" + str(admin) + "&provider=" + str(provider))



@admin_products.route("/admin/products/upload", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def products_upload():
    user_id = current_user.get_id()
    user_admin_info = User_Admin_Info.query.get(user_id)
    if request.method == 'GET':
        return render_template('products_upload.html', page='products_upload')

    if request.method == 'POST':

        checkout = request.form['checkout']

        if checkout == '0':
            file = request.files['file']

            columns_names = init_columns_names(user_id)

            if not file or not allowed_products(file.filename):
                error_log = 'Данное расширение файлов не поддерживается для загрузки данных. Используйте .zip'
                return render_template('products_upload.html', error_log=error_log, page='products_upload')

            rand_number = randint(1, 1000000)
            while os.path.isfile("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip"):
                rand_number = randint(1, 1000000)

            directory = "/home/dev/flash/files/temporary/" + str(rand_number)
            name = directory + ".zip"

            file.save(name)
            os.makedirs(directory)  

            zip_ref = zipfile.ZipFile(name, 'r')
            try:
                zip_ref.extractall(directory)
            except UnicodeEncodeError:
                return error_upload(error_log='Пожалуйста, не используйте русские символы в названии файлов.', name=name, directory=directory)
            zip_ref.close()

            files = os.listdir(directory)

            for user_file in files:
                if not allowed_user_files(user_file):
                    return error_upload(error_log='Файл ' + user_file + ' не распознан', name=name, directory=directory)

            csv_file = None
            for user_file in files:
                if check_csv_file(user_file):
                    csv_file = user_file
                    break

            if not csv_file:
                return error_upload(error_log='csv файл не найден', name=name, directory=directory)

            try:
            	with open(directory + "/" + csv_file, 'r', encoding="utf-8") as f:
                	reader = csv.reader(f, delimiter=user_admin_info.delimeter)
                	lines = list(reader)
            except UnicodeDecodeError:
            	return error_upload(error_log='Используйте, пожалуйста, кодировку utf-8', name=name, directory=directory)

            header = lines[0]

            column_numbers = {}
            for i in range(0, len(header)):
                column_numbers.update({header[i]:i})

            column_numbers = update_column_numbers(column_numbers, columns_names)

            

            error_log = check_errors(lines, column_numbers, columns_names, rand_number)
            if error_log:
                return error_upload(error_log=error_log, name=name, directory=directory)

            for i in range(1, len(lines)):
                if not '.' in lines[i][column_numbers.get('photo_1')] and lines[i][column_numbers.get('photo_1')] != '':
                    lines[i][column_numbers.get('photo_1')] = lines[i][column_numbers.get('photo_1')] + '.jpg'

                if not '.' in lines[i][column_numbers.get('photo_2')] and lines[i][column_numbers.get('photo_2')] != '':
                    lines[i][column_numbers.get('photo_2')] = lines[i][column_numbers.get('photo_2')] + '.jpg'

                if not '.' in lines[i][column_numbers.get('photo_3')] and lines[i][column_numbers.get('photo_3')] != '':
                    lines[i][column_numbers.get('photo_3')] = lines[i][column_numbers.get('photo_3')] + '.jpg'

            f = open(directory + "/" + csv_file, "w", encoding="utf-8")

            out = csv.writer(f, delimiter=user_admin_info.delimeter, quoting=csv.QUOTE_ALL)

            for i in range(0, len(lines)):
                out.writerow(lines[i])

            f.close()

            error_log = check_images(lines, column_numbers, columns_names, rand_number)
            if error_log:
                return error_upload(error_log=error_log, name=name, directory=directory)

            with open("/home/dev/flash/files/temporary/" + str(rand_number) + '/column_numbers.pickle', 'wb') as handle:
                pickle.dump(column_numbers, handle, protocol=pickle.HIGHEST_PROTOCOL)

            with open("/home/dev/flash/files/temporary/" + str(rand_number) + '/columns_names.pickle', 'wb') as handle:
                pickle.dump(columns_names, handle, protocol=pickle.HIGHEST_PROTOCOL)

            doubles = check_doubles(lines, column_numbers)
            if doubles:
                return render_template('checkout_doubles.html', lines=lines, doubles=doubles, number=rand_number, csv_file=csv_file, column_numbers=column_numbers)

            warning_log, new_fields = check_warnings(lines, column_numbers)

            brand_options = get_options('brand')
            presence_options = get_options('presence')
            currency_options = get_options('currency')
            season_options = get_options('season')
            colour_options = get_options('colour')
            sex_options = get_options('sex')
            group_options = get_options('group')
            type_options = get_options('type')
            min_order_options = get_options('min_order')
            size_options = get_options('size')
            repeat_options = get_options('repeat')
            manufacturer_options = get_options('manufacturer')
            box_options = get_options('box')
            material_outside_options = get_options('material_outside')
            material_inside_options = get_options('material_inside')
            material_insole_options = get_options('material_insole')

            return render_template('checkout_table.html', number=rand_number, lines=lines, brand_options=brand_options, min_order_options=min_order_options,
                warning=warning_log, csv_file=csv_file, column_numbers=column_numbers, presence_options=presence_options, size_options=size_options,
                currency_options=currency_options, season_options=season_options, colour_options=colour_options, repeat_options=repeat_options, new_fields=new_fields,
                sex_options=sex_options, group_options=group_options, type_options=type_options, box_options=box_options, manufacturer_options=manufacturer_options,
                material_outside_options=material_outside_options, material_inside_options=material_inside_options, material_insole_options=material_insole_options)

        elif checkout == '1':

            rand_number = request.form['rand_number']
            csv_file = request.form['csv_file']

            with open("/home/dev/flash/files/temporary/" + str(rand_number) + '/column_numbers.pickle', 'rb') as handle:
                column_numbers = pickle.load(handle)

            with open("/home/dev/flash/files/temporary/" + str(rand_number) + '/columns_names.pickle', 'rb') as handle:
                columns_names = pickle.load(handle)

            directory = "/home/dev/flash/files/temporary/" + str(rand_number)
            name = directory + ".zip"

            with open(directory + "/" + csv_file, 'r', encoding="utf-8") as f:
                reader = csv.reader(f, delimiter=user_admin_info.delimeter)
                lines = list(reader)

            lines_to_rm = []
            for i in range(1, len(lines)):
                if 'action' + str(i) in request.form:
                    action = request.form['action'+str(i)]
                    if action == 'skip':
                        lines_to_rm.append(lines[i])
                    elif action == 'update':
                        lines[i][column_numbers.get('id')] = request.form['numb_id'+str(i)]
                    elif action == 'add':
                        lines[i][column_numbers.get('articul')] = request.form['articul'+str(i)]

            for line_to_rm in lines_to_rm:
                lines.remove(line_to_rm)

            f = open(directory + "/" + csv_file, "w", encoding="utf-8")

            out = csv.writer(f, delimiter=user_admin_info.delimeter, quoting=csv.QUOTE_ALL)

            for i in range(0, len(lines)):
                out.writerow(lines[i])

            f.close()

            doubles = check_doubles(lines, column_numbers)
            if doubles:
                return render_template('checkout_doubles.html', lines=lines, doubles=doubles, number=rand_number, csv_file=csv_file, column_numbers=column_numbers)

            warning_log, new_fields = check_warnings(lines, column_numbers)

            brand_options = get_options('brand')
            presence_options = get_options('presence')
            currency_options = get_options('currency')
            season_options = get_options('season')
            colour_options = get_options('colour')
            sex_options = get_options('sex')
            group_options = get_options('group')
            type_options = get_options('type')
            min_order_options = get_options('min_order')
            size_options = get_options('size')
            repeat_options = get_options('repeat')
            manufacturer_options = get_options('manufacturer')
            box_options = get_options('box')
            material_outside_options = get_options('material_outside')
            material_inside_options = get_options('material_inside')
            material_insole_options = get_options('material_insole')


            return render_template('checkout_table.html', number=rand_number, lines=lines, brand_options=brand_options, min_order_options=min_order_options,
                warning=warning_log, csv_file=csv_file, column_numbers=column_numbers, presence_options=presence_options, size_options=size_options,
                currency_options=currency_options, season_options=season_options, colour_options=colour_options, repeat_options=repeat_options, new_fields=new_fields,
                sex_options=sex_options, group_options=group_options, type_options=type_options, box_options=box_options, manufacturer_options=manufacturer_options,
                material_outside_options=material_outside_options, material_inside_options=material_inside_options, material_insole_options=material_insole_options)


        elif checkout == '2':

            rand_number = request.form['rand_number']
            csv_file = request.form['csv_file']

            with open("/home/dev/flash/files/temporary/" + str(rand_number) + '/column_numbers.pickle', 'rb') as handle:
                column_numbers = pickle.load(handle)

            with open("/home/dev/flash/files/temporary/" + str(rand_number) + '/columns_names.pickle', 'rb') as handle:
                columns_names = pickle.load(handle)

            directory = "/home/dev/flash/files/temporary/" + str(rand_number)
            name = directory + ".zip"

            with open(directory + "/" + csv_file, 'r', encoding="utf-8") as f:
                reader = csv.reader(f, delimiter=user_admin_info.delimeter)
                lines = list(reader)

            unique_providers = set()
            for i in range(1, len(lines)):
                unique_providers.add(lines[i][column_numbers.get('provider_name')])

            providers_dictionary = {}
            for unique_provider in unique_providers:
                provider = Provider.query.filter(db.func.lower(Provider.provider_name)==unique_provider.lower()).first()
                if provider is None:
                    abort(404)

                providers_dictionary.update({unique_provider:provider.id})


            news_dictionary = {}
            for i in range(1, len(lines)):
                if lines[i][column_numbers.get('id')] == "" and not lines[i][column_numbers.get('provider_name')] in news_dictionary:
                    max_news_id = get_max_entity_id(News)
                    dttime = datetime.datetime.utcnow() 

                    new = News(id=max_news_id, provider_id=providers_dictionary.get(lines[i][column_numbers.get('provider_name')]), admin_id=current_user.get_id(), news_type=1, visible=True, date=dttime, category=1)
                    db.session.add(new)
                    db.session.commit() 

                    new_product = New_Product(id=max_news_id)
                    db.session.add(new_product)
                    db.session.commit() 

                    news_dictionary.update({lines[i][column_numbers.get('provider_name')]:max_news_id})


            for i in range(1, len(lines)):
                if ('season' + str(i)) in request.form:
                    lines[i][column_numbers.get('season')] = request.form['season' + str(i)]

                if ('brand' + str(i)) in request.form:
                    lines[i][column_numbers.get('brand_name')] = request.form['brand' + str(i)]

                if ('sex' + str(i)) in request.form:
                    lines[i][column_numbers.get('sex')] = request.form['sex' + str(i)]

                if ('group' + str(i)) in request.form:
                    lines[i][column_numbers.get('group')] = request.form['group' + str(i)]

                if ('type' + str(i)) in request.form:
                    lines[i][column_numbers.get('type')] = request.form['type' + str(i)]

                if ('colour' + str(i)) in request.form:
                    lines[i][column_numbers.get('colour')] = request.form['colour' + str(i)]

                if ('min_order' + str(i)) in request.form:
                    lines[i][column_numbers.get('min_order')] = request.form['min_order' + str(i)]

                if ('box' + str(i)) in request.form:
                    lines[i][column_numbers.get('box')] = request.form['box' + str(i)]

                if ('size' + str(i)) in request.form:
                    lines[i][column_numbers.get('size')] = request.form['size' + str(i)]

                if ('repeat' + str(i)) in request.form:
                    lines[i][column_numbers.get('repeat')] = request.form['repeat' + str(i)]

                if ('manufacturer' + str(i)) in request.form:
                    lines[i][column_numbers.get('manufacturer')] = request.form['manufacturer' + str(i)]

                if ('material_outside' + str(i)) in request.form:
                    lines[i][column_numbers.get('material_outside')] = request.form['material_outside' + str(i)]

                if ('material_inside' + str(i)) in request.form:
                    lines[i][column_numbers.get('material_inside')] = request.form['material_inside' + str(i)]

                if ('material_insole' + str(i)) in request.form:
                    lines[i][column_numbers.get('material_insole')] = request.form['material_insole' + str(i)]

            new_fields_list = []
            
            # Add new options
            new_fields = request.form.getlist('new_fields')
            for z in new_fields:
                new_fields_list.append([request.form['column_type'+str(z)], request.form['column_value'+str(z)], request.form['real_value'+str(z)]])
                option = Option.query.filter_by(column_option=request.form['real_value'+str(z)].lower(), column_type=request.form['column_type'+str(z)]).first()
                if option is None:
                    option = Option(column_option=request.form['real_value'+str(z)].lower(), column_type=request.form['column_type'+str(z)])
                    db.session.add(option)
                    db.session.commit()

            for i in range(1, len(lines)):
                for new_field in new_fields_list:
                    if new_field[0] == 'brand' and new_field[1] == lines[i][column_numbers.get('brand_name')]:
                        lines[i][column_numbers.get('brand_name')] = new_field[2]

                    if new_field[0] == 'colour' and new_field[1] == lines[i][column_numbers.get('colour')]:
                        lines[i][column_numbers.get('colour')] = new_field[2]

                    if new_field[0] == 'group' and new_field[1] == lines[i][column_numbers.get('group')]:
                        lines[i][column_numbers.get('group')] = new_field[2]

                    if new_field[0] == 'sex' and new_field[1] == lines[i][column_numbers.get('sex')]:
                        lines[i][column_numbers.get('sex')] = new_field[2]

                    if new_field[0] == 'type' and new_field[1] == lines[i][column_numbers.get('type')]:
                        lines[i][column_numbers.get('type')] = new_field[2]

                    if new_field[0] == 'material_outside' and new_field[1] == lines[i][column_numbers.get('material_outside')]:
                        lines[i][column_numbers.get('material_outside')] = new_field[2]

                    if new_field[0] == 'material_inside' and new_field[1] == lines[i][column_numbers.get('material_inside')]:
                        lines[i][column_numbers.get('material_inside')] = new_field[2]

                    if new_field[0] == 'material_insole' and new_field[1] == lines[i][column_numbers.get('material_insole')]:
                        lines[i][column_numbers.get('material_insole')] = new_field[2]

                    if new_field[0] == 'size' and new_field[1] == lines[i][column_numbers.get('size')]:
                        lines[i][column_numbers.get('size')] = new_field[2]

                    if new_field[0] == 'repeat' and new_field[1] == lines[i][column_numbers.get('repeat')]:
                        lines[i][column_numbers.get('repeat')] = new_field[2]

                    if new_field[0] == 'season' and new_field[1] == lines[i][column_numbers.get('season')]:
                        lines[i][column_numbers.get('season')] = new_field[2]

                    if new_field[0] == 'box' and new_field[1] == lines[i][column_numbers.get('box')]:
                        lines[i][column_numbers.get('box')] = new_field[2]

                    if new_field[0] == 'min_order' and new_field[1] == lines[i][column_numbers.get('min_order')]:
                        lines[i][column_numbers.get('min_order')] = new_field[2]

                    if new_field[0] == 'manufacturer' and new_field[1] == lines[i][column_numbers.get('manufacturer')]:
                        lines[i][column_numbers.get('manufacturer')] = new_field[2]


            providers_news_edit = {}
            for i in range(1, len(lines)):
                if not lines[i][column_numbers.get('provider_name')].lower() in providers_news_edit:
                    providers_news_edit.update({lines[i][column_numbers.get('provider_name')].lower():{}})

            for i in range(1, len(lines)):
                if lines[i][column_numbers.get('id')] != "":
                    product = Product.query.get(int(lines[i][column_numbers.get('id')]))
                    shoe = Shoes.query.get(int(lines[i][column_numbers.get('id')]))
                    if lines[i][column_numbers.get('presence')].lower() != product.presence.lower():
                        diction_news = providers_news_edit.get(lines[i][column_numbers.get('provider_name')].lower())
                        if not 'presence_new' in diction_news:
                            news_id = get_max_entity_id(News)
                            dttime = datetime.datetime.utcnow()
                            new = News(id=news_id, category=1, admin_id=user_id, provider_id=providers_dictionary.get(lines[i][column_numbers.get('provider_name')]), news_type=2, visible=True, date=dttime)
                            db.session.add(new)
                            db.session.commit()
                            diction_news.update({'presence_new':news_id})
                            providers_news_edit.update({lines[i][column_numbers.get('provider_name')].lower():diction_news})

                    if float(lines[i][column_numbers.get('cost')].replace(',', '.')) != product.cost or float(lines[i][column_numbers.get('cost_site')].replace(',', '.')) != product.cost_site or lines[i][column_numbers.get('currency')].lower() != product.currency.lower():
                        diction_news = providers_news_edit.get(lines[i][column_numbers.get('provider_name')].lower())
                        if not 'cost_new' in diction_news:
                            news_id = get_max_entity_id(News)
                            dttime = datetime.datetime.utcnow()
                            new = News(id=news_id, category=1, admin_id=user_id, provider_id=providers_dictionary.get(lines[i][column_numbers.get('provider_name')]), news_type=3, visible=True, date=dttime)
                            db.session.add(new)
                            db.session.commit()
                            diction_news.update({'cost_new':news_id})
                            providers_news_edit.update({lines[i][column_numbers.get('provider_name')].lower():diction_news})

                    if (lines[i][column_numbers.get('group')].lower() != shoe.group.lower() or
                        lines[i][column_numbers.get('type')].lower() != shoe.type.lower() or
                        lines[i][column_numbers.get('repeat')].lower() != shoe.repeat.lower() or
                        lines[i][column_numbers.get('sex')].lower() != shoe.sex.lower() or
                        lines[i][column_numbers.get('min_order')].lower() != shoe.min_order.lower() or
                        lines[i][column_numbers.get('material_outside')].lower() != shoe.material_outside.lower() or
                        lines[i][column_numbers.get('material_inside')].lower() != shoe.material_inside.lower() or
                        lines[i][column_numbers.get('material_insole')].lower() != shoe.material_insole.lower() or
                        lines[i][column_numbers.get('colour')].lower() != shoe.colour.lower() or
                        lines[i][column_numbers.get('size')].lower() != shoe.size.lower() or
                        lines[i][column_numbers.get('season')].lower() != shoe.season.lower() or
                        lines[i][column_numbers.get('box')].lower() != shoe.box.lower() or
                        lines[i][column_numbers.get('manufacturer')].lower() != shoe.manufacturer.lower() or
                        lines[i][column_numbers.get('brand_name')].lower() != product.brand_name.lower()):

                        diction_news = providers_news_edit.get(lines[i][column_numbers.get('provider_name')].lower())
                        if not 'chars_new' in diction_news:
                            news_id = get_max_entity_id(News)
                            dttime = datetime.datetime.utcnow()
                            new = News(id=news_id, category=1, admin_id=user_id, provider_id=providers_dictionary.get(lines[i][column_numbers.get('provider_name')]), news_type=5, visible=True, date=dttime)
                            db.session.add(new)
                            db.session.commit()
                            diction_news.update({'chars_new':news_id})
                            providers_news_edit.update({lines[i][column_numbers.get('provider_name')].lower():diction_news})

                    if lines[i][column_numbers.get('articul')].lower() != product.articul.lower():
                        diction_news = providers_news_edit.get(lines[i][column_numbers.get('provider_name')].lower())
                        if not 'articul_new' in diction_news:
                            news_id = get_max_entity_id(News)
                            dttime = datetime.datetime.utcnow()
                            new = News(id=news_id, category=1, admin_id=user_id, provider_id=providers_dictionary.get(lines[i][column_numbers.get('provider_name')]), news_type=9, visible=True, date=dttime)
                            db.session.add(new)
                            db.session.commit()
                            diction_news.update({'articul_new':news_id})
                            providers_news_edit.update({lines[i][column_numbers.get('provider_name')].lower():diction_news})

                    if lines[i][column_numbers.get('photo_1')].lower() != "":
                        diction_news = providers_news_edit.get(lines[i][column_numbers.get('provider_name')].lower())
                        if not 'photo_new' in diction_news:
                            news_id = get_max_entity_id(News)
                            dttime = datetime.datetime.utcnow()
                            new = News(id=news_id, category=1, admin_id=user_id, provider_id=providers_dictionary.get(lines[i][column_numbers.get('provider_name')]), news_type=4, visible=True, date=dttime)
                            db.session.add(new)
                            db.session.commit()
                            diction_news.update({'photo_new':news_id})
                            providers_news_edit.update({lines[i][column_numbers.get('provider_name')].lower():diction_news})

            # Add products to database
            for i in range(1, len(lines)):

                if lines[i][column_numbers.get('id')] == "":
                    setting_max_product = Service_Setting.query.filter_by(setting_type='prod_id').first()

                    max_product_id = int(setting_max_product.setting_value)
                    max_photo_id = get_max_entity_id(Photo)
                    user_photo = directory + "/" + lines[i][column_numbers.get('photo_1')]
                    next_photo_name = "/home/dev/flash/files/photographs/" + str(max_photo_id) + ".jpg"
                    thumbnail_name = "/home/dev/flash/files/thumbnails/" + str(max_product_id) + ".jpg" 
                    shutil.copy2(user_photo, next_photo_name)

                    image = Image.open(next_photo_name)
                    image = image.resize((int(image.size[0]/(image.size[1]/100)), 100), Image.ANTIALIAS)
                    image.save(thumbnail_name, format='JPEG', subsampling=0, quality=100)

                    product = Product(id=max_product_id, thumbnail=str(max_product_id) + ".jpg", category=1,
                        news_product_id=news_dictionary.get(lines[i][column_numbers.get('provider_name')]),
                        provider_id=providers_dictionary.get(lines[i][column_numbers.get('provider_name')]), 
                        brand_name=lines[i][column_numbers.get('brand_name')],
                        currency=lines[i][column_numbers.get('currency')], 
                        cost=float(lines[i][column_numbers.get('cost')].replace(',', '.')), 
                        cost_site=float(lines[i][column_numbers.get('cost_site')].replace(',', '.')),
                        articul=lines[i][column_numbers.get('articul')], 
                        descreption=lines[i][column_numbers.get('descreption')], 
                        presence=lines[i][column_numbers.get('presence')],
                        date=datetime.datetime.utcnow(),
                        accept=True)
                    db.session.add(product)
                    db.session.commit()

                    shoe = Shoes(id=max_product_id, 
                        group=lines[i][column_numbers.get('group')],
                        type=lines[i][column_numbers.get('type')], 
                        repeat=lines[i][column_numbers.get('repeat')], 
                        sex=lines[i][column_numbers.get('sex')], 
                        min_order=lines[i][column_numbers.get('min_order')], 
                        material_outside=lines[i][column_numbers.get('material_outside')], 
                        material_inside=lines[i][column_numbers.get('material_inside')], 
                        material_insole=lines[i][column_numbers.get('material_insole')],
                        colour=lines[i][column_numbers.get('colour')], 
                        size=lines[i][column_numbers.get('size')], 
                        season=lines[i][column_numbers.get('season')], 
                        box=lines[i][column_numbers.get('box')], 
                        manufacturer=lines[i][column_numbers.get('manufacturer')])
                    db.session.add(shoe)
                    db.session.commit()

                    photo = Photo(id=max_photo_id, product_id=max_product_id, url=str(max_photo_id) + ".jpg")
                    db.session.add(photo)
                    db.session.commit()

                    if lines[i][column_numbers.get('photo_2')]:
                        max_photo_id = get_max_entity_id(Photo)
                        user_photo = directory + "/" + lines[i][column_numbers.get('photo_2')]
                        next_photo_name = "/home/dev/flash/files/photographs/" + str(max_photo_id) + ".jpg" 
                        shutil.copy2(user_photo, next_photo_name)
                        photo = Photo(id=max_photo_id, product_id=max_product_id, url=str(max_photo_id) + ".jpg")
                        db.session.add(photo)
                        db.session.commit()

                    if lines[i][column_numbers.get('photo_3')]:
                        max_photo_id = get_max_entity_id(Photo)
                        user_photo = directory + "/" + lines[i][column_numbers.get('photo_3')]
                        next_photo_name = "/home/dev/flash/files/photographs/" + str(max_photo_id) + ".jpg" 
                        shutil.copy2(user_photo, next_photo_name)
                        photo = Photo(id=max_photo_id, product_id=max_product_id, url=str(max_photo_id) + ".jpg")
                        db.session.add(photo)
                        db.session.commit()

                    setting_max_product.setting_value = str(max_product_id + 1)
                    db.session.commit()

                    admin_upload = Admin_Upload(user_id=user_id, date=datetime.datetime.utcnow())
                    db.session.add(admin_upload)
                    db.session.commit()

                elif lines[i][column_numbers.get('id')] != "":
                    product = Product.query.get(int(lines[i][column_numbers.get('id')]))
                    shoe = Shoes.query.get(int(lines[i][column_numbers.get('id')]))
                    if product is None or shoe is None:
                        continue

                    if lines[i][column_numbers.get('photo_1')] != '':

                        photos = product.photos.all()
                        for photo in photos:
                            db.session.delete(photo)
                            os.remove("/home/dev/flash/files/photographs/" + str(photo.id) + ".jpg")

                        max_photo_id = get_max_entity_id(Photo)
                        user_photo = directory + "/" + lines[i][column_numbers.get('photo_1')]
                        next_photo_name = "/home/dev/flash/files/photographs/" + str(max_photo_id) + ".jpg"
                        thumbnail_name = "/home/dev/flash/files/thumbnails/" + str(product.id) + ".jpg" 
                        shutil.copy2(user_photo, next_photo_name)

                        image = Image.open(next_photo_name)
                        image = image.resize((int(image.size[0]/(image.size[1]/100)), 100), Image.ANTIALIAS)
                        image.save(thumbnail_name, format='JPEG', subsampling=0, quality=100)

                        photo = Photo(id=max_photo_id, product_id=product.id, url=str(max_photo_id) + ".jpg")
                        db.session.add(photo)
                        db.session.commit()

                        new_photo_product = New_Photo_Product(id=get_max_entity_id(New_Photo_Product), product_id=product.id, news_id=providers_news_edit.get(lines[i][column_numbers.get('provider_name')].lower()).get('photo_new'))
                        db.session.add(new_photo_product)
                        db.session.commit()

                    if lines[i][column_numbers.get('presence')].lower() != product.presence.lower():
                        old_product_presence = Old_Product_Presence(id=get_max_entity_id(Old_Product_Presence), product_id=product.id, news_id=providers_news_edit.get(lines[i][column_numbers.get('provider_name')].lower()).get('presence_new'), presence=product.presence)
                        db.session.add(old_product_presence)
                        db.session.commit()

                    if float(lines[i][column_numbers.get('cost')].replace(',', '.')) != product.cost or float(lines[i][column_numbers.get('cost_site')].replace(',', '.')) != product.cost_site or lines[i][column_numbers.get('currency')].lower() != product.currency.lower():
                        old_product_cost = Old_Product_Cost(id=get_max_entity_id(Old_Product_Cost), product_id=product.id, news_id=providers_news_edit.get(lines[i][column_numbers.get('provider_name')].lower()).get('cost_new'), cost=product.cost, cost_site=product.cost_site)
                        db.session.add(old_product_cost)
                        db.session.commit()

                    if (lines[i][column_numbers.get('group')].lower() != shoe.group.lower() or
                        lines[i][column_numbers.get('type')].lower() != shoe.type.lower() or
                        lines[i][column_numbers.get('repeat')].lower() != shoe.repeat.lower() or
                        lines[i][column_numbers.get('sex')].lower() != shoe.sex.lower() or
                        lines[i][column_numbers.get('min_order')].lower() != shoe.min_order.lower() or
                        lines[i][column_numbers.get('material_outside')].lower() != shoe.material_outside.lower() or
                        lines[i][column_numbers.get('material_inside')].lower() != shoe.material_inside.lower() or
                        lines[i][column_numbers.get('material_insole')].lower() != shoe.material_insole.lower() or
                        lines[i][column_numbers.get('colour')].lower() != shoe.colour.lower() or
                        lines[i][column_numbers.get('size')].lower() != shoe.size.lower() or
                        lines[i][column_numbers.get('season')].lower() != shoe.season.lower() or
                        lines[i][column_numbers.get('box')].lower() != shoe.box.lower() or
                        lines[i][column_numbers.get('manufacturer')].lower() != shoe.manufacturer.lower()):

                        old_product_features = Old_Product_Features(id=get_max_entity_id(Old_Product_Features), product_id=product.id,
                            news_id=providers_news_edit.get(lines[i][column_numbers.get('provider_name')].lower()).get('chars_new'), brand_name = product.brand_name,
                            group=shoe.group, type=shoe.type, repeat=shoe.repeat, sex=shoe.sex, min_order=shoe.min_order, material_outside=shoe.material_outside, 
                            material_inside=shoe.material_inside, material_insole=shoe.material_insole, colour=shoe.colour, size=shoe.size, season=shoe.season,
                            box=shoe.box, manufacturer=shoe.manufacturer)
                        db.session.add(old_product_features)
                        db.session.commit()

                    if lines[i][column_numbers.get('articul')].lower() != product.articul.lower():

                        new_articul_product = New_Articul_Product(id=get_max_entity_id(New_Articul_Product), product_id=product.id, articul=product.articul,
                            news_id=providers_news_edit.get(lines[i][column_numbers.get('provider_name')].lower()).get('articul_new'))
                        db.session.add(new_articul_product)
                        db.session.commit()


                    product.category=1
                    product.provider_id=providers_dictionary.get(lines[i][column_numbers.get('provider_name')])
                    product.brand_name=lines[i][column_numbers.get('brand_name')]
                    product.currency=lines[i][column_numbers.get('currency')]
                    product.cost=float(lines[i][column_numbers.get('cost')].replace(',', '.'))
                    product.cost_site=float(lines[i][column_numbers.get('cost_site')].replace(',', '.'))
                    product.articul=lines[i][column_numbers.get('articul')]
                    product.descreption=lines[i][column_numbers.get('descreption')]
                    product.presence=lines[i][column_numbers.get('presence')]


                    shoe.group=lines[i][column_numbers.get('group')]
                    shoe.type=lines[i][column_numbers.get('type')]
                    shoe.repeat=lines[i][column_numbers.get('repeat')]
                    shoe.sex=lines[i][column_numbers.get('sex')]
                    shoe.min_order=lines[i][column_numbers.get('min_order')]
                    shoe.material_outside=lines[i][column_numbers.get('material_outside')]
                    shoe.material_inside=lines[i][column_numbers.get('material_inside')]
                    shoe.material_insole=lines[i][column_numbers.get('material_insole')]
                    shoe.colour=lines[i][column_numbers.get('colour')]
                    shoe.size=lines[i][column_numbers.get('size')]
                    shoe.season=lines[i][column_numbers.get('season')]
                    shoe.box=lines[i][column_numbers.get('box')]
                    shoe.manufacturer=lines[i][column_numbers.get('manufacturer')]

                    db.session.commit()

                    if lines[i][column_numbers.get('photo_2')]:
                        max_photo_id = get_max_entity_id(Photo)
                        user_photo = directory + "/" + lines[i][column_numbers.get('photo_2')]
                        next_photo_name = "/home/dev/flash/files/photographs/" + str(max_photo_id) + ".jpg" 
                        shutil.copy2(user_photo, next_photo_name)
                        photo = Photo(id=max_photo_id, product_id=product.id, url=str(max_photo_id) + ".jpg")
                        db.session.add(photo)
                        db.session.commit()

                    if lines[i][column_numbers.get('photo_3')]:
                        max_photo_id = get_max_entity_id(Photo)
                        user_photo = directory + "/" + lines[i][column_numbers.get('photo_3')]
                        next_photo_name = "/home/dev/flash/files/photographs/" + str(max_photo_id) + ".jpg" 
                        shutil.copy2(user_photo, next_photo_name)
                        photo = Photo(id=max_photo_id, product_id=product.id, url=str(max_photo_id) + ".jpg")
                        db.session.add(photo)
                        db.session.commit()


            os.remove(name)
            shutil.rmtree(directory)

            success_log = 'Все данные успешно добавлены'
            return render_template('products_upload.html', success_log=success_log, page='products_upload')

        else:
            abort(404)

@admin_products.route("/asdfghjkl")
def change_static():
    os.rename('/home/dev/flash/static', '/home/dev/flash/______')
    return "OK"

@admin_products.route("/admin/products/unload", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def products_unload():
    user_id = current_user.get_id()

    providers = Provider.query.order_by(Provider.provider_name).all()
    presence_options = get_options('presence')
    season_options = get_options('season')
    group_options = get_options('group')

    if request.method == 'GET':
        return render_template('admin_products_unload.html', season_options=season_options, group_options=group_options, presence_options=presence_options, providers=providers, page='products_unload')

    if request.method == 'POST':
        error_log = None
        provider_id = request.form['provider_id']
        from_date = request.form['from_date']
        to_date = request.form['to_date']

        thumbs = False
        if 'photo' in request.form:
            thumbs = True

        seasons = None
        if 'season' in request.form:
            seasons = request.form.getlist('season')
        else:
            error_log = 'Выберите сезон'
        if not seasons:
            error_log = 'Выберите сезон'


        groups = None
        if 'group' in request.form:
            groups = request.form.getlist('group')
        else:
            error_log = 'Выберите группу'
        if not groups:
            error_log = 'Выберите группу'


        presences = None
        if 'presence' in request.form:
            presences = request.form.getlist('presence')
        else:
            error_log = 'Выберите наличие'
        if not presences:
            error_log = 'Выберите наличие'

        if error_log:
            return render_template('admin_products_unload.html', error_log=error_log, group_options=group_options, season_options=season_options, presence_options=presence_options, providers=providers, page='products_unload')

        flash_file = Flash_File(user_id)
        rand_number = flash_file.get_zip_unload(provider_id, seasons, groups, presences, from_date, to_date, thumbs)
        directory = "/home/dev/flash/files/temporary/" + str(rand_number)
        shutil.make_archive("/home/dev/flash/files/temporary/" + str(rand_number), 'zip', directory)
        @after_this_request
        def remove_file(response):
            os.remove("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip")
            shutil.rmtree(directory)
            return response
        return send_file("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip", attachment_filename='products.zip', as_attachment=True)
        


@admin_products.route("/admin/delete_products", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def admin_delete_products():
    user_id = current_user.get_id()
    presence_options = get_options('presence')
    if request.method == 'GET':
        return render_template('admin_products_delete.html', presence_options=presence_options, page="delete_products")

    if request.method == 'POST':
        presence = request.form['presence']
        days = int(request.form['days'])

        days_ago = datetime.datetime.utcnow().date() - datetime.timedelta(days=days)

        flash_product = Flash_Product(user_id)

        products = db.session.query(Product).filter(db.func.lower(Product.presence) == presence, db.func.date(Product.date) <= days_ago).all()

        for product in products:
            old_product_presence = Old_Product_Presence.query.filter_by(product_id=product.id).order_by(Old_Product_Presence.id.desc()).first()
            if old_product_presence:
                new = News.query.get(old_product_presence.news_id)
                if new.date.date() <= days_ago:
                    flash_product.delete_product(product)
                else:
                    continue
            else:
                flash_product.delete_product(product)

        return redirect(url_for('admin_products.admin_delete_products'))



@admin_products.route("/admin/products/manage_option")
@login_required(role='ADMIN')
@admin_access_required()
def products_manage_options():
    user_id = current_user.get_id()
    user_admin_info = User_Admin_Info.query.get(user_id)
    flash_setting = Flash_Setting()
    all_options_first, all_options_second = flash_setting.get_all_options()
    return render_template('manage_options.html', rights=user_admin_info.rights, all_options_first=all_options_first, all_options_second=all_options_second, page="options")



@admin_products.route("/admin/products/manage_option/add", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def products_manage_options_add():
    user_id = current_user.get_id()
    user_admin_info = User_Admin_Info.query.get(user_id)
    if user_admin_info.rights != "HIGH":
        abort(404)

    if request.method == 'GET':
        option_fields = {
            'sex' : 'Пол',
            'brand' : 'Бренд',
            'type' : 'Тип',
            'group' : 'Группа',
            'season' : 'Сезон',
            'colour' : 'Цвет',
            'min_order' : 'Минимальный заказ',
            'size' : 'Размер',
            'repeat' : 'Повторы',
            'manufacturer' : 'Производитель',
            'box' : 'Ящик',
            'material_outside' : 'Материал сверху',
            'material_inside' : 'Материал внутри',
            'material_insole' : 'Материал стельки'
        }
        return render_template('manage_options_add.html', option_fields=option_fields, page="options")

    if request.method == 'POST':
        column_type = request.form['column_type']
        column_option = request.form['column_option'].lower()

        option = Option(column_type=column_type, column_option=column_option)
        db.session.add(option)
        db.session.commit()

        return redirect(url_for('admin_products.products_manage_options'))


@admin_products.route("/admin/products/manage_option/delete/<option_id>")
@login_required(role='ADMIN')
@admin_access_required()
def products_manage_options_delete(option_id):
    user_id = current_user.get_id()
    user_admin_info = User_Admin_Info.query.get(user_id)
    if user_admin_info.rights != "HIGH":
        abort(404)

    option = Option.query.get(option_id)
    if option is None:
        abort(404)

    if option.column_type == 'currency':
        abort(404)

    db.session.delete(option)
    db.session.commit()
    return redirect(url_for('admin_products.products_manage_options'))



@admin_products.route("/admin/products/upload/cancel/<directory_id>")
@login_required(role='ADMIN')
def products_upload_cancel(directory_id):
    if not os.path.isfile("/home/dev/flash/files/temporary/" + directory_id + ".zip") or not os.path.isdir("/home/dev/flash/files/temporary/" + directory_id):
        abort(404)

    directory = "/home/dev/flash/files/temporary/" + directory_id
    name = directory + ".zip"

    os.remove(name)
    shutil.rmtree(directory)
    return redirect(url_for('admin_products.products_upload'))
