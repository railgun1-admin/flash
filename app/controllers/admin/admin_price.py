from flask import Blueprint, redirect, url_for, render_template, abort, send_file, after_this_request, request
from functools import wraps
from app.models import *
from config import login_manager
from flask_login import login_user, current_user, logout_user, AnonymousUserMixin
from PIL import Image, ImageEnhance
from random import randint
from app.flash_lib.flash_file import Flash_File
from app.flash_lib.flash_product import Flash_Product
from app.flash_lib.flash_news import Flash_News
from app.flash_lib.flash_user import Flash_User
from app.flash_lib.flash_setting import Flash_Setting
from app.flash_lib.flash_feedback import Flash_Feedback
from app.flash_lib.flash_price import Flash_Price
from urllib.parse import urlencode, unquote, quote
from dateutil.relativedelta import relativedelta
import datetime
from dateutil import tz
import csv
import itertools
import zipfile
import os
import shutil
import pickle


admin_price = Blueprint('admin_price', __name__, template_folder='../../templates/admin')

def get_options(column_type):
    options_db = Option.query.filter_by(column_type=column_type).all()
    options = []
    for option_db in options_db:
        options.append(option_db.column_option)
    return options
    

def get_max_entity_id(entity):
    max_enity_id = 0
    try:
        max_enity_id = db.session.query(db.func.max(entity.id)).scalar() + 1
    except TypeError:
        max_enity_id = 1
    return max_enity_id


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if current_user.is_anonymous:
                return redirect(url_for('main.login'))
            urole = current_user.get_urole()
            if ( (urole != role) and (role != "ANY")):
                return login_manager.unauthorized()      
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


def admin_access_required():
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            user_id = current_user.get_id()
            user = User.query.get(user_id)
            if user.role == 'ADMIN':
                user_admin_info = User_Admin_Info.query.get(user_id)
                if user_admin_info.rights == 'BLOCK':
                    return redirect(url_for('main.logout'))
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


@admin_price.route("/admin/price/download", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def download_price():
    flash_price = Flash_Price(current_user.get_id())

    providers = Provider.query.order_by(Provider.provider_name).all()
    presence_options = get_options('presence')
    season_options = get_options('season')
    group_options = get_options('group')
    max_date = db.session.query(db.func.max(Product.date)).first()[0].strftime("%Y-%m-%d")
    min_date = db.session.query(db.func.min(Product.date)).first()[0].strftime("%Y-%m-%d")

    price_info = dict()
    if 'price_id' in request.args:
        price_info = flash_price.get_price_info(request.args.get('price_id'))

    if request.method == 'GET':
        return render_template('products_price.html', season_options=season_options, group_options=group_options, presence_options=presence_options, providers=providers, page='price_download',
            from_date=min_date, to_date=max_date, price_info=price_info)

    if request.method == 'POST':
        error_log = None
        ids = None

        from_date = datetime.datetime.strptime(request.form['from_date'], "%Y-%m-%d")
        to_date = datetime.datetime.strptime(request.form['to_date'], "%Y-%m-%d").date()

        from_date -= relativedelta(days=1)
        from_date = from_date.replace(hour=22)

        if 'ids' in request.form:
            ids = request.form.getlist('ids')
        else:
            error_log = 'Выберите поставщиков'
        if not ids:
            error_log = 'Выберите поставщиков'

        seasons = None
        if 'season' in request.form:
            seasons = request.form.getlist('season')
        else:
            error_log = 'Выберите сезон'
        if not seasons:
            error_log = 'Выберите сезон'


        groups = None
        if 'group' in request.form:
            groups = request.form.getlist('group')
        else:
            error_log = 'Выберите группу'
        if not groups:
            error_log = 'Выберите группу'


        presences = None
        if 'presence' in request.form:
            presences = request.form.getlist('presence')
        else:
            error_log = 'Выберите наличие'
        if not presences:
            error_log = 'Выберите наличие'


        sorting = []
        sorting_price = []
        if 'sort' in request.form:
            sort = request.form.getlist('sort')
            if sort:
                for i in range(1, 5):
                    if 'articul' in sort and request.form['articul_order'] == str(i):
                        sorting.append(Product.articul)
                        sorting_price.append('articul')
                    if 'group' in sort and request.form['group_order'] == str(i):
                        sorting.append(Shoes.group)
                        sorting_price.append('group')
                    if 'season' in sort and request.form['season_order'] == str(i): 
                        sorting.append(Shoes.season)
                        sorting_price.append('season')

        if error_log:
            return render_template('products_price.html', error_log=error_log, group_options=group_options, season_options=season_options, presence_options=presence_options, providers=providers, page='price_download')

        photo = False
        if 'photo' in request.form:
            photo = True

        double_page = False
        if 'double_page' in request.form:
            double_page = True

        flash_file = Flash_File(current_user.get_id())
        if photo:
            file_name = flash_file.get_pdf_with_photo(ids, seasons, presences, groups, sorting, from_date, to_date, double_page)
        else:
            file_name = flash_file.get_pdf(ids, seasons, presences, groups, sorting, from_date, to_date, double_page)
        if file_name == False:
            return redirect(url_for('admin_price.download_price'))

        price_max_id = get_max_entity_id(Price)
        dttime = datetime.datetime.utcnow()
        price = Price(id=price_max_id, admin_id=current_user.get_id(), photo=photo, date_start=from_date, date_end=to_date, date=dttime)
        db.session.add(price)
        db.session.commit()

        for id in ids:
            price_provider = Price_Provider(price_id=price_max_id, provider_id=id)
            db.session.add(price_provider)
            db.session.commit()

        for presence in presences:
            price_filter = Price_Filter(price_id=price_max_id, column_type='presence', column_value=presence)
            db.session.add(price_filter)
            db.session.commit()

        for season in seasons:
            price_filter = Price_Filter(price_id=price_max_id, column_type='season', column_value=season)
            db.session.add(price_filter)
            db.session.commit()

        for group in groups:
            price_filter = Price_Filter(price_id=price_max_id, column_type='group', column_value=group)
            db.session.add(price_filter)
            db.session.commit()

        for i in range(1, len(sorting_price) + 1):
            price_sort = Price_Sort(price_id=price_max_id, column_type=sorting_price[i-1], order_number=i)
            db.session.add(price_sort)
            db.session.commit()

        @after_this_request
        def remove_file(response):
            os.remove(file_name)
            return response

        return send_file(file_name, attachment_filename='price.pdf', as_attachment=True)


@admin_price.route("/admin/price/all")
@login_required(role='ADMIN')
@admin_access_required()
def admin_price_all():
    if not 'page' in request.args:
        return redirect(url_for('admin_price.admin_price_all') + '?page=1')

    try:
        page = int(request.args.get('page'))
    except ValueError:
        abort(404)

    user_id = current_user.get_id()
    flash_price = Flash_Price(user_id)
    prices, pages_li, next, prev  = flash_price.get_prices(page)

    return render_template('admin_price_all_prices.html', prices=prices, page_pag=page, next=next, prev=prev, pages_li=pages_li, page='price_settings')


@admin_price.route("/admin/price/detail/<price_id>")
@login_required(role='ADMIN')
@admin_access_required()
def admin_price_detail(price_id):
    user_id = current_user.get_id()
    flash_price = Flash_Price(user_id)
    price_detail = flash_price.get_price_detail(price_id)
    return render_template('admin_price_detail.html', price_detail=price_detail,  page='price_settings')


@admin_price.route("/admin/price/delete/<price_id>")
@login_required(role='ADMIN')
@admin_access_required()
def admin_price_delete_price(price_id):
    user_id = current_user.get_id()
    flash_price = Flash_Price(user_id)
    flash_price.delete_price(price_id)
    return redirect(url_for('admin_price.admin_price_all'))


@admin_price.route("/admin/price/edit", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def edit_price():
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')
    providers = Provider.query.order_by(Provider.provider_name).all()
    if request.method == 'GET':
        return render_template('admin_price_providers.html', providers=providers, step="1", page='price_edit')

    if request.method == 'POST':
        error_log = None
        step = request.form['step']
        if step == '1':
            try:
                provider_id = int(request.form['provider'])
            except ValueError:
                abort(404)

            prices_db = db.session.query(Price).filter(Price_Provider.price_id==Price.id, Price_Provider.provider_id==provider_id).order_by(Price.id.desc()).all()
            prices = []
            for price_db in prices_db:
                price= {
                    'id' : price_db.id,
                    'date' : price_db.date.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%d-%m-%Y %H:%M")
                }
                prices.append(price)


            return render_template('admin_price_providers.html', prices=prices, provider_id=provider_id, step="2", page='price_edit')

        elif step == '2':
            try:
                price_id = int(request.form['price'])
                provider_id = int(request.form['provider'])
            except ValueError:
                abort(404)

            return redirect(url_for('admin_price.edit_price_products') + '?price_id={}&provider_id={}'.format(price_id, provider_id))




@admin_price.route("/admin/price/edit_price", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def edit_price_products():
    user_id = current_user.get_id()
    if not 'price_id' in request.args or not 'provider_id' in request.args:
        abort(404)

    try:
        price_id = int(request.args.get('price_id'))
        provider_id = int(request.args.get('provider_id'))
    except ValueError:
        abort(404)

    price = Price.query.get(price_id)
    if price is None:
        abort(404)

    provider = Provider.query.get(provider_id)
    if provider is None:
        abort(404)

    if Price_Provider.query.filter_by(provider_id=provider_id, price_id=price_id).first() is None:
        abort(404)

    if request.method == 'GET':
        flash_product = Flash_Product(user_id)
        pages = flash_product.get_products_by_price_provider(price_id, provider_id)
        if price.photo:
            return render_template('admin_price_edit_products_photo.html', pages=pages, provider_name=provider.provider_name, 
                price_id=price_id, provider_id=provider_id, page='price_edit')
        else:
            return render_template('admin_price_edit_products.html', pages=pages, provider_name=provider.provider_name, 
                price_id=price_id, provider_id=provider_id, page='price_edit')

    if request.method == 'POST':
        max_cost_news_id = 0
        max_pres_news_id = 0
        ids = request.form.getlist('ids')
        presences = dict()
        for id in ids:
            presences.update({id : 'нет' if 'presence' + str(id) in request.form else 'есть'})

        for id in ids:
            if request.form['cost' + str(id)] != '':
                try:
                    cost = float(request.form['cost'+str(id)])
                except ValueError:
                    continue

                max_cost_news_id = get_max_entity_id(News)
                dttime = datetime.datetime.utcnow()
                new = News(id=max_cost_news_id, provider_id=provider.id, admin_id=current_user.get_id(), news_type=3, visible=True, date=dttime, category=1)
                db.session.add(new)
                db.session.commit()
                break

            if request.form['cost_site' + str(id)] != '':
                try:
                    cost_site = float(request.form['cost_site'+str(id)])
                except ValueError:
                    continue

                max_cost_news_id = get_max_entity_id(News)
                dttime = datetime.datetime.utcnow()
                new = News(id=max_cost_news_id, provider_id=provider.id, admin_id=current_user.get_id(), news_type=3, visible=True, date=dttime, category=1)
                db.session.add(new)
                db.session.commit()
                break


        for id in ids:
            product = Product.query.get(id)
            if presences[id] != product.presence.lower():
                max_pres_news_id = get_max_entity_id(News)
                dttime = datetime.datetime.utcnow()
                new = News(id=max_pres_news_id, provider_id=provider.id, admin_id=current_user.get_id(), news_type=2, visible=True, date=dttime, category=1)
                db.session.add(new)
                db.session.commit()
                break


        for id in ids:
            product = Product.query.get(id)
            if request.form['cost' + str(id)] != '' or request.form['cost_site' + str(id)] != '':
                check_cost = False
                try:
                    cost = float(request.form['cost'+str(product.id)])
                except ValueError:
                    check_cost = True

                check_cost_site = False
                try:
                    cost_site = float(request.form['cost_site'+str(product.id)])
                except ValueError:
                    check_cost_site = True

                if request.form['cost' + str(id)] == '' or check_cost:
                    cost = product.cost

                if request.form['cost_site' + str(id)] == '' or check_cost_site:
                    cost_site = product.cost_site

                if check_cost and check_cost_site:
                    continue

                if cost_site <= cost:
                    continue

                max_old_cost_id = get_max_entity_id(Old_Product_Cost)
                old_product_cost = Old_Product_Cost(id=max_old_cost_id, news_id=max_cost_news_id, product_id=product.id, cost=product.cost, cost_site=product.cost_site)
                db.session.add(old_product_cost)
                db.session.commit()

                product.cost = cost
                product.cost_site = cost_site
                db.session.commit()

            if presences[id] != product.presence.lower():
                max_old_pres_id = get_max_entity_id(Old_Product_Presence)
                old_product_presence = Old_Product_Presence(id=max_old_pres_id, news_id=max_pres_news_id, product_id=product.id, presence=product.presence)
                db.session.add(old_product_presence)
                db.session.commit()

                product.presence = presences[id]
                db.session.commit()

        return redirect(url_for('admin_price.edit_price_products') + '?price_id={}&provider_id={}'.format(price_id, provider_id))



@admin_price.route("/admin/price/delete_old")
@login_required(role='ADMIN')
@admin_access_required()
def delete_old_price():
    user_id = current_user.get_id()
    flash_price = Flash_Price(user_id)

    dttime = datetime.datetime.utcnow()
    dttime -= relativedelta(days=7)
    prices = Price.query.filter(Price.date <= dttime).all()
    for price in prices:
        flash_price.delete_price(price.id)

    return redirect(url_for('admin_price.admin_price_all'))