from flask import Blueprint, redirect, url_for, render_template, abort, send_file, after_this_request, request
from functools import wraps
from app.models import *
from config import login_manager
from flask_login import login_user, current_user, logout_user, AnonymousUserMixin
from PIL import Image, ImageEnhance
from random import randint
from app.flash_lib.flash_file import Flash_File
from app.flash_lib.flash_product import Flash_Product
from app.flash_lib.flash_news import Flash_News
from app.flash_lib.flash_user import Flash_User
from app.flash_lib.flash_setting import Flash_Setting
from app.flash_lib.flash_feedback import Flash_Feedback
from urllib.parse import urlencode, unquote, quote
import datetime
from dateutil import tz
import csv
import itertools
import zipfile
import os
import shutil
import pickle


admin_news = Blueprint('admin_news', __name__, template_folder='../../templates/admin')

def get_options(column_type):
    options_db = Option.query.filter_by(column_type=column_type).all()
    options = []
    for option_db in options_db:
        options.append(option_db.column_option)
    return options
    

def get_max_entity_id(entity):
    max_enity_id = 0
    try:
        max_enity_id = db.session.query(db.func.max(entity.id)).scalar() + 1
    except TypeError:
        max_enity_id = 1
    return max_enity_id


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if current_user.is_anonymous:
                return redirect(url_for('main.login'))
            urole = current_user.get_urole()
            if ( (urole != role) and (role != "ANY")):
                return login_manager.unauthorized()      
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


def admin_access_required():
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            user_id = current_user.get_id()
            user = User.query.get(user_id)
            if user.role == 'ADMIN':
                user_admin_info = User_Admin_Info.query.get(user_id)
                if user_admin_info.rights == 'BLOCK':
                    return redirect(url_for('main.logout'))
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper
    

@admin_news.route("/admin/news")
@login_required(role='ADMIN')
@admin_access_required()
def manage_news():
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')

    max_date_db = db.session.query(db.func.max(News.date)).first()[0]
    min_date_db = db.session.query(db.func.min(News.date)).first()[0]

    if not max_date_db or not min_date_db:
        max_date_db = datetime.datetime.utcnow()
        min_date_db = datetime.datetime.utcnow()

    max_date = max_date_db.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")
    min_date = min_date_db.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")


    if not 'page' in request.args or not 'from_date' in request.args or not 'to_date' in request.args or not 'provider' in request.args or not 'type' in request.args or not 'admin' in request.args:
        return redirect('/admin/news?page=1&from_date={}&to_date={}&type=all&provider=all&admin=all'.format(min_date, max_date))

    filter = {
        'from_date' : datetime.datetime.strptime(request.args.get('from_date'), "%Y-%m-%d").replace(tzinfo=to_zone).date(),
        'to_date' : datetime.datetime.strptime(request.args.get('to_date'), "%Y-%m-%d").replace(tzinfo=to_zone).date(),
        'page' : request.args.get('page'),
        'provider' : request.args.get('provider'),
        'type' : request.args.get('type'),
        'admin' : request.args.get('admin')
    }

    user_id = current_user.get_id()

    flash_news = Flash_News(user_id)
    response = flash_news.get_all_news(filter)

    if response == False:
        abort(404)

    news, pages_li, next, prev = response

    admins = User_Admin_Info.query.order_by(User_Admin_Info.admin_name).all()
    providers = Provider.query.order_by(Provider.provider_name).all()

    if filter.get('admin') != 'all':
        filter.update({'admin' : int(filter.get('admin'))})

    if filter.get('provider') != 'all':
        filter.update({'provider' : int(filter.get('provider'))})

    return render_template('news.html', prev=prev, next=next, pages_li=pages_li, page_pag=int(filter.get('page')), news=news, page='news',
        admins=admins, providers=providers, url=request.url, max_date=max_date, min_date=min_date, provider=filter.get('provider'),
        admin=filter.get('admin'), from_date=request.args.get('from_date'), to_date=request.args.get('to_date'), type=filter.get('type'))


@admin_news.route("/admin/news/new_message", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def new_admin_message():
    if request.method == 'GET':
        return render_template('new_message.html', page='new_admin_message')

    if request.method == 'POST':
        tittle = request.form['tittle']
        message = request.form['message']

        max_news_id = get_max_entity_id(News)
        dttime = datetime.datetime.utcnow()
        news = News(id=max_news_id, admin_id=current_user.get_id(), news_type=8, visible=True, date=dttime, category=0)
        db.session.add(news)
        db.session.commit()

        max_message_id = get_max_entity_id(Admin_Message)
        admin_message = Admin_Message(id=max_message_id, tittle=tittle, message=message, news_id=max_news_id)
        db.session.add(admin_message)
        db.session.commit()

        return redirect(url_for('admin_news.manage_news'))


@admin_news.route("/admin/news/detail/<news_id>")
@login_required(role='ADMIN')
@admin_access_required()
def news_detail_admin(news_id):
    user_id = current_user.get_id()
    
    new = News.query.get(news_id)

    if new.news_type != 8:
        flash_product = Flash_Product(user_id)
        products = flash_product.get_products_by_news(news_id)
        if products == False:
            abort(404)

        displayed_fields = flash_product.get_displayed_fields()
        presence_options = get_options('presence')

        return render_template('admin_news_products.html', products=products,  displayed_fields=displayed_fields, page='news', news_type=new.news_type)
    else:
        admin_message = new.message
        return render_template('admin_news_message.html', admin_message=admin_message, page='news')


@admin_news.route("/admin/news/setting_news_time", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def news_setting_time():
    time_db = Service_Setting.query.filter_by(setting_type='news_active_days').first()

    if request.method == 'GET':
        return render_template('news_setting_time.html', time_db=time_db, page='news_time')
    
    if request.method == 'POST':
        time_req = request.form['time_req']
        time_db.setting_value = str(time_req)

        db.session.commit()
        return redirect(url_for('admin_news.news_setting_time'))


@admin_news.route("/admin/news/show/<news_id>")
@login_required(role='ADMIN')
@admin_access_required()
def news_show(news_id):
    user_id = current_user.get_id()
    flash_news = Flash_News(user_id)
    reply = flash_news.set_visible_news([news_id], True)
    if not reply:
        abort(404)
    url = request.args.get('url')
    return redirect(url)


@admin_news.route("/admin/news/hide/<news_id>")
@login_required(role='ADMIN')
@admin_access_required()
def news_hide(news_id):
    user_id = current_user.get_id()
    flash_news = Flash_News(user_id)
    reply = flash_news.set_visible_news([news_id], False)
    if not reply:
        abort(404)
    url = request.args.get('url')
    return redirect(url)


@admin_news.route("/admin/news/set_visible_news", methods=['POST'])
@login_required(role='ADMIN')
@admin_access_required()
def set_visible_news():
    url = request.form['url']
    ids = None
    if 'ids' in request.form:
        ids = request.form.getlist('ids')
    else:
        return redirect(url_for('admin_news.manage_news'))
    if not ids:
        return redirect(url_for('admin_news.manage_news'))

    action = request.form['action']

    visible = None
    if action == 'show':
        visible = True
    elif action == 'hide':
        visible = False
    else:
        abort(404)

    user_id = current_user.get_id()
    flash_news = Flash_News(user_id)
    reply = flash_news.set_visible_news(ids, visible)
    if not reply:
        abort(404)
    return redirect(url)