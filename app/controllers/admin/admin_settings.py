from flask import Blueprint, redirect, url_for, render_template, abort, send_file, after_this_request, request
from functools import wraps
from app.models import *
from config import login_manager
from flask_login import login_user, current_user, logout_user, AnonymousUserMixin
from PIL import Image, ImageEnhance
from random import randint
from app.flash_lib.flash_file import Flash_File
from app.flash_lib.flash_product import Flash_Product
from app.flash_lib.flash_news import Flash_News
from app.flash_lib.flash_user import Flash_User
from app.flash_lib.flash_setting import Flash_Setting
from app.flash_lib.flash_feedback import Flash_Feedback
from urllib.parse import urlencode, unquote, quote
import datetime
from dateutil import tz
import csv
import itertools
import zipfile
import os
import shutil

admin_settings = Blueprint('admin_settings', __name__, template_folder='../../templates/admin')

def get_max_entity_id(entity):
    max_enity_id = 0
    try:
        max_enity_id = db.session.query(db.func.max(entity.id)).scalar() + 1
    except TypeError:
        max_enity_id = 1
    return max_enity_id


def get_fields_dict():
    diction = {
        'id' : 'id',
        'name' : 'Наименование',
        'articul' : 'Артикул',
        'brand_name' : 'Бренд',
        'provider_name' : 'Поставщик',
        'cost' : 'Цена',
        'cost_site' : 'Цена сайт',
        'cost_uah' : 'Цена грн',
        'cost_site_uah' : 'Цена сайт грн',
        'cost_doll' : 'Цена $',
        'cost_site_doll' : 'Цена сайт $',
        'cost_euro' : 'Цена евро',
        'cost_site_euro' : 'Цена сайт евро',
        'currency' : 'Валюта',
        'sex' : 'Пол',
        'group' : 'Группа',
        'type' : 'Тип',
        'material_outside' : 'Материал верха',
        'material_inside' : 'Материал внутри',
        'material_insole' : 'Материал стельки',
        'empty' : 'Пустая',
        'size' : 'Размер',
        'repeat' : 'Повторы',
        'season' : 'Сезон',
        'colour' : 'Цвет',
        'box' : 'Ящик',
        'min_order' : 'Минимальный заказ',
        'manufacturer' : 'Производитель',
        'descreption' : 'Описание',
        'photo_1' : 'Фото1',
        'photo_2' : 'Фото2',
        'photo_3' : 'Фото3',
        'presence' : 'Наличие'
    }
    return diction


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if current_user.is_anonymous:
                return redirect(url_for('main.login'))
            urole = current_user.get_urole()
            if ( (urole != role) and (role != "ANY")):
                return login_manager.unauthorized()      
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


def admin_access_required():
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            user_id = current_user.get_id()
            user = User.query.get(user_id)
            if user.role == 'ADMIN':
                user_admin_info = User_Admin_Info.query.get(user_id)
                if user_admin_info.rights == 'BLOCK':
                    return redirect(url_for('main.logout'))
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


@admin_settings.route("/admin/setting/change_password", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def change_password():
    user_id = current_user.get_id()

    if request.method == 'GET':
        return render_template('admin_change_password.html', page='change_password')

    if request.method == 'POST':

        user = User.query.get(user_id)

        old_password = request.form['old_password']
        new_password = request.form['new_password']
        new_password_again = request.form['new_password_again']

        error_log = None

        if new_password != new_password_again:
            error_log = 'Пароли не совпадают'
            return render_template('admin_change_password.html', error_log=error_log, page='change_password')

        flash_user = Flash_User()
        resp = flash_user.change_password_store(user_id, old_password, new_password)

        if not resp:
            error_log = 'Старый пароль введён неверно.'
            
        if error_log:
            return render_template('admin_change_password.html', error_log=error_log, page='change_password')

        success_log = 'Пароль успешно обнволён'
        return render_template('admin_change_password.html', success_log=success_log, page='change_password')


@admin_settings.route("/admin/setting/upload_fields", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def setting_upload():
    user_id = current_user.get_id()
    user_admin_info = User_Admin_Info.query.get(user_id)

    uploads_fields_db = Upload_Field.query.filter_by(user_id=user_id).all()
    uploads_fields = dict()
    for upload_field_db in uploads_fields_db:
        uploads_fields.update({upload_field_db.column_type:upload_field_db})


    if request.method == 'GET':
        return render_template('admin_products_upload_parameters.html', delimeter=user_admin_info.delimeter, upload_fields=uploads_fields, page='setting_upload')

    if request.method == 'POST':
        for key, value in uploads_fields.items():
            value.column_name = request.form[key]

        db.session.commit()

        delimeter = request.form['delimeter']
        user_admin_info.delimeter = delimeter

        db.session.commit()

        return redirect(url_for('admin_settings.setting_upload'))


@admin_settings.route("/admin/setting/displayed_fields", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def setting_displayed_fields():
    user_id = current_user.get_id()

    if request.method == 'POST':
        displayed_fields_db = Displayed_Fields.query.filter_by(user_id=user_id).all()
        for displayed_field in displayed_fields_db:
            displayed_field.flag = True if displayed_field.field_name in request.form else False
        db.session.commit()

    flash_product = Flash_Product(user_id)
    displayed_fields = flash_product.get_displayed_fields()

    return render_template('displayed_fields.html', page='displayed_fields', displayed_fields=displayed_fields)


@admin_settings.route("/admin/setting/download_fields/table", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def setting_fields_table():
    user_id = current_user.get_id()

    diction = get_fields_dict()

    fields = Download_Field.query.filter_by(user_id=user_id).order_by(Download_Field.number_column).all()

    numbers = range(1, 52)


    if request.method == 'GET':
        return render_template('setting_fields_table.html', page='setting_fields', diction=diction, fields=fields, numbers=numbers)


    if request.method == 'POST':

        fields_quantity = Download_Field.query.filter_by(user_id=user_id).count()

        request_data = []

        for i in range(0, fields_quantity):
            request_data.append([int(request.form['number_column' + str(i)]), request.form['column_type' + str(i)], request.form['name' + str(i)]])

        for i in range(0, len(request_data)):
            for j in range(0, len(request_data)):
                if request_data[i][0] == request_data[j][0] and i != j :
                    error_log = 'Порядковый номер колонок должен быть уникальным'
                    return render_template('setting_fields_table.html', error_log=error_log, page='setting_fields', diction=diction, fields=fields, numbers=numbers)

        for i in range(0, len(fields)):
            fields[i].number_column = request_data[i][0]
            fields[i].column_type = request_data[i][1]
            fields[i].name_column = request_data[i][2]
            
        db.session.commit()

        return redirect(url_for('admin_settings.setting_fields_table'))



@admin_settings.route("/admin/setting/download_fields/add", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def setting_fields_add():

    user_id = current_user.get_id()

    diction = get_fields_dict()

    numbers = range(1, 52)

    if request.method == 'GET':
        return render_template('setting_fields_add.html', page='setting_fields', fields=diction, numbers=numbers)

    if request.method == 'POST':
        name = request.form['name']
        number = int(request.form['number'])
        column_type = request.form['column_type']

        if Download_Field.query.filter_by(user_id=user_id, number_column=number).first():
            return render_template('setting_fields_add.html', page='setting_fields', error_log='Номер ' + str(number) + ' уже используется', fields=diction, numbers=numbers)

        if Download_Field.query.filter_by(user_id=user_id, column_type=column_type).first():
            return render_template('setting_fields_add.html', page='setting_fields', error_log='Стобец ' + diction.get(column_type) + ' уже используется', fields=diction, numbers=numbers)


        field = Download_Field(user_id=user_id, number_column=number, column_type=column_type, name_column=name, category=1)
        db.session.add(field)
        db.session.commit()

        return redirect(url_for('admin_settings.setting_fields_table'))


@admin_settings.route("/admin/setting_fields/delete/<field_id>")
@login_required(role='ADMIN')
@admin_access_required()
def setting_fields_delete(field_id):
    user_id = current_user.get_id()

    field = Download_Field.query.filter_by(user_id=user_id, id=field_id).first()
    if not field:
        abort(404)

    db.session.delete(field)
    db.session.commit()

    return redirect(url_for('admin_settings.setting_fields_table'))
