from flask import Blueprint, redirect, url_for, render_template, abort, send_file, after_this_request, request
from functools import wraps
from app.models import *
from config import login_manager
from flask_login import login_user, current_user, logout_user, AnonymousUserMixin
from PIL import Image, ImageEnhance
from random import randint
from app.flash_lib.flash_file import Flash_File
from app.flash_lib.flash_product import Flash_Product
from app.flash_lib.flash_news import Flash_News
from app.flash_lib.flash_user import Flash_User
from app.flash_lib.flash_setting import Flash_Setting
from app.flash_lib.flash_feedback import Flash_Feedback
from urllib.parse import urlencode, unquote, quote
import datetime
from dateutil import tz
from dateutil.relativedelta import relativedelta
import csv
import itertools
import zipfile
import os
import shutil
import pickle


admin_users = Blueprint('admin_users', __name__, template_folder='../../templates/admin')

def allowed_watermark(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ['png']

def get_options(column_type):
    options_db = Option.query.filter_by(column_type=column_type).all()
    options = []
    for option_db in options_db:
        options.append(option_db.column_option)
    return options
    

def get_max_entity_id(entity):
    max_enity_id = 0
    try:
        max_enity_id = db.session.query(db.func.max(entity.id)).scalar() + 1
    except TypeError:
        max_enity_id = 1
    return max_enity_id


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if current_user.is_anonymous:
                return redirect(url_for('main.login'))
            urole = current_user.get_urole()
            if ( (urole != role) and (role != "ANY")):
                return login_manager.unauthorized()      
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


def admin_access_required():
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            user_id = current_user.get_id()
            user = User.query.get(user_id)
            if user.role == 'ADMIN':
                user_admin_info = User_Admin_Info.query.get(user_id)
                if user_admin_info.rights == 'BLOCK':
                    return redirect(url_for('main.logout'))
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper

@admin_users.route("/admin/users/admins", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def users_admins():
    user_id = current_user.get_id()
    user = User_Admin_Info.query.get(user_id)
    if request.method == 'GET':
        users_admin_info_db = User_Admin_Info.query.all()
        users_admin_info = []
        for user_admin_info_db in users_admin_info_db:
            admin_dict = {
                'id' : user_admin_info_db.id,
                'admin_name' : user_admin_info_db.admin_name,
                'rights' : user_admin_info_db.rights,
                'quantity_products' : Admin_Upload.query.filter_by(user_id=user_admin_info_db.id).count()
            }
            users_admin_info.append(admin_dict)
        return render_template('users_admins.html', right=user.rights, users_admin_info=users_admin_info, page='users_admins')

    if request.method == 'POST':
        if user.rights != 'HIGH':
            return redirect(url_for('admin_users.users_admins'))
        ids = request.form.getlist('ids')
        for id in ids:
            user_admin_info = User_Admin_Info.query.get(id)
            user_admin_info.rights = request.form['rights'+str(id)]
            user_admin_info.admin_name = request.form['admin_name'+str(id)]
            db.session.commit()

        return redirect(url_for('admin_users.users_admins'))

@admin_users.route("/admin/users/admins/new_admin", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def users_admins_new_admin():
    if request.method == 'GET':
        return render_template('new_admin.html', page='new_admin')

    if request.method == 'POST':
        error_log = None

        user_name = request.form['user_name']
        password = request.form['password']
        password_2 = request.form['password2']
        admin_name = request.form['admin_name']
        rights = request.form['rights']

        if password != password_2:
            error_log = 'Пароли не совпадают'

        if User.query.filter_by(user_name=user_name).first():
            error_log = 'Пользователь с подобным логином уже существует'

        if " " in user_name:
            error_log = 'Не используйте пробел для создания логина'

        if error_log:
            return render_template('new_admin.html', page='new_admin', error_log=error_log)

        flash_user = Flash_User()
        flash_user.add_user_admin(user_name=user_name, password=password, admin_name=admin_name, admin_rights=rights)

        return redirect(url_for('admin_users.users_admins'))


@admin_users.route("/admin/users/stores")
@login_required(role='ADMIN')
@admin_access_required()
def users_stores():
    users_store_info = User_Store_Info.query.all()
    return render_template('users_stores.html', users_store_info=users_store_info, page='users_stores')


@admin_users.route("/admin/users/stores/change_password/<store_id>", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def users_stores_change_password(store_id):
    print('ok')
    user = User.query.get(store_id)
    if user is None:
        abort(404)

    if request.method == 'GET':
        return render_template('admin_store_change_password.html', page='users_stores')

    if request.method == 'POST':
        new_password = request.form['new_password']
        flash_user = Flash_User()
        resp = flash_user.change_password_store(store_id, '', new_password, True)
        return redirect(url_for('admin_users.users_stores'))


@admin_users.route("/admin/users/constraints")
@login_required(role='ADMIN')
@admin_access_required()
def users_constraints():

    if not 'store' in request.args or not 'provider' in request.args:
        return redirect("{}?store=all&provider=all".format(url_for('admin_users.users_constraints')))

    store_name = request.args.get('store')
    provider_name = request.args.get('provider')
    
    constraints_db_query = db.session.query(Store_Provider)


    if store_name != 'all':
        print(store_name)
        user_store_info = User_Store_Info.query.filter_by(store_name=store_name).first() 
        if user_store_info is None:
            abort(404)

        constraints_db_query = constraints_db_query.filter(Store_Provider.store_id == user_store_info.id)


    if provider_name != 'all':
        print(provider_name)
        provider = Provider.query.filter_by(provider_name=provider_name).first() 
        if provider is None:
            abort(404)

        constraints_db_query = constraints_db_query.filter(Store_Provider.provider_id == provider.id)    


    constraints_db = constraints_db_query.all()

    constraints = []
    for constraint_db in constraints_db:
        constraint_dict ={
            'id' : constraint_db.id,
            'store_name' : User_Store_Info.query.get(constraint_db.store_id).store_name,
            'provider_name' : Provider.query.get(constraint_db.provider_id).provider_name
        }
        constraints.append(constraint_dict)

    users_store_info = User_Store_Info.query.all()
    providers = Provider.query.all()
    return render_template('users_constraints.html', constraints=constraints, store_name=store_name, provider_name=provider_name,
        users_store_info=users_store_info, providers=providers, page='store_constraints')


@admin_users.route("/admin/users/constraints/new", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def users_constraints_new():
    if request.method == 'GET':
        users_store_info = User_Store_Info.query.all()
        providers = Provider.query.all()
        return render_template('users_constraints_add.html', users_store_info=users_store_info, providers=providers, page='new_store_constraints')

    if request.method == 'POST':
        store_id = request.form['store']
        provider_id = request.form['provider']

        if User_Store_Info.query.get(store_id) is None or Provider.query.get(provider_id) is None:
            abort(404)

        if Store_Provider.query.filter_by(store_id=store_id, provider_id=provider_id).first():
            error_log = 'Данное ограничение уже существует'
            return render_template('users_constraints_add.html', error_log=error_log, users_store_info=users_store_info, providers=providers, page='new_store_constraints')

        constraint = Store_Provider(store_id=store_id, provider_id=provider_id)
        db.session.add(constraint)
        db.session.commit()

        return redirect(url_for('admin_users.users_constraints'))


@admin_users.route("/admin/users/constraints/delete/<constraint_id>")
@login_required(role='ADMIN')
@admin_access_required()
def users_constraints_delete(constraint_id):
    store_provider = Store_Provider.query.get(constraint_id)
    if store_provider is None:
        abort(404)
    db.session.delete(store_provider)
    db.session.commit()  
    return redirect(url_for('admin_users.users_constraints'))


@admin_users.route("/admin/users/reports")
@login_required(role='ADMIN')
@admin_access_required()
def reports_prepare():
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')

    max_date_db = db.session.query(db.func.max(Admin_Upload.date)).first()[0]
    min_date_db = db.session.query(db.func.min(Admin_Upload.date)).first()[0]

    if not max_date_db or not min_date_db:
        max_date_db = datetime.datetime.utcnow()
        min_date_db = datetime.datetime.utcnow()

    max_date = max_date_db.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")
    min_date = min_date_db.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")

    return render_template('admin_users_reports.html', min_date=min_date, max_date=max_date, page='report_products')


@admin_users.route("/admin/users/reports/table")
@login_required(role='ADMIN')
@admin_access_required()
def reports_table():
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')

    from_date = request.args.get('from_date')
    to_date = request.args.get('to_date')

    from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d")
    to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d")

    layout = request.args.get('layout')
    
    if layout == 'without':
        from_date_filt = from_date.replace(tzinfo=to_zone).astimezone(from_zone)
        to_date_filt = to_date.replace(tzinfo=to_zone).date()
        header = ['Имя администратора', 'Итого']
        users_admin_info = User_Admin_Info.query.all()
        table_body = []
        for user_admin_info in users_admin_info:
            table_body.append([user_admin_info.admin_name, Admin_Upload.query.filter(Admin_Upload.user_id==user_admin_info.id, Admin_Upload.date >= from_date_filt, db.func.date(Admin_Upload.date) <= to_date_filt).count()])
    
    elif layout == 'day':
        users_admin_info = User_Admin_Info.query.all()
        header = ['Имя администратора']
        table_body = []

        days = []
        while from_date <= to_date:
            days.append(from_date)
            from_date += relativedelta(days=1)

        days.append(from_date)

        for i in range(0, len(days) - 1):
            header.append(days[i].strftime("%B %d"))

        for user_admin_info in users_admin_info:
            row_body = [user_admin_info.admin_name]
            for i in range(0, len(days) - 1):
                from_date_filt = days[i].replace(tzinfo=to_zone).astimezone(from_zone)
                to_date_filt = days[i+1].replace(tzinfo=to_zone).astimezone(from_zone)

                row_body.append(Admin_Upload.query.filter(Admin_Upload.user_id==user_admin_info.id, Admin_Upload.date >= from_date_filt, Admin_Upload.date <= to_date_filt).count())

            table_body.append(row_body)
    
    elif layout == 'month':
        users_admin_info = User_Admin_Info.query.all()
        header = ['Имя администратора']
        table_body = []

        months = []
        months.append(from_date)
        from_date += relativedelta(months=1)
        from_date = from_date.replace(day=1)
        while from_date <= to_date:
            months.append(from_date)
            from_date += relativedelta(months=1)

        months.append(to_date)

        for i in range(0, len(months) - 1):
            header.append(months[i].strftime("%B"))

        for user_admin_info in users_admin_info:
            row_body = [user_admin_info.admin_name]
            for i in range(0, len(months) - 1):
                from_date_filt = months[i].replace(tzinfo=to_zone).astimezone(from_zone)
                to_date_filt = months[i+1].replace(tzinfo=to_zone).date()

                row_body.append(Admin_Upload.query.filter(Admin_Upload.user_id==user_admin_info.id, Admin_Upload.date >= from_date_filt, db.func.date(Admin_Upload.date) <= to_date_filt).count())

            table_body.append(row_body)

    else:
        abort(404)

    length_total = len(table_body[0])

    total = ['Итого']
    for i in range(1, length_total):
        total.append(sum(row[i] for row in table_body))

    table_body.append(total)

    return render_template('admin_users_reports_table.html', header=header, table_body=table_body, page='report_products')




@admin_users.route("/admin/users/water_mark/<user_id>", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def add_water_mark(user_id):
    user = User.query.get(user_id)
    user_store_info = User_Store_Info.query.get(user_id)
    if not user_store_info:
        abort(404)
        
    if request.method == 'GET':
        return render_template('add_watermark.html', user=user, user_store_info=user_store_info, page='users_stores')
        
    if request.method == 'POST':

        user = User.query.get(user_id)

        file = request.files['file']

        if file and allowed_watermark(file.filename):
            filename = str(user.id) + '.png'
            file.save("/home/dev/flash/files/watermarks/" + filename)
            user_store_info.water_mark = True
            db.session.commit()
        else:
            error_log = 'Данное расширение файлов не поддерживается для водяного знака. Используйте .png'
            return render_template('add_watermark.html', user=user, user_store_info=user_store_info, error_log=error_log, page='users_stores')

        return redirect(url_for('admin_users.add_water_mark', user_id=user_id))


@admin_users.route("/admin/users/stores/grant_access/<user_id>")
@login_required(role='ADMIN')
@admin_access_required()
def grant_access(user_id):
    user_store_info = User_Store_Info.query.get(user_id)
    if user_store_info:
        user_store_info.access = True
        db.session.commit()
    else:
        abort(404)
    return redirect(url_for('admin_users.users_stores'))


@admin_users.route("/admin/users/stores/block_access/<user_id>")
@login_required(role='ADMIN')
@admin_access_required()
def block_access(user_id):
    user_store_info = User_Store_Info.query.get(user_id)
    if user_store_info:
        user_store_info.access = False
        db.session.commit()
    else:
        abort(404)
    return redirect(url_for('admin_users.users_stores'))


@admin_users.route("/admin/users/stores/grant_without_watermark/<user_id>")
@login_required(role='ADMIN')
@admin_access_required()
def grant_without_watermark(user_id):
    user_store_info = User_Store_Info.query.get(user_id)
    if user_store_info:
        user_store_info.without_watermark = True
        db.session.commit()
    else:
        abort(404)
    return redirect(url_for('admin_users.users_stores'))


@admin_users.route("/admin/users/stores/block_without_watermark/<user_id>")
@login_required(role='ADMIN')
@admin_access_required()
def block_without_watermark(user_id):
    user_store_info = User_Store_Info.query.get(user_id)
    if user_store_info:
        user_store_info.without_watermark = False
        db.session.commit()
    else:
        abort(404)

    if not user_store_info.water_mark:
        download_type = Download_Type.query.get(user_id)
        if download_type.download_type in [2, 3, 6, 7]:
            download_type.download_type = 1
            db.session.commit()
    return redirect(url_for('admin_users.users_stores'))
    