from flask import Blueprint, redirect, url_for, render_template, abort, send_file, after_this_request, request
from functools import wraps
from app.models import *
from config import login_manager
from flask_login import login_user, current_user, logout_user, AnonymousUserMixin
from PIL import Image, ImageEnhance
from random import randint
from app.flash_lib.flash_file import Flash_File
from app.flash_lib.flash_product import Flash_Product
from app.flash_lib.flash_news import Flash_News
from app.flash_lib.flash_user import Flash_User
from app.flash_lib.flash_setting import Flash_Setting
from app.flash_lib.flash_feedback import Flash_Feedback
from urllib.parse import urlencode, unquote, quote
import datetime
from dateutil import tz
from dateutil.relativedelta import relativedelta
import csv
import itertools
import zipfile
import os
import shutil
import pickle
import hashlib


admin_provider = Blueprint('admin_provider', __name__, template_folder='../../templates/admin')

def get_options(column_type):
    options_db = Option.query.filter_by(column_type=column_type).all()
    options = []
    for option_db in options_db:
        options.append(option_db.column_option)
    return options
    

def get_max_entity_id(entity):
    max_enity_id = 0
    try:
        max_enity_id = db.session.query(db.func.max(entity.id)).scalar() + 1
    except TypeError:
        max_enity_id = 1
    return max_enity_id


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if current_user.is_anonymous:
                return redirect(url_for('main.login'))
            urole = current_user.get_urole()
            if ( (urole != role) and (role != "ANY")):
                return login_manager.unauthorized()      
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


def admin_access_required():
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            user_id = current_user.get_id()
            user = User.query.get(user_id)
            if user.role == 'ADMIN':
                user_admin_info = User_Admin_Info.query.get(user_id)
                if user_admin_info.rights == 'BLOCK':
                    return redirect(url_for('main.logout'))
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


@admin_provider.route("/admin/users/providers")
@login_required(role='ADMIN')
@admin_access_required()
def users_providers():
    providers = Provider.query.order_by(Provider.provider_name).all()
    return render_template('users_providers.html', providers=providers, page='users_providers')


@admin_provider.route("/admin/users/providers/change", methods=['POST'])
@login_required(role='ADMIN')
@admin_access_required()
def providers_change_address():
    user_id = current_user.get_id()

    action = request.form['action']

    ids = None

    if 'ids' in request.form:
        ids = request.form.getlist('ids')
    else:
        return redirect(url_for('admin_provider.users_providers'))
    if not ids:
        return redirect(url_for('admin_provider.users_providers'))

    if action == 'address':
        for id in ids:
            provider = Provider.query.get(id)
            if provider is None:
                abort(404)

            address = request.form['address'+str(provider.id)]
            if provider.address != address:
                provider.address = address

                dttime = datetime.datetime.utcnow()
                new = News(id=get_max_entity_id(News), provider_id=provider.id, admin_id=current_user.get_id(), news_type=7, visible=True, date=dttime, category=0)
                db.session.add(new)
                db.session.commit()

            doll_to_uah = request.form['doll_to_uah'+str(provider.id)]
            euro_to_uah = request.form['euro_to_uah'+str(provider.id)]
            try:
                doll_to_uah = float(doll_to_uah.replace(',', '.'))
                euro_to_uah = float(euro_to_uah.replace(',', '.'))
            except ValueError:
                continue
            if provider.doll_to_uah != doll_to_uah or provider.euro_to_uah != euro_to_uah:
                provider.doll_to_uah = doll_to_uah
                provider.euro_to_uah = euro_to_uah

                dttime = datetime.datetime.utcnow()
                new = News(id=get_max_entity_id(News), provider_id=provider.id, admin_id=current_user.get_id(), news_type=6, visible=True, date=dttime, category=0)
                db.session.add(new)
                db.session.commit()

            provider.provider_phone = request.form['phone'+str(provider.id)]
            provider.provider_viber = request.form['viber'+str(provider.id)]
            provider.provider_mail = request.form['email'+str(provider.id)]
            provider.provider_name = request.form['provider_name'+str(provider.id)]
            if 'market_rate' + str(id) in request.form:
                provider.market_rate = True
            else:
                provider.market_rate = False
            db.session.commit()

        return redirect(url_for('admin_provider.users_providers'))

    else:
        abort(404)


@admin_provider.route("/admin/users/providers/new", methods = ['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def users_providers_new():
    if request.method == 'GET':
        return render_template('new_provider.html', page='new_provider')

    if request.method == 'POST':
        error_log = None

        provider_name = request.form['provider_name']
        provider_address = request.form['provider_address']

        try:
            doll_to_uah = float(request.form['doll_to_uah'].replace(',', '.'))
            euro_to_uah = float(request.form['euro_to_uah'].replace(',', '.'))
        except ValueError:
            error_log = 'Неверный формат курса. Уточните Ваши данные.'

        phone = request.form['phone']
        viber = request.form['viber']
        email = request.form['email']

        provider = Provider.query.filter(db.func.lower(Provider.provider_name)==provider_name.lower()).first()
        if provider:
            error_log = 'Поставщик с таким названием уже существует'

        if error_log:
            return render_template('new_provider.html', error_log=error_log, page='new_provider')

        max_provider_id = get_max_entity_id(Provider)
        provider = Provider(id=max_provider_id, provider_name=provider_name, address=provider_address, market_rate=False,
            doll_to_uah=doll_to_uah, euro_to_uah=euro_to_uah, provider_phone = phone, provider_viber = viber, provider_mail = email)
        db.session.add(provider)
        db.session.commit()

        users_store_info = User_Store_Info.query.all()
        for user_store_info in users_store_info:
            store_provider_name = Store_Provider_Name(store_id=user_store_info.id, provider_id = max_provider_id, provider_name=provider_name)
            db.session.add(store_provider_name)
            db.session.commit()
        return redirect(url_for('admin_provider.users_providers'))


@admin_provider.route("/admin/users/providers/settings", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def users_providers_settings():
    providers_settings_list = ['disp_provs_adrs', 'disp_provs_doll', 
    'disp_provs_euro', 'disp_provs_phon', 'disp_provs_vibr', 'disp_provs_mail']

    providers_settings = dict()
    for provider_setting in providers_settings_list:
        providers_settings.update({provider_setting:Service_Setting.query.filter_by(setting_type=provider_setting).first()})

    if request.method == 'GET':
        return render_template('users_providers_settings.html', providers_settings=providers_settings, page='setting_providers')
    
    if request.method == 'POST':
        for provider_setting in providers_settings_list:
            setting = providers_settings.get(provider_setting)
            if provider_setting in request.form:
                setting.setting_value = 'True'
            else:
                setting.setting_value = 'False'

        db.session.commit()
        return redirect(url_for('admin_provider.users_providers_settings'))


@admin_provider.route("/admin/users/provider-users")
@login_required(role='ADMIN')
@admin_access_required()
def users_providers_users():
    providers_users = db.session.query(User, User_Provider_Info, Provider).filter(User_Provider_Info.provider_id == Provider.id, User.id == User_Provider_Info.id).all()
    providers = []
    for us, user, provider in providers_users:
        up_diction = {
            'id' : user.id,
            'login' : us.user_name,
            'provider_name' : provider.provider_name,
            'provider_address' : provider.address,
            'access_photo' : user.access_photo,
            'locale' : user.provider_locale
        }
        providers.append(up_diction)

    return render_template('users_provider_users.html', providers=providers, page='provider_users')


@admin_provider.route("/admin/users/provider-users/change", methods=['POST'])
@login_required(role='ADMIN')
@admin_access_required()
def users_providers_users_change():
    if 'ids' in request.form:
        ids = request.form.getlist('ids')
    else:
        return redirect(url_for('admin_provider.users_providers_users'))
    if not ids:
        return redirect(url_for('admin_provider.users_providers_users'))

    for id in ids:
        user_provider_info = User_Provider_Info.query.get(id)

        if 'access_photo'+str(user_provider_info.id) in request.form:
            user_provider_info.access_photo = True
        else:
            user_provider_info.access_photo = False

        user_provider_info.provider_locale = request.form['locale'+str(user_provider_info.id)]
        db.session.commit()


    return redirect(url_for('admin_provider.users_providers_users'))



@admin_provider.route("/admin/users/provider-users/register", methods = ['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def users_providers_register():
    providers = Provider.query.all()
    if request.method == 'GET':
        return render_template('admin_provider_register.html', providers=providers, page='provider_users_register')

    if request.method == 'POST':
        user_name = request.form['user_name']
        password = request.form['password']
        password_confirm = request.form['confirm_password']

        provider_id = int(request.form['provider_id'])
        access_photo = False 
        if request.form['access_photo'] == 'True':
            access_photo = True

        locale = request.form['locale']

        flash_user = Flash_User()

        error_log = None

        if password != password_confirm:
            error_log = 'Пароли не совпадают'

        if flash_user.check_existing_user(user_name):
            error_log = 'Пользователь с подобным логином уже существует'

        user_provider_info = User_Provider_Info.query.filter_by(provider_id=provider_id).first()
        if user_provider_info:
            error_log = 'Данный поставщик уже зарегистрирован'

        if " " in user_name:
            error_log = 'Не используйте пробел для создания логина'

        if error_log:
            return render_template('admin_provider_register.html', error_log=error_log, providers=providers, page='provider_users_register')

        flash_user.add_user_provider(user_name=user_name, password=password, provider_id=provider_id, access_photo=access_photo, locale=locale)

        return redirect(url_for('admin_provider.users_providers_users'))

 

@admin_provider.route("/admin/users/provider-users/delete/<provider_id>", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def users_providers_users_delete(provider_id):
    if request.method == 'GET' or request.method == 'POST':
        quantity_users = User_Provider_Info.query.filter_by(provider_id=provider_id).count()
        quantity_constraints = Store_Provider.query.filter_by(provider_id=provider_id).count()
        quantity_products = Product.query.filter_by(provider_id=provider_id).count()
        quantity_news = News.query.filter_by(provider_id=provider_id).count()
        quantity_names = Store_Provider_Name.query.filter_by(provider_id=provider_id).count()
        quantity_filters = Price_Provider.query.filter_by(provider_id=provider_id).count()

        return render_template('admin_users_provider_delete.html', quantity_users=quantity_users, quantity_filters=quantity_filters, quantity_names=quantity_names,
            quantity_news=quantity_news, quantity_products=quantity_products, quantity_constraints=quantity_constraints, page='users_providers')


    # if request.method == 'POST':
    #     flash_product = Flash_Product(current_user.get_id())

    #     user_provider_info = User_Provider_Info.query.filter_by(provider_id=provider_id).first()

    #     user = User.query.get(user_provider_info.id)

    #     displayed_fields = Displayed_Fields.query.filter_by(user_id=user.id).all()
    #     for displayed_field in displayed_fields:
    #         db.session.delete(displayed_field)
    #         db.session.commit()

    #     preparatory_shoes = Prepartory_shoes.query.filter_by(provider_id=user_provider_info.id).all()
    #     for preparatory_shoe in preparatory_shoes:
    #         db.session.delete(preparatory_shoe)
    #         db.session.commit()



    #     db.session.delete(user_provider_info)


@admin_provider.route("/admin/users/provider-users/drop_password/<user_id>")
@login_required(role='ADMIN')
@admin_access_required()
def users_providers_drop_password(user_id):
    user = User.query.get(user_id)
    hash = hashlib.sha256('provider'.encode('utf-8')).hexdigest()
    user.password_hash = hash
    db.session.commit()
    return redirect(url_for('admin_provider.users_providers_users'))





