from flask import Blueprint, redirect, url_for, render_template, abort, send_file, after_this_request, request
from functools import wraps
from app.models import *
from config import login_manager
from flask_login import login_user, current_user, logout_user, AnonymousUserMixin
from PIL import Image, ImageEnhance
from random import randint
from app.flash_lib.flash_file import Flash_File
from app.flash_lib.flash_product import Flash_Product
from app.flash_lib.flash_news import Flash_News
from app.flash_lib.flash_user import Flash_User
from app.flash_lib.flash_setting import Flash_Setting
from app.flash_lib.flash_feedback import Flash_Feedback
from urllib.parse import urlencode, unquote, quote
import datetime
from dateutil import tz
import csv
import itertools
import zipfile
import os
import shutil
import pickle


admin_feedback = Blueprint('admin_feedback', __name__, template_folder='../../templates/admin')

def get_options(column_type):
    options_db = Option.query.filter_by(column_type=column_type).all()
    options = []
    for option_db in options_db:
        options.append(option_db.column_option)
    return options
    

def get_max_entity_id(entity):
    max_enity_id = 0
    try:
        max_enity_id = db.session.query(db.func.max(entity.id)).scalar() + 1
    except TypeError:
        max_enity_id = 1
    return max_enity_id


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if current_user.is_anonymous:
                return redirect(url_for('main.login'))
            urole = current_user.get_urole()
            if ( (urole != role) and (role != "ANY")):
                return login_manager.unauthorized()      
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper

def admin_access_required():
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            user_id = current_user.get_id()
            user = User.query.get(user_id)
            if user.role == 'ADMIN':
                user_admin_info = User_Admin_Info.query.get(user_id)
                if user_admin_info.rights == 'BLOCK':
                    return redirect(url_for('main.logout'))
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper
    

@admin_feedback.route("/admin/feedback")
@login_required(role='ADMIN')
@admin_access_required()
def admin_feedback_all():
    if not 'page' in request.args:
        return redirect(url_for('admin_feedback.admin_feedback_all') + '?page=1')

    try:
        page = int(request.args.get('page'))
    except ValueError:
        abort(404)
    flash_feedback = Flash_Feedback()
    feedbacks, next, prev, pages_li = flash_feedback.get_all_feedbacks(page)
    return render_template('admin_feedback_all.html', feedbacks=feedbacks, next=next, prev=prev, page_pag=page, pages_li=pages_li, page='feedback_messages')


@admin_feedback.route("/admin/feedback/<feedback_id>")
@login_required(role='ADMIN')
@admin_access_required()
def admin_feedback_detail(feedback_id):
    feedback = Feed_Back.query.get(feedback_id)
    if feedback is None:
        abort(404)
    return render_template('admin_feedback_detail.html', feedback=feedback, page='feedback_messages')


@admin_feedback.route("/admin/feedback/settings", methods=['GET', 'POST'])
@login_required(role='ADMIN')
@admin_access_required()
def admin_feedback_setting_email():
    email_setting = Service_Setting.query.filter_by(setting_type='feedback_email').first()

    if request.method == 'POST':
        email_setting.setting_value = request.form['feedback_email']
        db.session.commit()

    return render_template('admin_feedback_setting_email.html', email_setting=email_setting, page='feedback_email')