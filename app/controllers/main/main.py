from flask import Blueprint, redirect, url_for, render_template, send_file, request, current_app, abort, after_this_request
from app.models import *
from config import login_manager
from flask_login import login_user, current_user, logout_user, AnonymousUserMixin
from functools import wraps
import datetime
import math
from dateutil import tz
from app.flash_lib.flash_product import Flash_Product
from app.flash_lib.flash_user import Flash_User
from app.flash_lib.flash_news import Flash_News
from app.flash_lib.flash_setting import Flash_Setting
from app.flash_lib.flash_feedback import Flash_Feedback
from app.flash_lib.flash_file import Flash_File
import os
import shutil
from dateutil.relativedelta import relativedelta


main = Blueprint('main', __name__, template_folder='../../templates/main')


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def access_required():
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            user_id = current_user.get_id()
            store = User_Store_Info.query.get(user_id)
            if store.access == False:
                return redirect(url_for('main.block'))
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper



def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if current_user.is_anonymous:
                return redirect(url_for('main.login'))
            urole = current_user.get_urole()
            if ( (urole != role) and (role != "ANY")):
                return login_manager.unauthorized()      
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


@main.route('/logout')
@login_required(role='ANY')
def logout():
    logout_user()
    return redirect(url_for('main.login'))


@main.route("/block")
@login_required(role='STORE')
def block():
    user_id = current_user.get_id()
    user = User.query.get(user_id)
    store = User_Store_Info.query.get(user_id)
    if store.access == True:
        return redirect(url_for('store.store_news'))
    return render_template('block.html', user=user)


@main.route('/feedback', methods=['GET', 'POST'])
def feedback():
    if request.method == 'GET':
        return render_template('feedback.html')

    if request.method == 'POST':
        flash_feedback = Flash_Feedback()
        flash_feedback.new_feedback(name=request.form['name'], phone=request.form['phone'], email=request.form['email'], message=request.form['message'])
        return render_template('after_feedback.html')


@main.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        if not current_user.is_anonymous:
            if current_user.get_urole() == 'ADMIN':
                return redirect(url_for('admin_products.products'))
            elif current_user.get_urole() == 'STORE':
                return redirect(url_for('main.main_poviders'))
            elif current_user.get_urole() == 'PROVIDER':
                return redirect(url_for('provider.provider_products'))
        else:
            return render_template('login.html')


        return render_template('login.html')

    if request.method == 'POST':
        user_name = request.form['user_name']
        password = request.form['password']
        remember_me = False

        flash_user = Flash_User()


        if 'remember_me' in request.form:
            remember_me = True

        registered_user = flash_user.get_user_by_credentials(user_name, password)

        if registered_user is None:
            error_log = "Неверный логин или пароль, попробуйте снова"
            return render_template('login.html', error_log=error_log)

        login_user(registered_user, remember = remember_me)

        if registered_user.get_urole() == 'ADMIN':
            return redirect(url_for('admin_products.products'))
        elif registered_user.get_urole() == 'STORE':
            return redirect(url_for('main.main_poviders'))
        elif registered_user.get_urole() == 'PROVIDER':
            return redirect(url_for('provider.provider_products'))  


@main.route("/register", methods=['GET', 'POST'])
def register():
    
    if request.method == 'GET':
        return render_template('register.html')

    if request.method == 'POST':
        error_log = None

        user_name = request.form['user_name']
        password = request.form['password']
        password_2 = request.form['password2']  
        phone_number = request.form['phone_number']
        store_name = request.form['store_name']
        store_address = request.form['store_address']
        e_mail = request.form['e_mail']
        director_name = request.form['director_name']
        director_phone = request.form['director_phone']

        if password != password_2:
            error_log = 'Пароли не совпадают'

        if User.query.filter_by(user_name=user_name).first():
            error_log = 'Пользователь с подобным логином уже существует'

        if " " in user_name:
            error_log = 'Не используйте пробел для создания логина'

        if error_log:
            return render_template('register.html', error_log=error_log) 

        flash_user = Flash_User()
        flash_user.add_user_store(user_name=user_name, password=password, phone_number=phone_number, store_name=store_name, e_mail=e_mail, 
            director_name=director_name, director_phone=director_phone, store_address=store_address)

        return render_template('register_message.html')


@main.route('/')
def main_poviders():
    user = current_user if not current_user.is_anonymous else None

    # users_provider_info = User_Provider_Info.query.all()
    # for user_provider_info in users_provider_info:
    #     user_provider_info.access_photo = True
    #     user_provider_info.provider_locale = 'Europe/Kiev'
    #     db.session.commit()

    if user is None:
        return redirect(url_for('main.login'))

    if user.role == 'PROVIDER':
        return redirect(url_for('provider.provider_products'))

    if user and user.role == 'STORE' and not User_Store_Info.query.get(current_user.get_id()).access:
        return redirect(url_for('main.block'))

    search = False
    if 'search' in request.args:
        search = request.args.get('search')
        providers = Provider.query.filter(db.func.lower(Provider.provider_name).contains(search.lower())).all()
    elif 'type' in request.args and request.args.get('type') == 'New':
        providers = Provider.query.order_by(Provider.id.desc()).limit(8).all()
    else:
        providers = Provider.query.order_by(Provider.provider_name).all()

    providers_list = []
    provider_row = []
    for i in range(1, len(providers) + 1):
        provider_row.append(providers[i-1])
        if (i % 8 == 0 and i != 0) or i == len(providers):
            providers_list.append(provider_row)
            provider_row = []

    return render_template('providers.html', user=user, search=search, providers=providers_list, page='providers')


@main.route('/providers/<provider_name>')
def provider_detail(provider_name):
    user = current_user if not current_user.is_anonymous else None
    if user is None:
        return redirect(url_for('main.login'))

    if user and user.role == 'STORE' and not User_Store_Info.query.get(current_user.get_id()).access:
        return redirect(url_for('main.block'))

    if user.role == 'PROVIDER':
        return redirect(url_for('provider.provider_products'))

    provider = Provider.query.filter(db.func.lower(Provider.provider_name)==provider_name.lower()).first()
    if provider is None:
        abort(404)

    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')

    user_id = current_user.get_id()

    flash_news = Flash_News(user_id)
    dates = flash_news.get_providers_dates_for_provider(provider.id)

    dates_new = []
    for date in dates:
        if not date in dates_new:
            dates_new.append(date)

    dates_new = dates_new[:8]

    max_date_db = db.session.query(db.func.max(Product.date)).filter(Product.provider_id == provider.id).first()[0]
    min_date_db = db.session.query(db.func.min(Product.date)).filter(Product.provider_id == provider.id).first()[0]

    if dates:
        date=dates[0]

    if not max_date_db or not min_date_db:
        date = datetime.datetime.utcnow().replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")
        date = datetime.datetime.utcnow().replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")
        max_date_db = datetime.datetime.utcnow()
        min_date_db = datetime.datetime.utcnow()


    max_date = max_date_db.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")
    min_date = min_date_db.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")


    if not 'from_date' in request.args or not 'to_date' in request.args or not 'page' in request.args:
        return redirect('/providers/' + provider_name + '?from_date=' + date + '&to_date=' + date + '&page=1')


    try:
        from_date = datetime.datetime.strptime(request.args.get('from_date'), "%Y-%m-%d").replace(tzinfo=to_zone)
        to_date = datetime.datetime.strptime(request.args.get('to_date'), "%Y-%m-%d").replace(tzinfo=to_zone)
    except ValueError:
        from_date = datetime.datetime.strptime(request.args.get('from_date'), "%d-%m-%Y").replace(tzinfo=to_zone)
        to_date = datetime.datetime.strptime(request.args.get('to_date'), "%d-%m-%Y").replace(tzinfo=to_zone)

    from_date_filt = from_date
    from_date_filt -= relativedelta(days=1)
    from_date_filt = from_date_filt.replace(hour=21)
    to_date_filt = to_date.date()


    from_date = from_date.date()
    to_date = to_date.date()
    
    page = request.args.get('page')

    try:
        page = int(page)
    except ValueError:
        abort(404)

    if 'articul' in request.args:
        articul = request.args.get('articul')
    else:
        articul = ""

    flash_setting = Flash_Setting()
    flash_product = Flash_Product(user.id)
    

    providers_settings = flash_setting.get_providers_settings()
    displayed_fields = flash_product.get_displayed_fields()

    dates = flash_news.get_products_upload_dates(provider.id)

    dates_new = []
    for date in dates:
        date_diction = {
            'date' : date.strftime("%d-%m-%Y"),
            'range' : date.date() >= from_date and date.date() <= to_date
        }
        if not date_diction in dates_new:
            dates_new.append(date_diction)

    dates_new = dates_new[:6]
    

    if user.role == "STORE" and (Store_Provider.query.filter_by(store_id=user.id, provider_id=provider.id).first() or not User_Store_Info.query.get(user.id).access):
        abort(404)
    else:
        products, pages_li, next, prev = flash_product.get_products_by_provider(provider.id, page, 50, from_date_filt, to_date_filt, articul)

    return render_template('provider_detail.html', products=products, displayed_fields=displayed_fields, min_date=min_date, max_date=max_date, to_date=to_date, 
        from_date=from_date, providers_settings=providers_settings, user=user, dates=dates_new, provider=provider, page='providers', next=next, articul=articul,
        prev=prev, pages_li=pages_li, page_pag=page)


@main.route("/main/providers/download", methods=['POST'])
@login_required(role='STORE')
@access_required()
def main_providers_donwload():
    user_id = current_user.get_id()

    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')

    action = request.form['action']
    prov_id = request.form['provider']
    provider = Provider.query.get(prov_id)
    if action == 'selected':
        ids = None
        if 'ids' in request.form:
            ids = request.form.getlist('ids')
        else:
            return redirect('/providers/' + provider.provider_name)
        if not ids:
            return redirect('/providers/' + provider.provider_name)    


        temp_ids = []
        for id in ids:
            temp_ids.append(int(id))

        ids = temp_ids
    

        flash_file = Flash_File(user_id)
        rand_number = flash_file.get_products_archive(ids)
        directory = "/home/dev/flash/files/temporary/" + str(rand_number)
        shutil.make_archive("/home/dev/flash/files/temporary/" + str(rand_number), 'zip', directory)
        @after_this_request
        def remove_file(response):
            os.remove("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip")
            shutil.rmtree(directory)
            return response
        return send_file("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip", attachment_filename='products.zip', as_attachment=True)
    elif action == 'all':
        from_date = request.form['from_date']
        to_date = request.form['to_date']
        try:
            from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d").replace(tzinfo=to_zone)
            to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d").replace(tzinfo=to_zone)
        except ValueError:
            abort(404)

        from_date -= relativedelta(days=1)
        from_date = from_date.replace(hour=21)
        to_date = to_date.date()

        ids = db.session.query(Product.id).filter(Product.provider_id == provider.id, db.func.lower(Product.presence) == 'есть', Product.date >= from_date, db.func.date(Product.date) <= to_date).all()
        if not ids:
            return redirect('/providers/' + provider.provider_name)

        temp_ids = []
        for id in ids:
            temp_ids.append(id[0])

        ids = temp_ids

        flash_file = Flash_File(user_id)
        rand_number = flash_file.get_products_archive(ids)
        directory = "/home/dev/flash/files/temporary/" + str(rand_number)
        shutil.make_archive("/home/dev/flash/files/temporary/" + str(rand_number), 'zip', directory)
        @after_this_request
        def remove_file(response):
            os.remove("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip")
            shutil.rmtree(directory)
            return response
        return send_file("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip", attachment_filename='products.zip', as_attachment=True)

    else:
        abort(404)



@main.route('/main_news')
def main_news():
    providers = Provider.query.order_by(Provider.provider_name).all()
    user = current_user if not current_user.is_anonymous else None

    if user is None:
        return redirect(url_for('main.login'))

    if user and user.role == 'STORE' and not User_Store_Info.query.get(current_user.get_id()).access:
        return redirect(url_for('main.block'))

    if user.role == 'PROVIDER':
        return redirect(url_for('provider.provider_products'))

    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Europe/Kiev')

    user_id = current_user.get_id()

    max_date_db = db.session.query(db.func.max(News.date)).first()[0]
    min_date_db = db.session.query(db.func.min(News.date)).first()[0]

    if not max_date_db or not min_date_db:
        max_date_db = datetime.datetime.utcnow()
        min_date_db = datetime.datetime.utcnow()

    max_date = max_date_db.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")
    min_date = min_date_db.replace(tzinfo=from_zone).astimezone(to_zone).strftime("%Y-%m-%d")


    if not 'provider' in request.args or not 'type' in request.args or not 'from_date' in request.args or not 'to_date' in request.args or not 'page' in request.args:
        return redirect('/main_news?type=all&provider=all&from_date=' + min_date + '&to_date=' + max_date + '&page=1')

    from_date = datetime.datetime.strptime(request.args.get('from_date'), "%Y-%m-%d").replace(tzinfo=to_zone).date()
    to_date = datetime.datetime.strptime(request.args.get('to_date'), "%Y-%m-%d").replace(tzinfo=to_zone).date()

    provider = request.args.get('provider')
    type_news = request.args.get('type')
    page = request.args.get('page')

    flash_news = Flash_News(user.id)
    response = flash_news.get_visible_news(type_news, provider, from_date, to_date, page)

    if response == False:
        abort(404)

    news, pages_li, next, prev = response

    if provider != 'all':
        provider = int(provider)

    return render_template('main_news.html', user=user, news=news, page_pag=int(page), pages_li=pages_li, page='main_news', 
        type=type_news, from_date=request.args.get('from_date'), to_date=request.args.get('to_date'), min_date=min_date, 
        max_date=max_date, prev=prev, next=next, provider=provider, providers=providers)


@main.route('/main_news/detail/<news_id>')
def main_news_detail(news_id):
    user_id = current_user.get_id()
    user = current_user if not current_user.is_anonymous else None

    if user is None:
        return redirect(url_for('main.login'))

    if user and user.role == 'STORE' and not User_Store_Info.query.get(current_user.get_id()).access:
        return redirect(url_for('main.block'))

    if user.role == 'PROVIDER':
        return redirect(url_for('provider.provider_products'))

    new = News.query.get(news_id)
    if new is None:
        abort(404)

    if new.news_type != 8:

        if Store_Provider.query.filter_by(store_id=user_id, provider_id=new.provider_id).first():
            abort(404)

        flash_product = Flash_Product(user_id)
        goods = flash_product.get_products_by_news(news_id)
         
        displayed_fields_db = Displayed_Fields.query.filter_by(user_id=user_id).all()
        displayed_fields = {}

        for field in displayed_fields_db:
            displayed_fields.update({field.field_name : field.flag})

        if new.news_type == 5:
            for good in goods:
                if good.get('brand_name') != good.get('old_brand_name'):
                    displayed_fields.update({'brand':True})
                if good.get('group') != good.get('old_group'):
                    displayed_fields.update({'group':True})
                if good.get('type') != good.get('old_type'):
                    displayed_fields.update({'type':True})
                if good.get('repeat') != good.get('old_repeat'):
                    displayed_fields.update({'repeat':True})
                if good.get('sex') != good.get('old_sex'):
                    displayed_fields.update({'sex':True})
                if good.get('min_order') != good.get('old_min_order'):
                    displayed_fields.update({'min_order':True})
                if good.get('material_outside') != good.get('old_material_outside'):
                    displayed_fields.update({'material_outside':True})
                if good.get('material_inside') != good.get('old_material_inside'):
                    displayed_fields.update({'material_inside':True})
                if good.get('material_insole') != good.get('old_material_insole'):
                    displayed_fields.update({'material_insole':True})
                if good.get('colour') != good.get('old_colour'):
                    displayed_fields.update({'colour':True})
                if good.get('size') != good.get('old_size'):
                    displayed_fields.update({'size':True})
                if good.get('season') != good.get('old_season'):
                    displayed_fields.update({'season':True})
                if good.get('box') != good.get('old_box'):
                    displayed_fields.update({'box':True})
                if good.get('manufacturer') != good.get('old_manufacturer'):
                    displayed_fields.update({'manufacturer':True})
        
        return render_template('news_detail.html', displayed_fields=displayed_fields, user=user, products=goods, page='main_news', news_type=new.news_type)

    else:
        admin_message = new.message
        return render_template('news_message.html', admin_message=admin_message, news_type=new.news_type, user=user, page='main_news')


@main.route('/contacts')
def main_contacts():
    user = current_user if not current_user.is_anonymous else None
    if user is None:
        return redirect(url_for('main.login'))

    if user and user.role == 'STORE' and not User_Store_Info.query.get(current_user.get_id()).access:
        return redirect(url_for('main.block'))

    if user.role == 'PROVIDER':
        return redirect(url_for('provider.provider_products'))

    return render_template('contacts.html', user=user, page='contacts')



