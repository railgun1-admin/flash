from .provider.provider import provider

from .store.store import store

from .main.main import main

from .admin.admin_products import admin_products
from .admin.admin_price import admin_price
from .admin.admin_users import admin_users
from .admin.admin_news import admin_news
from .admin.admin_feedback import admin_feedback
from .admin.admin_settings import admin_settings
from .admin.admin_provider import admin_provider