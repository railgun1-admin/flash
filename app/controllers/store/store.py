from flask import Blueprint, redirect, url_for, render_template, abort, send_file, after_this_request, request
from flask_login import login_user, current_user, logout_user, AnonymousUserMixin
from app.models import *
from config import login_manager
from app.flash_lib.flash_file import Flash_File
from app.flash_lib.flash_product import Flash_Product
from app.flash_lib.flash_news import Flash_News
from app.flash_lib.flash_user import Flash_User
from functools import wraps
from random import randint
from sqlalchemy import and_
from PIL import Image, ImageEnhance
import datetime
from dateutil import tz
import zipfile
import csv
import os
import shutil
import xlsxwriter

store = Blueprint('store', __name__, template_folder='../../templates/store')


def get_fields_dict():
    diction = {
        'id' : 'id',
        'name' : 'Наименование',
        'articul' : 'Артикул',
        'brand_name' : 'Бренд',
        'provider_name' : 'Поставщик',
        'cost' : 'Цена',
        'cost_site' : 'Цена сайт',
        'cost_uah' : 'Цена грн',
        'cost_site_uah' : 'Цена сайт грн',
        'cost_doll' : 'Цена $',
        'cost_site_doll' : 'Цена сайт $',
        'cost_euro' : 'Цена евро',
        'cost_site_euro' : 'Цена сайт евро',
        'currency' : 'Валюта',
        'sex' : 'Пол',
        'group' : 'Группа',
        'type' : 'Тип',
        'material_outside' : 'Материал верха',
        'material_inside' : 'Материал внутри',
        'material_insole' : 'Материал стельки',
        'empty' : 'Пустая',
        'size' : 'Размер',
        'repeat' : 'Повторы',
        'season' : 'Сезон',
        'colour' : 'Цвет',
        'box' : 'Ящик',
        'min_order' : 'Минимальный заказ',
        'manufacturer' : 'Производитель',
        'descreption' : 'Описание',
        'photo_1' : 'Фото1',
        'photo_2' : 'Фото2',
        'photo_3' : 'Фото3',
        'presence' : 'Наличие'
    }
    return diction


def get_options(column_type):
    options_db = Option.query.filter_by(column_type=column_type).all()
    options = []
    for option_db in options_db:
        options.append(option_db.column_option)
    return options

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if current_user.is_anonymous:
                return redirect(url_for('main.login'))
            urole = current_user.get_urole()
            if ( (urole != role) and (role != "ANY")):
                return login_manager.unauthorized()
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


def access_required():
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            user_id = current_user.get_id()
            store = User_Store_Info.query.get(user_id)
            if store.access == False:
                return redirect(url_for('main.block'))
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


@store.route("/store/news/download", methods=['POST'])
@login_required(role='STORE')
@access_required()
def store_news_download():
    user_id = current_user.get_id()

    ids_news = None
    if 'ids' in request.form:
        ids_news = request.form.getlist('ids')
    else:
        return redirect(url_for('main.main_news'))
    if not ids_news:
        return redirect(url_for('main.main_news'))
    
    flash_news = Flash_News(user_id)
    ids = flash_news.get_ids_from_news(ids_news)

    for id in ids_news:
        if Seen_News.query.filter_by(store_id=user_id, news_id=id).first() is None:
            seen_news = Seen_News(store_id=user_id, news_id=id)
            db.session.add(seen_news)
            db.session.commit()
    
    flash_file = Flash_File(user_id)
    rand_number = flash_file.get_products_archive(ids)
    directory = "/home/dev/flash/files/temporary/" + str(rand_number)
    shutil.make_archive("/home/dev/flash/files/temporary/" + str(rand_number), 'zip', directory)
    @after_this_request
    def remove_file(response):
        os.remove("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip")
        shutil.rmtree(directory)
        return response
    return send_file("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip", attachment_filename='products.zip', as_attachment=True)


@store.route("/store/products/download", methods=['POST'])
@login_required(role='STORE')
@access_required()
def store_products_download():
    user_id = current_user.get_id()
    ids = None
    if 'ids' in request.form:
        ids = request.form.getlist('ids')
    else:
        return redirect(url_for('main.main_news'))
    if not ids:
        return redirect(url_for('main.main_news'))

    temp_ids = []
    for id in ids:
        temp_ids.append(int(id))
    ids = temp_ids


    flash_file = Flash_File(user_id)
    rand_number = flash_file.get_products_archive(ids)
    directory = "/home/dev/flash/files/temporary/" + str(rand_number)
    shutil.make_archive("/home/dev/flash/files/temporary/" + str(rand_number), 'zip', directory)
    @after_this_request
    def remove_file(response):
        os.remove("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip")
        shutil.rmtree(directory)
        return response
    return send_file("/home/dev/flash/files/temporary/" + str(rand_number) + ".zip", attachment_filename='products.zip', as_attachment=True)



@store.route('/store/settings/change_password', methods=['GET', 'POST'])
@login_required(role='STORE')
@access_required()
def change_password():
    user_id = current_user.get_id()

    if request.method == 'GET':
        return render_template('change_password.html', page='setting_password')

    if request.method == 'POST':

        user = User.query.get(user_id)

        old_password = request.form['old_password']
        new_password = request.form['new_password']
        new_password_again = request.form['new_password_again']

        error_log = None

        if new_password != new_password_again:
            error_log = 'Пароли не совпадают'
            return render_template('change_password.html', error_log=error_log, page='setting_password')

        flash_user = Flash_User()
        resp = flash_user.change_password_store(user_id, old_password, new_password)

        if not resp:
            error_log = 'Старый пароль введён неверно.'
            
        if error_log:
            return render_template('change_password.html', error_log=error_log, page='setting_password')

        success_log = 'Пароль успешно обнволён'
        return render_template('change_password.html', success_log=success_log, page='setting_password')


@store.route('/store/settings/change_email', methods=['GET', 'POST'])
@login_required(role='STORE')
@access_required()
def change_email():
    user_id = current_user.get_id()
    store = User_Store_Info.query.get(user_id)

    if request.method == 'GET':
        return render_template('change_email.html', e_mail=store.e_mail, page='setting_mail')

    if request.method == 'POST':

        e_mail = request.form['email']

        store.e_mail = e_mail
        db.session.commit()

        success_log = 'Почта успешно обновлена'
        return render_template('change_email.html', e_mail=store.e_mail, success_log=success_log, page='setting_mail')



@store.route('/store/settings/change_phone', methods=['GET', 'POST'])
@login_required(role='STORE')
@access_required()
def change_phone():
    user_id = current_user.get_id()
    store = User_Store_Info.query.get(user_id)

    if request.method == 'GET':
        return render_template('change_phone.html', phone_number=store.phone_number, page='setting_number')

    if request.method == 'POST':

        phone_number = request.form['phone_number']

        store.phone_number = phone_number
        db.session.commit()

        success_log = 'Телефонный номер успешно обновлён'
        return render_template('change_phone.html', phone_number=store.phone_number, success_log=success_log, page='setting_number')


@store.route("/store/setting_displayed_fields", methods=['GET', 'POST'])
@login_required(role='STORE')
@access_required()
def setting_displayed_fields_store():
    user_id = current_user.get_id()

    if request.method == 'POST':
        displayed_fields_db = Displayed_Fields.query.filter_by(user_id=user_id).all()
        for displayed_field in displayed_fields_db:
            displayed_field.flag = True if displayed_field.field_name in request.form else False
        db.session.commit()


    displayed_fields_db = Displayed_Fields.query.filter_by(user_id=user_id).all()

    displayed_fields = {}
    for field in displayed_fields_db:
        displayed_fields.update({field.field_name : field.flag})

    return render_template('displayed_fields_store.html', page='setting_displayed_fields', displayed_fields=displayed_fields)


@store.route("/store/setting_fields/table", methods=['GET', 'POST'])
@login_required(role='STORE')
@access_required()
def setting_fields_table_store():
    user_id = current_user.get_id()

    diction = get_fields_dict()

    fields = Download_Field.query.filter_by(user_id=user_id).order_by(Download_Field.number_column).all()

    numbers = range(1, 52)


    if request.method == 'GET':
        return render_template('setting_fields_table_store.html', page='setting_fields', diction=diction, fields=fields, numbers=numbers)


    if request.method == 'POST':

        fields_quantity = Download_Field.query.filter_by(user_id=user_id).count()

        request_data = []

        for i in range(0, fields_quantity):
            request_data.append([int(request.form['number_column' + str(i)]), request.form['column_type' + str(i)], request.form['name' + str(i)]])

        for i in range(0, len(request_data)):
            for j in range(0, len(request_data)):
                if request_data[i][0] == request_data[j][0] and i != j :
                    error_log = 'Порядковый номер колонок должен быть уникальным'
                    return render_template('setting_fields_table_store.html', error_log=error_log, page='setting_fields', diction=diction, fields=fields, numbers=numbers)

        for i in range(0, len(fields)):
            fields[i].number_column = request_data[i][0]
            fields[i].column_type = request_data[i][1]
            fields[i].name_column = request_data[i][2]
            
        db.session.commit()

        return redirect(url_for('store.setting_fields_table_store'))


@store.route("/store/setting_fields/add", methods=['GET', 'POST'])
@login_required(role='STORE')
@access_required()
def setting_fields_add_store():

    user_id = current_user.get_id()

    diction = get_fields_dict()

    numbers = range(1, 52)

    if request.method == 'GET':
        return render_template('setting_fields_add_store.html', page='setting_fields', fields=diction, numbers=numbers)

    if request.method == 'POST':
        name = request.form['name']
        number = int(request.form['number'])
        column_type = request.form['column_type']

        if Download_Field.query.filter_by(user_id=user_id, number_column=number).first():
            return render_template('setting_fields_add_store.html', page='setting_fields', error_log='Номер ' + str(number) + ' уже используется', fields=diction, numbers=numbers)

        if Download_Field.query.filter_by(user_id=user_id, column_type=column_type).first():
            return render_template('setting_fields_add_store.html', page='setting_fields', error_log='Столбец ' + diction.get(column_type) + ' уже используется', fields=diction, numbers=numbers)


        field = Download_Field(user_id=user_id, number_column=number, column_type=column_type, name_column=name, category=1)
        db.session.add(field)
        db.session.commit()

        return redirect(url_for('store.setting_fields_table_store'))


@store.route("/store/setting_fields/delete/<field_id>")
@login_required(role='STORE')
@access_required()
def setting_fields_delete_store(field_id):
    user_id = current_user.get_id()

    field = Download_Field.query.filter_by(user_id=user_id, id=field_id).first()
    if not field:
        abort(404)

    db.session.delete(field)
    db.session.commit()

    return redirect(url_for('store.setting_fields_table_store'))


@store.route("/store/settings/download_type", methods=['GET', 'POST'])
@login_required(role='STORE')
@access_required()
def settings_download_type():
    user_id = current_user.get_id()
    watermark = User_Store_Info.query.get(user_id).water_mark
    without = User_Store_Info.query.get(user_id).without_watermark

    download_type = Download_Type.query.get(user_id)

    if request.method == 'GET':
        return render_template('store_settings_download_type.html', watermark=watermark, without=without, download_type=download_type, page='download_type')

    if request.method == 'POST':
        down_type = request.form['down_type']
        code_type = request.form['code_type']
        deli_type = request.form['deli_type']

        download_type.download_type = int(down_type)
        download_type.delimiter = deli_type
        download_type.coding = code_type
        db.session.commit()
        return redirect(url_for('store.settings_download_type'))


@store.route("/store/settings/providers_names", methods=['GET', 'POST'])
@login_required(role='STORE')
@access_required()
def settings_providers_names():
    user_id = current_user.get_id()

    if request.method == 'GET':
        store_provider_names = db.session.query(Store_Provider_Name, Provider).filter(Store_Provider_Name.store_id == user_id, Provider.id == Store_Provider_Name.provider_id).order_by(Provider.provider_name).all()
        return render_template('store_settings_provider_names.html', names = store_provider_names, page='settings_providers_names')

    if request.method == 'POST':
        ids = request.form.getlist('ids')
        for id in ids:
            store_provider_name = Store_Provider_Name.query.get(id)
            store_provider_name.provider_name = request.form['provider_name' + str(id)]
            db.session.commit()

        return redirect(url_for('store.settings_providers_names'))


@store.route("/store/settings/download_feature", methods=['GET', 'POST'])
@login_required(role='STORE')
@access_required()
def settings_download_feature():
    user_id = current_user.get_id()

    if request.method == 'GET':
        feature_value = Download_Feature.query.filter_by(store_id = user_id).first().feature_value
        return render_template('store_settings_download_feature.html', feature_value = feature_value, page = 'settings_download_feature')

    if request.method == 'POST':
        before_group = request.form['before_group']
        download_feature = Download_Feature.query.filter_by(store_id = user_id).first()
        download_feature.feature_value = before_group
        db.session.commit()

        return redirect(url_for('store.settings_download_feature'))