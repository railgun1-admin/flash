import pathlib

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail
import os
import logging
from logging.handlers import RotatingFileHandler

app = Flask(__name__)
app.secret_key = '347d793c50fe057ea3ea72fb06c91f22'

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
PUBLIC_PATH = os.path.join(APP_ROOT, 'public/')

app.config.update(
    SQLALCHEMY_DATABASE_URI=os.getenv("DATABASE_URL"),
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT=465,
    MAIL_USE_SSL=True,
    MAIL_USE_TLS=False,
    MAIL_USERNAME='fleshka.studio@gmail.com',
    MAIL_PASSWORD='fleshkaq1w2e3r4'
)

db = SQLAlchemy(app)
mail = Mail(app)

login_manager = LoginManager()
login_manager.init_app(app)

file_handler_error = RotatingFileHandler(
    pathlib.Path(APP_ROOT, 'logs/actions_log.log'), maxBytes=1024 * 1024 * 100, backupCount=20
)
# file_handler_actions = RotatingFileHandler('/home/dev/flash/logs/actions_log.log', maxBytes=1024 * 1024 * 100, backupCount=20)
file_handler_error.setLevel(logging.ERROR)
# file_handler_actions.setLevel(logging.DEBUG)
app.logger.addHandler(file_handler_error)
# app.logger.addHandler(file_handler_actions)
