#!make
ENV_FILE_CHECK = .env
ifneq ("$(wildcard $(ENV_FILE_CHECK))","")
	include .env
	export $(shell sed 's/=.*//' .env)
endif

run:
	python router.py

migrate:
	python manage.py db stamp head
	python manage.py db migrate
	python manage.py db upgrade

manager:
	python manage.py
