# -*- coding: utf-8 -*- #
from functools import wraps
from flask import Flask, request, url_for, send_from_directory, render_template
import datetime
from config import app, db, login_manager, APP_ROOT, PUBLIC_PATH
import app.controllers as blueprints
import datetime

app.register_blueprint(blueprints.main)
app.register_blueprint(blueprints.admin_products)
app.register_blueprint(blueprints.admin_price)
app.register_blueprint(blueprints.admin_users)
app.register_blueprint(blueprints.admin_news)
app.register_blueprint(blueprints.admin_feedback)
app.register_blueprint(blueprints.admin_settings)
app.register_blueprint(blueprints.admin_provider)
app.register_blueprint(blueprints.store)
app.register_blueprint(blueprints.provider)


@app.errorhandler(404)
def not_found(error):
    return send_from_directory(PUBLIC_PATH, '404.html'), 404

@app.errorhandler(500)
def server_error(error):
    app.logger.error(datetime.datetime.utcnow().strftime("[%d-%m-%Y %H:%M:%S]"))
    app.logger.error("-------------------------------------------------------")
    return send_from_directory(PUBLIC_PATH, '500.html'), 500

# Sends some static files (js, css, less, fonts)
@app.route('/static/<path:path>')
def static_file(path):
    return send_from_directory('static', path)

# Sends user's watermarks
@app.route('/watermarks/<path:path>')
def send_watermark(path):
    return send_from_directory('files/watermarks', path)

# Sends user's photographs
@app.route('/model/<store_name>/<photo_name>')
def send_user_photo(store_name, photo_name):
    return send_from_directory('files/users/' + store_name, photo_name)

# Sends product's thumbnails
@app.route('/thumbnails/<thumbnail>')
def send_thumbnail(thumbnail):
    return send_from_directory('files/thumbnails', thumbnail)

# Send some favicons
@app.route('/<path:path>')
def send_favicons(path):
    return send_from_directory(PUBLIC_PATH, path)

@app.route('/temp_images/<number>/<image>')
def send_temp_images(number, image):
    return send_from_directory('files/temporary/' + number, image)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, debug=True)
