"""empty message

Revision ID: 2ab4f1465efd
Revises: cc0fc53bdf14
Create Date: 2018-03-09 16:50:06.088014

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2ab4f1465efd'
down_revision = 'cc0fc53bdf14'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('preparatory_shoes',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('product_id', sa.Integer(), nullable=True),
    sa.Column('admin_id', sa.Integer(), nullable=True),
    sa.Column('provider_id', sa.Integer(), nullable=True),
    sa.Column('articul', sa.String(length=40), nullable=True),
    sa.Column('cost', sa.Float(), nullable=False),
    sa.Column('cost_site', sa.Float(), nullable=False),
    sa.Column('currency', sa.String(length=40), nullable=False),
    sa.Column('presence', sa.String(length=40), nullable=False),
    sa.Column('brand_name', sa.String(length=40), nullable=True),
    sa.Column('group', sa.String(length=40), nullable=True),
    sa.Column('type', sa.String(length=40), nullable=True),
    sa.Column('repeat', sa.String(length=40), nullable=True),
    sa.Column('sex', sa.String(length=40), nullable=True),
    sa.Column('min_order', sa.String(length=40), nullable=True),
    sa.Column('material_outside', sa.String(length=40), nullable=True),
    sa.Column('material_inside', sa.String(length=40), nullable=True),
    sa.Column('material_insole', sa.String(length=40), nullable=True),
    sa.Column('colour', sa.String(length=40), nullable=True),
    sa.Column('size', sa.String(length=40), nullable=True),
    sa.Column('season', sa.String(length=40), nullable=True),
    sa.Column('box', sa.String(length=40), nullable=True),
    sa.Column('manufacturer', sa.String(length=40), nullable=True),
    sa.ForeignKeyConstraint(['admin_id'], ['user_admin_info.id'], ),
    sa.ForeignKeyConstraint(['product_id'], ['product.id'], ),
    sa.ForeignKeyConstraint(['provider_id'], ['user_provider_info.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.add_column('old_product_features', sa.Column('brand_name', sa.String(length=40), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('old_product_features', 'brand_name')
    op.drop_table('preparatory_shoes')
    # ### end Alembic commands ###
